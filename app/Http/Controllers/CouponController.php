<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_coupon';
        //$CouponData = Coupon::select('');

        $CouponData = DB::select('SELECT i.*, (Select GROUP_CONCAT(c.name) FROM  category c WHERE FIND_IN_SET(c.id, i.category_id)) as c, (Select GROUP_CONCAT(p.name) FROM  products p WHERE FIND_IN_SET(p.id, i.product_id)) as p FROm coupon i GROUP BY i.id');
      // dd($CouponData);
        return View('admin/coupon/index',compact('CouponData','activeTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activeTab = 'manage_coupon';
        $categories = DB::table("category")->get();
        $products = DB::table("products")->get();
        return View('admin/coupon/create',compact('activeTab','categories','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $coupon = new Coupon;
        $product1 = $request->input('product');
        $category1 = $request->input('category');
        $category = implode(',', $category1);
        $product = implode(',', $product1);
        $coupon->name = $request->input('name');
        $coupon->category_id = $category;
        $coupon->product_id = $product;
        $coupon->detail = $request->input('detail');
        $coupon->code = $request->input('code');
        $coupon->discount = $request->input('discount');
        $coupon->min_amount = $request->input('min_amount');
        $coupon->max_amount = $request->input('max_amount');
        $coupon->discount_amount = $request->input('discount_amount');
        $coupon->status = $request->input('status') != "" ? $request->input('status') : 0;
        $coupon->created_at = $date;

        

        if(!$coupon->save()) {
            //Redirect error
            return redirect()->route('coupon')->with('error','Failed to update coupon details.');
        }

        //Redirect success
        return redirect()->route('coupon')->with('success','coupon added successfully.');
    
    }

    public function changeCouponStatus(Request $request) {
        $coupon_id = $request->coupon_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($coupon_id)) {
            $menu = Coupon::where('id',$coupon_id)
                                ->update(['status' => $status]);

            if($menu) {
                echo "success";
            } else {
                echo "fail"; 
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activeTab = 'manage_coupon';
        
        $categories = DB::table("category")->get();
        $products = DB::table("products")->get();
        $coupon = Coupon::find($id);
        
            $cc = explode(',',$coupon->category_id);
            $cp = explode(',',$coupon->product_id);
        
        return View('admin/coupon/create',compact('coupon','activeTab','categories','products','cc','cp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $coupon = Coupon::find($id);
            $product1 = $request->input('product');
            $category1 = $request->input('category');
            $category = implode(',', $category1);
            $product = implode(',', $product1);
            $coupon->name = $request->input('name');
            $coupon->category_id = $category;
            $coupon->product_id = $product;
            $coupon->detail = $request->input('detail');
            $coupon->code = $request->input('code');
            $coupon->discount = $request->input('discount');
            $coupon->min_amount = $request->input('min_amount');
            $coupon->max_amount = $request->input('max_amount');
            $coupon->discount_amount = $request->input('discount_amount');
            $coupon->status = $request->input('status') != "" ? $request->input('status') : 0;
            $coupon->updated_at = $date;

            

            if($coupon->save()) {
                //Redirect success
                return redirect()->route('coupon')->with('success','Coupon updated successfully.');
            }

            //Redirect error
            return redirect()->route('coupon')->with('error','Failed to update coupon.');


        } catch (Exception $e) {
            Log::error("Failed to update coupon.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
    }
}
