<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Usermembership;
use App\Subscription;
use App\MembershipType;
use App\MembershipPackage;
use Auth;
use Razorpay\Api\Api;
use Redirect,Response;
use Illuminate\Support\Facades\Session;
use App\EmailTemplate;
use App\SmsTemplate;
use App\FileManagement;
use Mail;
use DB;
use PDF;
use Illuminate\Support\Str;


class DashboardController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }
	
	 public function index() {
		$activeTab = 'dashboard';
		 return view('admin/dashboard','activeTab');
    }
	
	
	 
	
}
