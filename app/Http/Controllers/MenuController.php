<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_menu';
        $MenuData = Menu::all();
        return View('admin/menu/index',compact('MenuData','activeTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activeTab = 'manage_menu';
        return View('admin/menu/create',compact('activeTab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $menu = new Menu;
        $menu->name = $request->input('name');
        $menu->type = $request->input('type');
        $menu->path = $request->input('path');
        $menu->status = $request->input('status') != "" ? $request->input('status') : 0;
        $menu->created_at = $date;

        

        if(!$menu->save()) {
            //Redirect error
            return redirect()->route('menu')->with('error','Failed to update menu details.');
        }

        //Redirect success
        return redirect()->route('menu')->with('success','menu added successfully.');
    
    }

    public function changeMenuStatus(Request $request) {
        $menu_id = $request->menu_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($menu_id)) {
            $menu = Menu::where('id',$menu_id)
                                ->update(['status' => $status]);

            if($menu) {
                echo "success";
            } else {
                echo "fail"; 
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activeTab = 'manage_menu';
        $menu = Menu::find($id);
        return View('admin/menu/create',compact('menu','activeTab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $menu = Menu::find($id);
            $menu->name = $request->input('name');
            $menu->type = $request->input('type');
            $menu->path = $request->input('path');
            $menu->status = $request->input('status') != "" ? $request->input('status') : 0;
            $menu->updated_at = $date;

            

            if($menu->save()) {
                //Redirect success
                return redirect()->route('menu')->with('success','Menu updated successfully.');
            }

            //Redirect error
            return redirect()->route('menu')->with('error','Failed to update menu.');


        } catch (Exception $e) {
            Log::error("Failed to update menu.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
    }
}
