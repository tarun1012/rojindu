<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Notifications\TwoFactorCode;
use App\EmailTemplate;
use Mail;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
       if(Auth::user()->roleid != "6")
       {
        if(Auth::user()->status == "1")
         {
             return 'admin/dashboard';
         }else
         {
            Auth::logout();
            return 'login';
         }
        
        }else
       {
         $user = Auth::user();
         $user->generateTwoFactorCode();
         
         return 'twoFactor';
       }
    }

    

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
