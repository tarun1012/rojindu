<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Crypt;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function changePasswordForm(Request $request) {
        $activeTab = 'change_password';
        return view('auth.passwords.change-password',compact('activeTab'));
    }

    public function changePasswordRequest(Request $request) {
        
        $request->validate([
            'old_password'   =>  'required|min:6',
            'new_password'   =>  'required|same:confirm_password|min:6'
        ]);

         $request->validate([
            'old_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'confirm_password' => ['same:new_password'],
        ]);
   
        $user = User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

            return redirect()->back()->with('success', 'Password updated successfully');
        

    }

}
