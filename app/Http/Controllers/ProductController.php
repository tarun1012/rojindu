<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ProductGallery;
use Illuminate\Support\Facades\Auth;
use DNS1D;

use DB;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_product';
        $ProductData = Product::join('category','products.category_id','=','category.id')->join('subcategory','products.subcategory_id','=','subcategory.id')->join('brand','products.brand_id','=','brand.id')->select('products.*','category.name as category_name','subcategory.name as subcategory_name','brand.name as brand_name')->get();
        return View('admin/product/index2',compact('ProductData','activeTab'));
    }

    public function ProductRequest()
    {
        $activeTab = 'manage_productrequest';
        $user = Auth::user();
        $userid = $user->id;
        $ProductRequestData  = Product::Where('created_by',$userid)->get();
        return View('admin/retailer/product_request/index',compact('ProductRequestData','activeTab'));
    }

     public function storeProductRequest(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $user = Auth::user();
        $product = new Product;
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->created_by = $user->id;
        $product->status = "0";
        $product->created_at = $date;
        
        if(!$product->save()) {
            //Redirect error
            return redirect()->route('productrequest')->with('error','Failed to  update product Request.');
        }


         

        //Redirect success
        return redirect()->route('productrequest')->with('success',' Product requested successfully.');
    
    }

    public function editProductRequest($id)
    {
        $activeTab = 'manage_productrequest';
        $products = DB::table("products")->get();
        
        $productrequest = Product::find($id);
        return View('admin/retailer/product_request/create',compact('activeTab','productrequest'));
    }

    public function updateProductRequest(Request $request, $id) 
    {
        try {
            $date = date('Y-m-d h:i:s');
            $product = Product::find($id);
            $product->name = $request->input('name');
            $product->price = $request->input('price');
            $product->updated_at = $date;

            
            if($product->save()) {
                //Redirect success
                return redirect()->route('productrequest')->with('success','Product Request updated successfully.');
            }

            //Redirect error
            return redirect()->route('productrequest')->with('error','Failed to  retailer product request.');


        } catch (Exception $e) {
            Log::error("Failed to update retailer product request.");
        }
    }

    public function createProductRequest()
    {
        $activeTab = 'manage_productrequest';
        $products = DB::table("products")->get();
        
        return View('admin/retailer/product_request/create',compact('products','activeTab'));
        
    }

    public function manage_product_request() {
         
        $user = Auth::user();
        $activeTab = 'manage_admin_product_request';
        
        
         try {
    
        
        $products = Product::where('status',0)->orderBy('id','asc')->get();
        
        //echo '<pre>'; print_r($subscription); echo '</pre>'; exit;
        return view('admin/retailer/product_request/product_request', compact('products','activeTab'));
        
        } catch (Exception $e) {
                Log::error("Failed to load manage product_request data -> ".$e);
            }
        
        
    }

    public function changeProductRequestStatus(Request $request) {
        $status = $request->status;
        $id = $request->id;
        

        if($id != "" && $status != "") {
            try {
                
            $product = Product::where(['id'=>$id])->update(['status' => $status ]);
            
                    //$user_membership = Usermembership::where(['id'=>$id])->update(['status' => $status ],['date' => date('Y-m-d')])
                    if($product) {
                        return response()->json(['result' => 'success']);
                    }
                    return response()->json(['result' => 'fail']);
               
            } catch (Exception $e) {
                Log::error("Failed to change product status -> ".$e);
            }
        }   
    }
    
    function str_random($length = 16)
    {
        return Str::random($length);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activeTab = 'manage_product';
        $categories = DB::table("category")->get();
        $subcategories = DB::table("subcategory")->get();
        $brands = DB::table("brand")->get();
        $units = DB::table("product_unit")->get();
        return View('admin/product/create',compact('categories','subcategories','activeTab','brands','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $product = new Product;
        $product->name = $request->input('name');
        $product->category_id = $request->input('category');
        $product->subcategory_id = $request->input('subcategory');
        $product->brand_id = $request->input('brand');
        $product->unit = $request->input('unit');
        $product->price = $request->input('price');
        $product->detail = $request->input('product-content');
        $product->status = $request->input('status') != "" ? $request->input('status') : 0;
        $product->created_at = $date;

        /**
         * Banner Upload Script.
         */
        $file1 = $request->file('image');
        $filename1=$file1->getClientOriginalName();
        $file1->move(public_path().'/uploads/product/', $filename1);
        $product->image = $filename1;





        if(!$product->save()) {
            //Redirect error
            return redirect()->route('product')->with('error','Failed to update product.');
        }

        

        if(!$product->save()) {
            //Redirect error
            return redirect()->route('product')->with('error','Failed to update product.');
        }

         $product_id = $product->id;
         if($files = $request->hasFile('product_gallery')) {
                foreach ($request->file('product_gallery') as $file){
                    $gallery = new ProductGallery;
                    $image_name = str_random(2).time().$file->getClientOriginalName();
                    $file->move(public_path().'/uploads/gallery_images/',$image_name);
                    $gallery['product_id'] = $product_id;
                    $gallery['image'] = $image_name;
                    $gallery['created_at'] = date('Y-m-d h:i:s');
                    
                    if(!$gallery->save()) {
                        Log::error("Product gallery image not uploaded.");
                    }
                }
            }

        //Redirect success
        return redirect()->route('product')->with('success','product added successfully.');
    
    }

    public function changeProductStatus(Request $request) {
        $product_id = $request->product_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($product_id)) {
            $product = Product::where('id',$product_id)
                                ->update(['status' => $status]);

            if($product) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

     public function getSubcategoryList(Request $request) {
        $subcategory = DB::table("subcategory")
            ->where("category_id",$request->c_id)
            ->pluck("name","id");
            return response()->json($subcategory);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activeTab = 'manage_product';
        $categories = DB::table("category")->get();
        $subcategories = DB::table("subcategory")->get();
        $brands = DB::table("brand")->get();
        $units = DB::table("product_unit")->get();
        $ProductGallery = ProductGallery::where('product_id',$id)->get();
        $product = Product::find($id);
        return View('admin/product/create',compact('subcategories','activeTab','categories','product','brands','ProductGallery','units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $product = Product::find($id);
            $product->name = $request->input('name');
            $product->category_id = $request->input('category');
            $product->subcategory_id = $request->input('subcategory');
            $product->price = $request->input('price');
            $product->brand_id = $request->input('brand');
            $product->unit = $request->input('unit');
            $product->status = $request->input('status') != "" ? $request->input('status') : 0;
            $product->updated_at = $date;

            if($request->hasFile('image')) {
                $file1 = $request->file('image');
                $filename1=$file1->getClientOriginalName();
                $file1->move(public_path().'/uploads/product/', $filename1);
                
                $old_banner = $request->input("product_path");

                if(file_exists(public_path().'\uploads\product\\'.$old_banner)) {
                    unlink('uploads\product\\'.$old_banner);
                }

                $product_img = Product::where('id',$id)
                                ->update(['image' => $filename1]);

                if(!$product_img) {
                    Log::error("Product not uploaded.");
                }
            }

            if($files = $request->hasFile('product_gallery')) {
                //$delete_gallery = DB::table('tour_gallery')->where('tourid',$id)->delete();
                foreach ($request->file('product_gallery') as $file) {
                    $gallery = new ProductGallery;
                    $image_name = str_random(2).time().$file->getClientOriginalName();
                    $file->move(public_path().'/uploads/gallery_images/',$image_name);
                    $gallery['product_id'] = $id;
                    $gallery['image'] = $image_name;
                    $gallery['created_at'] = date('Y-m-d h:i:s');
                    
                    if(!$gallery->save()) {
                        Log::error("Product gallery image not uploaded.");
                    }
                }
            }

            

            if($product->save()) {
                //Redirect success
                return redirect()->route('product')->with('success','Product updated successfully.');
            }

            //Redirect error
            return redirect()->route('product')->with('error','Failed to update product.');


        } catch (Exception $e) {
            Log::error("Failed to update product.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
    }

    public function changeProductStatus_datatable(Request $request, $id) {
        $Product = Product::find($id);

        if($Product->status == 1) {
            $Product->status = 0;
        }elseif($Product->status == 0) {
            $Product->status = 1;
        }
        $Product->save();

        return redirect()->route('product')->with('success', 'Product Status Updated');

         
        

    }

    public function destroyGallery($id)
    {
        try {
            $Gallery = ProductGallery::find($id)->delete($id);
            if($Gallery) {
                if(file_exists(public_path().'\uploads\gallery_images\\'.$_POST['path'])) {
                    unlink('uploads\gallery_images\\'.$_POST['path']);
                }
                echo "success";
            } else {
                echo "fail";
            }
        } catch (Exception $e) {
            Log::error("Failed to delete gallery image -> ".$e);
        }
    }

    public function allProducts(Request $request)
    {
        
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2=> 'category_id',
                            3=> 'created_at',
                            4=> 'id',
                        );
  
        $totalData = Product::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = Product::join('category','products.category_id','=','category.id')->join('subcategory','products.subcategory_id','=','subcategory.id')->join('brand','products.brand_id','=','brand.id')->select('products.*','category.name as category_name','subcategory.name as subcategory_name','brand.name as brand_name')->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts = Product::join('category','products.category_id','=','category.id')->join('subcategory','products.subcategory_id','=','subcategory.id')->join('brand','products.brand_id','=','brand.id')->select('products.*','category.name as category_name','subcategory.name as subcategory_name','brand.name as brand_name')
                         ->where('products.id','LIKE',"%{$search}%")
                         ->orWhere('products.name', 'LIKE',"%{$search}%")
                         ->orWhere('category.name', 'LIKE',"%{$search}%")
                         ->orWhere('subcategory.name', 'LIKE',"%{$search}%")
                         ->orWhere('brand.name', 'LIKE',"%{$search}%")
                         ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
            /*$posts =  Product::where('id','LIKE',"%{$search}%")
                            ->orWhere('name', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();*/

            $totalFiltered = Product::join('category','products.category_id','=','category.id')->join('subcategory','products.subcategory_id','=','subcategory.id')->join('brand','products.brand_id','=','brand.id')->select('products.*','category.name as category_name','subcategory.name as subcategory_name','brand.name as brand_name')
                         ->where('products.id','LIKE',"%{$search}%")
                         ->orWhere('products.name', 'LIKE',"%{$search}%")
                         ->orWhere('category.name', 'LIKE',"%{$search}%")
                         ->orWhere('subcategory.name', 'LIKE',"%{$search}%")
                         ->orWhere('brand.name', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('product.edit',$post->id); 
                $edit = route('product.edit', $post->id);
                $url =  asset('uploads/product/'.$post->image);
                $bar =  DNS1D::getBarcodePNG($post["id"].' '.$post["category_name"], 'C128');
                $st = route('change.product_datatable.status', $post->id);

                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->name;
                $nestedData['category_name'] = $post->category_name;
                $nestedData['subcategory_name'] = $post->subcategory_name;
                $nestedData['brand_name'] = $post->brand_name;
                $nestedData['detail'] = $post->detail;
              //  $nestedData['image'] = $post->image;
                $nestedData['image'] = '<img src="' . $url . '" style="width: 100px !important;" alt="' . $post->name. '">';
                $nestedData['barcode'] = '<img src="data:image/png;base64,'.$bar.'" alt="'.$post['id'].'" />';
                //$nestedData['status'] = $post->status;
                if($post->status == '1')
                {
                    $nestedData['status'] = '<span class="badge badge-success">Active</span>';
                }else
                {
                    $nestedData['status'] = '<span class="badge badge-danger">Paused</span>';
                }

                $options = "";

                if($post->status == '1')
                {
                    $options = '<button type="button" class="btn btn-rounded btn-sm btn-warning changeProductStatus" id="changeProductStatus" data-product_id="' . $post->id . '" data-status="' . $post->status . '" data-toggle="tooltip" title="Pause product"><i class="fa fa-power-off"></i></button>&nbsp;' ;

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-warning" data-toggle="tooltip" title="Pause product"> <i class="fa fa-power-off"></i> </a>&nbsp;';
                }else
                {
                   $options = '<button type="button" id="changeProductStatus" class="btn btn-rounded btn-sm btn-success changeProductStatus" data-product_id="' . $post->id . '" data-status="' . $post->status . '" data-toggle="tooltip" title="Activate product"><i class="fa fa-refresh"></i></button>&nbsp;';

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-success changeProductStatus" data-toggle="tooltip" title="Activate product"> <i class="fa fa-refresh"></i> </a>&nbsp;';
                }
                 
                 if(get_user_permission("manage_product","edit"))
                {
                 $options .= '<a href="'.$edit.'" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit product"><i class="fa fa-edit"></i></a>';
                 }

                $nestedData['options'] = $options;
                
                /*$nestedData['body'] = substr(strip_tags($post->detail),0,50)."...";
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";*/
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
        
    }
}
