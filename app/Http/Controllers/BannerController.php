<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Log;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_banners';
        $BannersData = Banner::all();
        return View('admin/banner/index2',compact('BannersData','activeTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createBanner()
    {
        $activeTab = 'manage_banners';
        return View('admin/banner/create',compact('activeTab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBanner(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $banner = new Banner;
        $banner->title = $request->input('title');
        $banner->name = $request->input('name');
        $banner->content = $request->input('banner-content');
        $banner->link = $request->input('link');
        $banner->status = $request->input('status') != "" ? $request->input('status') : 0;
        $banner->created_at = $date;

        /**
         * Banner Upload Script.
         */
        $file1 = $request->file('image');
        $filename1=$file1->getClientOriginalName();
        $file1->move(public_path().'/uploads/banner/', $filename1);
        $banner->image = $filename1;

        if(!$banner->save()) {
            //Redirect error
            return redirect()->route('banner')->with('error','Failed to update banner details.');
        }

        //Redirect success
        return redirect()->route('banner')->with('success','Banner added successfully.');
    
    }

    public function changeBannerStatus(Request $request) {
        $banner_id = $request->banner_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($banner_id)) {
            $banner = Banner::where('id',$banner_id)
                                ->update(['status' => $status]);

            if($banner) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editBanner($id)
    {
        $activeTab = 'manage_banners';
        $Banner = Banner::find($id);
        return View('admin/banner/create',compact('Banner','activeTab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateBanner(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $banner = Banner::find($id);
            $banner->title = $request->input('title');
            $banner->name = $request->input('name');
            $banner->content = $request->input('banner-content');
            $banner->link = $request->input('link');
            $banner->status = $request->input('status') != "" ? $request->input('status') : 0;
            $banner->updated_at = $date;

            /**
             * Banner Upload Script.
             */
            if($request->hasFile('image')) {
                $file1 = $request->file('image');
                $filename1=$file1->getClientOriginalName();
                $file1->move(public_path().'/uploads/banner/', $filename1);
                
                $old_banner = $request->input("banner_path");

                if(file_exists(public_path().'\uploads\banner\\'.$old_banner)) {
                    unlink('uploads\banner\\'.$old_banner);
                }

                $banner_img = Banner::where('id',$id)
                                ->update(['image' => $filename1]);

                if(!$banner_img) {
                    Log::error("Banner not uploaded.");
                }
            }

            if($banner->save()) {
                //Redirect success
                return redirect()->route('banner')->with('success','Banner updated successfully.');
            }

            //Redirect error
            return redirect()->route('banner')->with('error','Failed to update banner.');


        } catch (Exception $e) {
            Log::error("Failed to update banner.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyBanner($id)
    {   
        
    }

    public function changeBannerStatus_datatable(Request $request, $id) {
        $Banner = Banner::find($id);

        if($Banner->status == 1) {
            $Banner->status = 0;
        }elseif($Banner->status == 0) {
            $Banner->status = 1;
        }
        $Banner->save();

        return redirect()->route('banner')->with('success', 'Banner Status Updated');

         
        

    }

    public function allBanners(Request $request)
    {
        
        $columns = array( 
                            0 =>'id', 
                            1 =>'title',
                            2=> 'name',
                            3=> 'content',
                            4=> 'image',
                            5=> 'status',
                        );
  
        $totalData = Banner::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = Banner::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts = Banner::where('title','LIKE',"%{$search}%")
                         ->orWhere('name', 'LIKE',"%{$search}%")
                         ->orWhere('content', 'LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                         ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
            

            $totalFiltered = Banner::where('title','LIKE',"%{$search}%")
                         ->orWhere('name', 'LIKE',"%{$search}%")
                         ->orWhere('content', 'LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('banner.edit',$post->id); 
                $edit = route('banner.edit', $post->id);
                $url =  asset('uploads/banner/'.$post->image);
                
                $st = route('change.banner_datatable.status', $post->id);

                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->name;
                $nestedData['title'] = $post->title;
                $nestedData['content'] = $post->content;
                
                $nestedData['image'] = '<img src="' . $url . '" style="width: 200px !important;" alt="' . $post->name. '">';
                
                //$nestedData['status'] = $post->status;
                if($post->status == '1')
                {
                    $nestedData['status'] = '<span class="badge badge-success">Active</span>';
                }else
                {
                    $nestedData['status'] = '<span class="badge badge-danger">Paused</span>';
                }

                $options = "";

                if($post->status == '1')
                {
                    

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-warning" data-toggle="tooltip" title="Pause Banner"> <i class="fa fa-power-off"></i> </a>&nbsp;';
                }else
                {
                   // $options = '<button type="button" id="changeProductStatus" class="btn btn-rounded btn-sm btn-success changeProductStatus" data-product_id="' . $post->id . '" data-status="' . $post->status . '" data-toggle="tooltip" title="Activate product"><i class="fa fa-refresh"></i></button>&nbsp;';

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-success changeProductStatus" data-toggle="tooltip" title="Activate Banner"> <i class="fa fa-refresh"></i> </a>&nbsp;';
                }
                 
                 if(get_user_permission("manage_banner","edit"))
                {
                 $options .= '<a href="'.$edit.'" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit Banner"><i class="fa fa-edit"></i></a>';
                 }

                $nestedData['options'] = $options;
                
                /*$nestedData['body'] = substr(strip_tags($post->detail),0,50)."...";
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";*/
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
        
    }
}
