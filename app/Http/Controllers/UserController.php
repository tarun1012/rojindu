<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use DB;
use Crypt;
use Auth;
use App\UserDocuments;
use App\DocumentFiles;
use App\UserRequestStatus;
use Mail;
use App\EmailTemplate;
use App\SmsTemplate;
use App\Role;
use Excel;
use App\CustomClass\UplineExports;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    protected $ParentList,$UsersList;

    public function __construct()
    {
        $this->ParentList = array();
        $this->UsersList = User::whereIn('roleid',[1,2,3,4,5])->where('status','=',1)->get()->toArray();
    }
	
	public function showUsers() {
        /**
         * Old Query
         */
        // $users  =   User::join('roles','users.roleid','=','roles.id')
        //                 ->whereIn('users.roleid', [1,2,3,4,5])
        //                 ->select('users.*','roles.name as rolename')
        //                 ->orderBy('id','desc')
        //                 ->get();
       

		$users = User::select(
                'users.*',
                'parent.id as parent_id',
                'parent.name as parent_firstname',
                'parent.email as parent_username',
                'role.id as roleid',
                'role.name as role_name'
            )
            ->leftJoin('users as parent','parent.id','=','users.parentid')
            ->leftJoin('roles as role','users.roleid','=','role.id')
            ->whereIn('users.roleid',[1,2,3,4,5])
            ->get();
        $activeTab = 'manage_users';
        return view('admin.users.show-users',compact('users','activeTab'));
    }

    public function showRetailer() {
        /**
         * Old Query
         */
        // $users  =   User::join('roles','users.roleid','=','roles.id')
        //                 ->whereIn('users.roleid', [1,2,3,4,5])
        //                 ->select('users.*','roles.name as rolename')
        //                 ->orderBy('id','desc')
        //                 ->get();
       

        $users = User::select(
                'users.*',
                'parent.id as parent_id',
                'parent.name as parent_firstname',
                'parent.email as parent_username',
                'role.id as roleid',
                'role.name as role_name'
            )
            ->leftJoin('users as parent','parent.id','=','users.parentid')
            ->leftJoin('roles as role','users.roleid','=','role.id')
            ->whereIn('users.roleid',[11])
            ->get();
        $activeTab = 'manage_retailer';
        return view('admin.retailer.show-users',compact('users','activeTab'));
    }

    public function createRetailerRegisterPage() {

         $countries = DB::table("country")->get();
        $roles =Role::whereIn('id',[1,2,3,4,5])->orderBy('id','desc')->get();
        $activeTab = 'manage_retailer';
        return view('auth.retailer-register',compact('activeTab','roles','countries'));
        
    }


    

    public function createUsers() {

        
        $roles =Role::whereIn('id',[1,2,3,4,5])->orderBy('id','desc')->get();
        $activeTab = 'manage_users';
        return view('admin.users.create-users',compact('activeTab','roles'));
        
    }

    public function createRetailer() {

         $countries = DB::table("country")->get();
        $roles =Role::whereIn('id',[1,2,3,4,5])->orderBy('id','desc')->get();
        $activeTab = 'manage_retailer';
        return view('admin.retailer.create-users',compact('activeTab','roles','countries'));
        
    }

    public function createAdvisorUsers() {

        $countries = DB::table("country")->get();
        $activeTab = 'manage_advisor_users';
        return view('advisor.create-advisor',compact('countries','activeTab'));
        
    }

    public function saveUser(Request $request) {
              //  echo '<pre>'; print_r($request->all()); echo '</pre>'; exit;
        $request->validate([
            
            'firstname'              =>  'required',
            'email'             =>  'required|email|max:255|unique:users,email',
            'password'          =>  'required',
            'status'            =>  'required',
        ]);

        $user = new User;
        $user->name             =   $request->firstname;
        $user->email            =   $request->email;
        $user->password         =   Crypt::encrypt($request->password);
        $user->roleid             = $request->role;
		$user->status           =   $request->status != "" ? $request->status : 0;
        $user->parentid           =   $request->parent_user_id != "" ? $request->parent_user_id : 0;
        
        $user->save();
		
		

        return redirect()->route('users')->with('success', 'Add User Successfully');

    }

    public function saveRetailer(Request $request) {
              //  echo '<pre>'; print_r($request->all()); echo '</pre>'; exit;
        $request->validate([
            
            
            'firstname'              =>  'required',
            'lastname'              =>  'required',
            'email'             =>  'required|email|max:255|unique:users,email',
            'password'          =>  'required',
            'contactno'         =>  'required|numeric',
            'profilepic'        =>  'mimes:jpeg,jpg,png,gif|max:10000',
            
            'address'           =>  'required',
            'shop_address'           =>  'required',
            'shop_name'           =>  'required',
            'country'           =>  'required',
            'locality'           =>  'required',
            'state'             =>  'required',
            'city'              =>  'required',
            'status'            =>  'required',
        ]);

        $user = new User;
        
        //$user->username         =   $request->username;
        $user->firstname        =   $request->firstname;
        $user->lastname         =   $request->lastname;
        $user->name             =   $request->firstname .' '.$request->lastname;
        $user->email            =   $request->email;
        $user->password         =   Hash::make($request->password);
        $user->contactno        =   $request->contactno;
        $user->whatsappno        =  ($request->IsWhatsapp != "" ? $request->contactno : ($request->whatsapp != "" ? $request->whatsapp : ""));
        
        $imageName = time().'.'.$request->profilepic->extension();  
        $request->profilepic->move(public_path('profiles'), $imageName);
        $user->profilepic       =   $imageName;

        
        $user->address          =   $request->address;
        $user->shop_name          =   $request->shop_name;
        $user->shop_address          =   $request->shop_address;
        $user->country          =   $request->country;
        $user->state            =   $request->state;
        $user->city             =   $request->city; 
        $user->locality         =   $request->locality;
        $user->dob              =   $request->dob;
        $user->gender             =   $request->gender;
        $user->roleid             = '11';
        $user->status           =   $request->status != "" ? $request->status : 0;
        $user->save();

        $doc  =   new UserDocuments;
        $doc->userid     =   $user->id;
        $doc->doctypeid     =   $user->id;
        $doc->save();
        
        

        return redirect()->route('retailer')->with('success', 'Add Retailer Successfully');

    }

    public function saveRetailerRegister(Request $request) {
              //  echo '<pre>'; print_r($request->all()); echo '</pre>'; exit;
        $request->validate([
            
            
            'firstname'              =>  'required',
            'lastname'              =>  'required',
            'email'             =>  'required|email|max:255|unique:users,email',
            'password'          =>  'required',
            'contactno'         =>  'required|numeric',
            'profilepic'        =>  'mimes:jpeg,jpg,png,gif|max:10000',
            
            'address'           =>  'required',
            'shop_address'           =>  'required',
            'shop_name'           =>  'required',
            'country'           =>  'required',
            'locality'           =>  'required',
            'state'             =>  'required',
            'city'              =>  'required',
            
        ]);

        $user = new User;
        
        //$user->username         =   $request->username;
        $user->firstname        =   $request->firstname;
        $user->lastname         =   $request->lastname;
        $user->name             =   $request->firstname .' '.$request->lastname;
        $user->email            =   $request->email;
        $user->password         =   Hash::make($request->password);
        $user->contactno        =   $request->contactno;
        $user->whatsappno        =  ($request->IsWhatsapp != "" ? $request->contactno : ($request->whatsapp != "" ? $request->whatsapp : ""));
        
        $imageName = time().'.'.$request->profilepic->extension();  
        $request->profilepic->move(public_path('profiles'), $imageName);
        $user->profilepic       =   $imageName;

        
        $user->address          =   $request->address;
        $user->shop_name          =   $request->shop_name;
        $user->shop_address          =   $request->shop_address;
        $user->country          =   $request->country;
        $user->state            =   $request->state;
        $user->city             =   $request->city; 
        $user->locality         =   $request->locality;
        $user->dob              =   $request->dob;
        $user->gender             =   $request->gender;
        $user->roleid             = '11';
        $user->status           =   '0';
        $user->save();

        $doc  =   new UserDocuments;
        $doc->userid     =   $user->id;
        $doc->doctypeid     =   $user->id;
        $doc->save();
        
        

        return redirect()->back()->with('success', 'Add Retailer Successfully');
        

    }

    

    public function editUser(Request $request, $id) {
        $user = User::leftJoin('users as parent_user','users.parentid','=','parent_user.id')
                    ->select('users.*','parent_user.id as parentid','parent_user.name as parent_name')
                    ->where('users.id',$id)
                    ->first();
       
		$roles =    Role::whereIn('id',[1,2,3,4,5])->orderBy('id','desc')->get();
		$activeTab = 'manage_users';
        return view('admin.users.create-users',compact('user','activeTab','roles'));
     //   return view('users.create-users')->with('user', $user)->with('countries', $countries)->with('states', $state)->with('cities', $city);
    }
     public function editRetailer(Request $request, $id) {
        $user = User::leftJoin('users as parent_user','users.parentid','=','parent_user.id')
                    ->select('users.*','parent_user.id as parentid','parent_user.name as parent_name')
                    ->where('users.id',$id)
                    ->first();
        $countries  = DB::table("country")->get();
        $states      = DB::table("state")->get();
        $cities       = DB::table("city")->get();
        $roles =    Role::whereIn('id',[1,2,3,4,5,11])->orderBy('id','desc')->get();
        $activeTab = 'manage_retailer';
        return view('admin.retailer.create-users',compact('user','activeTab','roles','countries','states','cities'));
     //   return view('users.create-users')->with('user', $user)->with('countries', $countries)->with('states', $state)->with('cities', $city);
    }

    

    public function updateUser(Request $request, $id) {
        
        $request->validate([
            
            'firstname'              =>  'required',
            'email'             =>  'required|email|max:255',
            
            'status'            =>  'required',
        ]);

        $user = User::find($id);
        if($user == null) {
            return redirect()->back()->with('error', 'Invalid Route Direction');
        }

        $user->name             =   $request->firstname ;
        $user->email            =   $request->email;
        
        $user->roleid           = $request->role;
        $user->parentid         =   (!empty($request->parent_user_id) ? $request->parent_user_id : "0");
        $user->status           =  $request->status != "" ? $request->status : 0;
        
        $user->save();
        
        return redirect()->route('users')->with('success', 'Update User Successfully');
    }

    public function updateRetailer(Request $request, $id) {
        
        $request->validate([
            
            'firstname'              =>  'required',
            'lastname'              =>  'required',
            'email'             =>  'required|email|max:255',
            
            'contactno'         =>  'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'address'           =>  'required',
            'shop_name'           =>  'required',
            'shop_address'           =>  'required',
            'country'           =>  'required',
            'state'             =>  'required',
            'city'              =>  'required',
            'status'            =>  'required',
        ]);

        $user = User::find($id);
        if($user == null) {
            return redirect()->back()->with('error', 'Invalid Route Direction');
        }

        
        $user->firstname        =   $request->firstname;
        $user->lastname         =   $request->lastname;
        $user->name             =   $request->firstname .' '.$request->lastname;
        $user->email            =   $request->email;
        $user->contactno        =   $request->contactno;
        $user->whatsappno        =  ($request->IsWhatsapp != "" ? $request->contactno : ($request->whatsapp != "" ? $request->whatsapp : ""));

        if($request->file('profilepic')) {
            $imageName = time().'.'.$request->profilepic->extension();  
            $request->profilepic->move(public_path('profiles'), $imageName);
            $user->profilepic       =   $imageName;
        }
        

        $user->address          =   $request->address;
        $user->shop_address          =   $request->shop_address;
        $user->shop_name          =   $request->shop_name;
        $user->country          =   $request->country;
        $user->state            =   $request->state;
        $user->city             =   $request->city; 
        $user->locality         =   $request->locality;
        $user->dob              =   $request->dob;
        $user->gender             =   $request->gender;
        $user->roleid           = '11';
        $user->status           =  $request->status != "" ? $request->status : 0;
        
        $user->save();
        
        return redirect()->route('retailer')->with('success', 'Update Retailer Successfully');
    }

	public function existusername(Request $request){

        $id = $request->input('id');
        

        if($id != '')
        {
             $title_exists = (count(\App\User::where('id', '!=', $id)->where('email', '=', $request->input('email'))->get()) > 0) ? false : true;
            return response()->json($title_exists);
        }
        else
        {
            $title_exists = (count(\App\User::where('email', '=', $request->input('email'))->get()) > 0) ? false : true;
            return response()->json($title_exists);
        }  
    }
    
    public function changeUserStatus(Request $request, $id) {
        $user = User::find($id);

        if($user->status == 1) {
            $user->status = 0;
        }elseif($user->status == 0) {
            $user->status = 1;
        }
        $user->save();

        return redirect()->route('users')->with('success', 'User Status Updated');

    }

    public function editProfileForm(Request $request) {
        
        $user = \Auth::user();
        $countries = DB::table('country')->get();
        $states = DB::table('state')->get();
        $cities = DB::table('city')->get();
        // dd($user);
		$activeTab = "my_profile";
        return view('admin.users.edit-profile', compact('user','countries','states','cities','activeTab'));
    }

    public function updateProfile(Request $request, $id) {

        $request->validate([
            'profilepic' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        // dd($request->all());
        $user = \Auth::user();
        
        $user->firstname        =   $request->firstname;
        $user->lastname         =   $request->lastname;
        $user->name             =   $request->firstname .' '.$request->lastname;
        $user->contactno     = $request->contactno;
        //$user->ageGroup     = $request->ageGroup;
       // $user->address      = $request->address;
        $user->country      = $request->country;
        $user->state        = $request->state;
        $user->city         = $request->city;
        $user->gender       = $request->gender;
        $user->dob          = $request->dob;
        $user->locality          = $request->locality;

        
        $image = $request->file('profilepic');
        if($request->file('profilepic')) {
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('profiles/'), $new_name);
            $user->profilepic       =   $new_name;
        }
        
        
        $user->save();

        return redirect()->route('profile.update')->with('user', $user)->with('success', 'Profile Updated Successfully.');
    }

    public function myprofileView() {
        
        $user = Auth::user();

        //dd(Auth::user()->id,$userDoc, $bank_document, $aadhar_document, $pan_document);
        $activeTab = "profile";
        return view('admin.users.my-profile')->with('user', $user)->with('activeTab',$activeTab);
    }
    

    /*admin*/

    public function amyprofileView() {
        
        $user = Auth::user();

        //dd(Auth::user()->id,$userDoc, $bank_document, $aadhar_document, $pan_document);
        $activeTab = "profile";
        return view('admin.users.my-profile2')->with('user', $user)->with('activeTab',$activeTab);
    }

    public function aeditProfileForm(Request $request) {
        
        $user = \Auth::user();
        $countries = DB::table('country')->get();
        $states = DB::table('state')->get();
        $cities = DB::table('city')->get();
        // dd($user);
        $activeTab = "my_profile";
        return view('admin.users.edit-profile2', compact('user','countries','states','cities','activeTab'));
    }

    public function aupdateProfile(Request $request, $id) {

         $request->validate([
            'profilepic' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        // dd($request->all());
        $user = \Auth::user();
        
        $user->firstname        =   $request->firstname;
        $user->lastname         =   $request->lastname;
        $user->name             =   $request->firstname .' '.$request->lastname;
        $user->contactno     = $request->contactno;
        //$user->ageGroup     = $request->ageGroup;
       // $user->address      = $request->address;
        $user->country      = $request->country;
        $user->state        = $request->state;
        $user->city         = $request->city;
        $user->gender       = $request->gender;
        $user->dob          = $request->dob;
        $user->locality          = $request->locality;

        
        $image = $request->file('profilepic');
        if($request->file('profilepic')) {
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('profiles/'), $new_name);
            $user->profilepic       =   $new_name;
        }
        
        $user->save();
        

        return redirect()->route('aprofile.update')->with('user', $user)->with('success', 'Profile Updated Successfully.');
    }
     /*admin*/
    /*retailer*/

     public function reditProfileForm(Request $request) {
        
        $user = \Auth::user();
        $countries = DB::table('country')->get();
        $states = DB::table('state')->get();
        $cities = DB::table('city')->get();
        // dd($user);
        $activeTab = "my_profile";
        return view('admin.retailer.edit-profile2', compact('user','countries','states','cities','activeTab'));
    }

    

     public function rupdateProfile(Request $request, $id) {

         $request->validate([
            'profilepic' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        // dd($request->all());
        $user = \Auth::user();
        
        $user->firstname        =   $request->firstname;
        $user->lastname         =   $request->lastname;
        $user->name             =   $request->firstname .' '.$request->lastname;
        $user->contactno     = $request->contactno;
        //$user->ageGroup     = $request->ageGroup;
       // $user->address      = $request->address;
        $user->country      = $request->country;
        $user->state        = $request->state;
        $user->city         = $request->city;
        $user->gender       = $request->gender;
        $user->dob          = $request->dob;
        $user->locality          = $request->locality;

        
        $image = $request->file('profilepic');
        if($request->file('profilepic')) {
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('profiles/'), $new_name);
            $user->profilepic       =   $new_name;
        }
        
        $user->save();
        

        return redirect()->route('rprofile.update')->with('user', $user)->with('success', 'Profile Updated Successfully.');
    }

    

    public function rmyprofileView() {
        
        $user = Auth::user();

        //dd(Auth::user()->id,$userDoc, $bank_document, $aadhar_document, $pan_document);
        $activeTab = "profile";
        return view('admin.retailer.my-profile2')->with('user', $user)->with('activeTab',$activeTab);
    }

    /*retailer*/

    

    public function searchParent(Request $request) {
        if($request->ajax()) {
            $str = $request->input('parent_user');
            $parent_query = DB::table('users')
                                ->whereNotIn('roleid', [6,7])
                                ->whereNotIn('id',[$request->input('id')])
                                ->where('email','like','%'.$str.'%')
                                ->get();
            $response = array();
            if(!empty($parent_query)) {
                //echo json_encode($parent_query);
                foreach($parent_query as $user){
                    $response[] = array("value"=>$user->id,"label"=>$user->email."(".$user->name.")",'firstname' => $user->name,'lastname'=>$user->name);
                }
                echo json_encode($response);
                exit;
            }
        }
    }



    public function getParent($id) {
        $UserList = User::whereIn('roleid',[1,2,3,4,5])->where('status',1)->get()->toArray();
        $ParentList = array();
        $arr = array_column($UserList, "id");
        $Parents = User::whereIn('id',$arr)->where('id','=',$id)->first();
        if($Parents != "") {
            array_push($this->ParentList,$Parents->parentid);
            $this->getParent($Parents->parentid);
            return $this->ParentList;
        } else {
            return [];
        }   
    }

    public function getParentList($id) {    
        $Parents = current(array_filter($this->UsersList, function($e) use($id) { return $e['id']==$id; }));
        if($Parents != "") {
            array_push($this->ParentList,$Parents);
            $this->getParentList($Parents['parentid']);
            return $this->ParentList;
        } else {
            return [];
        }
    }

    
    public function view($id) {
        $user = User::find($id);
        if($user != null) {
            $user->country= DB::table('country')
                            ->where('id','=',$user->country)
                            ->first()->name;
                            
        $user->state= DB::table('state')
        ->where('id','=',$user->state)
        ->first()->name;
        $user->city= DB::table('city')
        ->where('id','=',$user->city) 
        ->first()->name;
        }
        return view('front_partials.user-details-view', compact('user'));
    }

    

    public function showPassword() {
        return view('front_partials.password-modal');
    }

     public function show_kyc_info() {
        if(get_user_permission("my_profile","view") == false) {
            return redirect(403);
        }
        $user = Auth::user();
        $userDoc = UserDocuments::where('userid', Auth::user()->id)->first();
        $bank_document      =   DocumentFiles::where('userid', Auth::user()->id)->where('doctype', 'bank_document')->first();
        $aadhar_document    =   DocumentFiles::where('userid', Auth::user()->id)->where('doctype', 'aadhar_document')->first();
        $pan_document       =   DocumentFiles::where('userid', Auth::user()->id)->where('doctype', 'pan_document')->first();
        $activeTab = "kyc_info";
        return view('admin.retailer.my-profile2')->with('user', $user)
                                        ->with('userDoc', $userDoc)
                                        ->with('bank_document', $bank_document)
                                        ->with('aadhar_document', $aadhar_document)
                                        ->with('pan_document', $pan_document)
                                        ->with('activeTab',$activeTab);
    }



    public function show_kyc_history() {
        if(get_user_permission("my_profile","view") == false) {
            return redirect(403);
        }
        $user = Auth::user();
        $userDocInfo = UserRequestStatus::with('getUsername')->where('userid', Auth::user()->id)->get();
        $activeTab = "kyc_history";
        return view('admin.retailer.my-profile2')->with('user', $user)
                                        ->with('userDocInfo', $userDocInfo)
                                        ->with('activeTab',$activeTab);
    }

    

    public function AllCloseAccountRequest() {
         
        $user = Auth::user();
        $activeTab = 'manage_allcloseaccountrequest';
        
        
         try {
    
        
        $users  = User::Where('close_account_req','1')->get();
        
        //echo '<pre>'; print_r($subscription); echo '</pre>'; exit;
        return view('admin/retailer/product_request/close_account_request', compact('users','activeTab'));
        
        } catch (Exception $e) {
                Log::error("Failed to load manage close cccount request data -> ".$e);
            }
        
        
    }

    public function CloseAccountRequestStatus(Request $request) {
        $status = $request->status;
        $id = $request->id;
        

        if($id != "" && $status != "") {
            try {
                
            $product = User::where(['id'=>$id])->update(['close_account_req' => $status ]);
            
                    //$user_membership = Usermembership::where(['id'=>$id])->update(['status' => $status ],['date' => date('Y-m-d')])
                    if($product) {
                        return response()->json(['result' => 'success']);
                    }
                    return response()->json(['result' => 'fail']);
               
            } catch (Exception $e) {
                Log::error("Failed to change product status -> ".$e);
            }
        }   
    }

    public function AdminCloseAccountRequestStatus(Request $request) {
        $status = $request->status;
        $id = $request->id;
        

        if($id != "" && $status != "") {
            try {



             if($status == "2")
             {
                   $product = User::where(['id'=>$id])->update(['close_account_req' => $status,'status' => '0'  ]);
             }else
             {
                $product = User::where(['id'=>$id])->update(['close_account_req' => $status ]);
             }
                
            
            
                    //$user_membership = Usermembership::where(['id'=>$id])->update(['status' => $status ],['date' => date('Y-m-d')])
                    if($product) {
                        return response()->json(['result' => 'success']);
                    }
                    return response()->json(['result' => 'fail']);
               
            } catch (Exception $e) {
                Log::error("Failed to change product status -> ".$e);
            }
        }   
    }

}