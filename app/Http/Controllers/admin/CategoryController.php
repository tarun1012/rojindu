<?php

namespace App\Http\Controllers\admin;

use App\Category;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_category';
        $BannersData = Category::all();
        return View('admin/category/index',compact('CategoryData','activeTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createBanner()
    {
        $activeTab = 'manage_banners';
        return View('banner/create',compact('activeTab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBanner(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $banner = new Banner;
        $banner->title = $request->input('title');
        $banner->name = $request->input('name');
        $banner->content = $request->input('banner-content');
        $banner->link = $request->input('link');
        $banner->status = $request->input('status') != "" ? $request->input('status') : 0;
        $banner->created_at = $date;

        /**
         * Banner Upload Script.
         */
        $file1 = $request->file('image');
        $filename1=$file1->getClientOriginalName();
        $file1->move(public_path().'/uploads/banner/', $filename1);
        $banner->image = $filename1;

        if(!$banner->save()) {
            //Redirect error
            return redirect()->route('banner')->with('error','Failed to update banner details.');
        }

        //Redirect success
        return redirect()->route('banner')->with('success','Banner added successfully.');
    
    }

    public function changeBannerStatus(Request $request) {
        $banner_id = $request->banner_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($banner_id)) {
            $banner = Banner::where('id',$banner_id)
                                ->update(['status' => $status]);

            if($banner) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editBanner($id)
    {
        $activeTab = 'manage_banners';
        $Banner = Banner::find($id);
        return View('banner/create',compact('Banner','activeTab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateBanner(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $banner = Banner::find($id);
            $banner->title = $request->input('title');
            $banner->name = $request->input('name');
            $banner->content = $request->input('banner-content');
            $banner->link = $request->input('link');
            $banner->status = $request->input('status') != "" ? $request->input('status') : 0;
            $banner->updated_at = $date;

            /**
             * Banner Upload Script.
             */
            if($request->hasFile('image')) {
                $file1 = $request->file('image');
                $filename1=$file1->getClientOriginalName();
                $file1->move(public_path().'/uploads/banner/', $filename1);
                
                $old_banner = $request->input("banner_path");

                if(file_exists(public_path().'\uploads\banner\\'.$old_banner)) {
                    unlink('uploads\banner\\'.$old_banner);
                }

                $banner_img = Banner::where('id',$id)
                                ->update(['image' => $filename1]);

                if(!$banner_img) {
                    Log::error("Banner not uploaded.");
                }
            }

            if($banner->save()) {
                //Redirect success
                return redirect()->route('banner')->with('success','Banner updated successfully.');
            }

            //Redirect error
            return redirect()->route('banner')->with('error','Failed to update banner.');


        } catch (Exception $e) {
            Log::error("Failed to update banner.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyBanner($id)
    {   
        
    }
}
