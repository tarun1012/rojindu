<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_category';
        $CategoryData = Category::all();
        return View('admin/category/index2',compact('CategoryData','activeTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activeTab = 'manage_category';
        return View('admin/category/create',compact('activeTab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $category = new Category;
        $category->name = $request->input('name');
        $category->status = $request->input('status') != "" ? $request->input('status') : 0;
        $category->created_at = $date;

        /**
         * category Upload Script.
         */
        $file1 = $request->file('image');
        $filename1=$file1->getClientOriginalName();
        $file1->move(public_path().'/uploads/category/', $filename1);
        $category->image = $filename1;

        if(!$category->save()) {
            //Redirect error
            return redirect()->route('category')->with('error','Failed to update category details.');
        }

        //Redirect success
        return redirect()->route('category')->with('success','category added successfully.');
    
    }

    public function changeCategoryStatus(Request $request) {
        $category_id = $request->category_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($category_id)) {
            $category = Category::where('id',$category_id)
                                ->update(['status' => $status]);

            if($category) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activeTab = 'manage_category';
        $category = Category::find($id);
        return View('admin/category/create',compact('category','activeTab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $category = Category::find($id);
            $category->name = $request->input('name');
            $category->status = $request->input('status') != "" ? $request->input('status') : 0;
            $category->updated_at = $date;

            /**
             * Banner Upload Script.
             */
            if($request->hasFile('image')) {
                $file1 = $request->file('image');
                $filename1=$file1->getClientOriginalName();
                $file1->move(public_path().'/uploads/category/', $filename1);
                
                $old_banner = $request->input("category_path");

                if(file_exists(public_path().'\uploads\category\\'.$old_banner)) {
                    unlink('uploads\category\\'.$old_banner);
                }

                $category_img = Category::where('id',$id)
                                ->update(['image' => $filename1]);

                if(!$category_img) {
                    Log::error("category not uploaded.");
                }
            }

            if($category->save()) {
                //Redirect success
                return redirect()->route('category')->with('success','Category updated successfully.');
            }

            //Redirect error
            return redirect()->route('category')->with('error','Failed to update category.');


        } catch (Exception $e) {
            Log::error("Failed to update category.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyBanner($id)
    {   
        
    } 

     public function changeCategoryStatus_datatable(Request $request, $id) {
        $Category = Category::find($id);

        if($Category->status == 1) {
            $Category->status = 0;
        }elseif($Category->status == 0) {
            $Category->status = 1;
        }
        $Category->save();

        return redirect()->route('category')->with('success', 'Category Status Updated');

         
        

    }

    public function allCategory(Request $request)
    {
        
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            3=> 'image',
                            4=> 'status',
                        );
  
        $totalData = Category::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = Category::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts = Category::where('name','LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                         ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
            

            $totalFiltered = Category::where('name','LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                         ->orWhere('content', 'LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('category.edit',$post->id); 
                $edit = route('category.edit', $post->id);
                $url =  asset('uploads/category/'.$post->image);
                
                $st = route('change.category_datatable.status', $post->id);

                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->name;
                $nestedData['image'] = '<img src="' . $url . '" style="width: 200px !important;" alt="' . $post->name. '">';
                
                //$nestedData['status'] = $post->status;
                if($post->status == '1')
                {
                    $nestedData['status'] = '<span class="badge badge-success">Active</span>';
                }else
                {
                    $nestedData['status'] = '<span class="badge badge-danger">Paused</span>';
                }

                $options = "";

                if($post->status == '1')
                {
                    

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-warning" data-toggle="tooltip" title="Pause Category"> <i class="fa fa-power-off"></i> </a>&nbsp;';
                }else
                {
                   // $options = '<button type="button" id="changeProductStatus" class="btn btn-rounded btn-sm btn-success changeProductStatus" data-product_id="' . $post->id . '" data-status="' . $post->status . '" data-toggle="tooltip" title="Activate product"><i class="fa fa-refresh"></i></button>&nbsp;';

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-success changeCategoryStatus" data-toggle="tooltip" title="Activate Category"> <i class="fa fa-refresh"></i> </a>&nbsp;';
                }
                 
                 if(get_user_permission("manage_category","edit"))
                {
                 $options .= '<a href="'.$edit.'" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit Category"><i class="fa fa-edit"></i></a>';
                 }

                $nestedData['options'] = $options;
                
                /*$nestedData['body'] = substr(strip_tags($post->detail),0,50)."...";
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";*/
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
        
    }
}
