<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DocumentFiles;
use App\UserDocuments;
use App\UserRequestStatus;
use Auth;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;

class BankVerificationController extends Controller
{
    public function editBankDetail() {
        $Docs = UserDocuments::where('userid','=',Auth::user()->id)->first();
        $BankDoc = DocumentFiles::where('userid','=',Auth::user()->id)->where('doctype','=','bank_document')->first();
        $AadharDoc = DocumentFiles::where('userid','=',Auth::user()->id)->where('doctype','=','aadhar_document')->first();
        $PanDoc = DocumentFiles::where('userid','=',Auth::user()->id)->where('doctype','=','pan_document')->first();
        return view('admin.retailer.bank-edit',compact('Docs','BankDoc','AadharDoc','PanDoc'));
    }

    public function kycValidation($request) {
        $request->validate([
            "bank_name"         => "required",
            "account_name"      => "required",
            "account_number"    => "required",
            "ifsc_code"         => "required:",
            "aadhar_name"       => "required",
            "aadhar_number"     => "required",
            "pan_name"          => "required",
            "pan_number"        => "required:",
            "bank_document"     => "required|max:3",
            "aadhar_document"   => "required|max:2",
            "pan_document"      => "required|max:2"
        ]);
    }

    public function updateBankdetail(Request $request) {
        
        

        $userDoc    =    UserRequestStatus::where('userid', Auth::user()->id)->get();

        if(!$userDoc->isEmpty()) {
            
            foreach($request->all() as  $key => $files) {
                
                if($key == "bank_document") {
                    
                    $row    =    UserRequestStatus::where('fieldname', 'bank_document')
                                            ->where('userid', Auth::user()->id)
                                            ->first();
                    
                    $bankDoc = $this->uploadKYCdocuments("bank_document", $files);
                    UserRequestStatus::where('fieldname', 'bank_document')
                                    ->where('id', $row->id)
                                    ->update([
                                        'newvalue'          =>    json_encode($bankDoc),
                                        'oldvalue'          =>    $row->newvalue,
                                        'status'            =>    'pending',
                                        'verifiedby'        =>    null,
                                        'admincomment'      =>    null
                                    ]);
                    
                }else if($key == "aadhar_document") {
                    
                    $row    =    UserRequestStatus::where('fieldname', 'aadhar_document')
                                            ->where('userid', Auth::user()->id)
                                            ->first();

                    $aadharDoc = $this->uploadKYCdocuments("aadhar_document", $files);
                    UserRequestStatus::where('fieldname', 'aadhar_document')
                                    ->where('id', $row->id)
                                    ->update([
                                        'newvalue'          =>   json_encode($aadharDoc),
                                        'oldvalue'          =>   $row->newvalue,
                                        'status'            =>   'pending',
                                        'verifiedby'        =>   null,
                                        'admincomment'      =>   null
                                    ]);

                }else if($key == "pan_document") {
                    
                    $row    =    UserRequestStatus::where('fieldname', 'pan_document')
                                            ->where('userid', Auth::user()->id)
                                            ->first();

                    $panDoc = $this->uploadKYCdocuments("pan_document", $files);
                    UserRequestStatus::where('fieldname', 'pan_document')
                                    ->where('id', $row->id)
                                    ->update([
                                        'newvalue'          =>    json_encode($panDoc),
                                        'oldvalue'          =>    $row->newvalue,
                                        'status'            =>    'pending',
                                        'verifiedby'        =>    null,
                                        'admincomment'      =>    null
                                    ]);

                }
                $this->UpdateKYCinfo($userDoc, $request);
            }
            return redirect('admin/dashboard/retailer/myprofile-show-kyc-info')->with('success', 'Thanks for Update Rejected Documents.');
        }
        $this->kycValidation($request);
        $fieldTotal = count($request->all()) - 1;
        

        foreach($request->all() as $field => $fieldValue) {
            
            if($field == "_token") {
                continue;
            }

            if(is_array($fieldValue)) {
                $filesNameArray = $this->uploadKYCdocuments($field, $fieldValue);
                
                // foreach($filesNameArray as $fileName) {
                    $this->savefieldsInfo($field, json_encode($filesNameArray), $request, 'file'); 
                // }
            }else{
                $this->savefieldsInfo($field, $fieldValue, $request, 'doc');
            }
        }

        return redirect('admin/dashboard/retailer/myprofile-show-kyc-info')->with('success', 'Document Under Review.');
    }

    public function UpdateKYCinfo($userDoc, $request) {
        
        foreach($userDoc as $key => $row) {
            
            if($row->type == "doc") {
                $userInfo = UserRequestStatus::where('groupid', $row->groupid)->first();
                if(!empty($request->get($row->fieldname)) && $userInfo->newvalue != $request->get($row->fieldname)) {
                    $userInfo->oldvalue     =   $userInfo->newvalue;
                    $userInfo->newvalue     =   $request->get($row->fieldname);
                    $userInfo->status       =   'pending';
                    $userInfo->verifiedby   =   null;
                    $userInfo->admincomment =   null;

                    $userInfo->save();
                }
            }
            
            $userInfo->save();
        }

    }

    public function uploadKYCdocuments($field, $files) {
        // dd($field, $files);
        $arrfileName = [];
        foreach($files as $key => $file)
        {
            $name = $field.$key.'_'.time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('documents/'), $name);
            array_push($arrfileName, $name);
        }
        return $arrfileName;
    }

    public function savefieldsInfo($field, $fieldValue, $request, $type) {
        
        $userRequest  =   new UserRequestStatus;

        $userRequest->userid            =   Auth::user()->id;
        $userRequest->fieldname         =   $field;
        $userRequest->newvalue          =   $fieldValue;
        $userRequest->oldvalue          =   $request->old($field);
        $userRequest->status            =   'pending';
        $userRequest->type              =   $type;
        $userRequest->groupid           =   uuid();
        $userRequest->FromWeb           =   1;

        $userRequest->save();
    }

    public function kycDocVerification() {
        $PendingRequest = UserRequestStatus::where('status','=','pending')->with('getUsername')->get();
        $RejectedRequest = UserRequestStatus::where('status','=','rejected')->with('getUsername')->get();
        //$userDocInfo = UserRequestStatus::with('getUsername')->get();
        $activeTab = "kycDocVerification";
    return view('admin.users.kyc-verification-table',compact('PendingRequest','RejectedRequest','activeTab'));
    }

    public function documentRejectionComment(Request $request) {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'request_id'    =>  'required',
            'comment'       =>  'required'
        ]);

        if ($validator->fails()) {
            return \Response::json(array(
                'error'=>$validator->errors()->all(),
                'codes'  => 201
            ));
        }
        
        $reqState = UserRequestStatus::find($request->request_id);
        
        $reqState->admincomment     =   $request->comment;
        $reqState->verifiedby       =   Auth::user()->username;
        $reqState->status           =   'rejected';
        $reqState->save();

        return response()->json(['status' => 'success', 'code' => 200]);
    }

    public function documentVerifiedRequest(Request $request, $id) {
        // dd($request->all(), $id);

        if($id) {
            $reqInfo = UserRequestStatus::find($id);
        
            $reqInfo->status        =   'approved';
            $reqInfo->verifiedby    =   Auth::user()->username;
            $reqInfo->admincomment  =   null;
            $reqInfo->save();

            $verifiedRow    =   UserDocuments::where('userid', $reqInfo->userid);

            if($reqInfo->type == 'doc') {
                $verifiedRow->update([
                                $reqInfo->fieldname   =>  $reqInfo->newvalue
                            ]);
            }elseif($reqInfo->type == 'file') {

                $docUpdate =    new DocumentFiles;
                $docUpdate->userid      =   $reqInfo->userid;
                $docUpdate->doctype     =   $reqInfo->fieldname;
                $docUpdate->image       =   $reqInfo->newvalue;
                $docUpdate->save();
            }
            $verifyStatus   =   $this->finalVerificationCheck();
            
            if($verifyStatus == true) {
                return redirect()->route('admin.document.varification')->with('success', 'All This User KYC Approved.');    
            }
            return redirect()->route('admin.document.varification')->with('success', 'Approved Document Field.');
        }else{
            return redirect()->back()->with('error', 'Invalid Direction');
        }
        
    }

    public function finalVerificationCheck() {
            
        $userDoc = UserDocuments::where('userid', Auth::user()->id)->first();
        $checkVerifyStatus = [];
        
        if(!empty($userDoc->bank_proof_type) && ($userDoc->bank_proof_type != null)) {
            array_push($checkVerifyStatus, 'bank_proof_type');
        }
        if(!empty($userDoc->bank_name) && ($userDoc->bank_name != null)) {
            array_push($checkVerifyStatus, "bank_name");
        }
        if(!empty($userDoc->account_name) && ($userDoc->account_name != null)) {
            array_push($checkVerifyStatus, "account_name");
        }
        if(!empty($userDoc->account_number) && ($userDoc->account_number != null)) {
            array_push($checkVerifyStatus, "account_number");
        }
        if(!empty($userDoc->ifsc_code) && ($userDoc->ifsc_code != null)) {
            array_push($checkVerifyStatus, "ifsc_code");
        }
        if(!empty($userDoc->aadhar_name) && ($userDoc->aadhar_name != null)) {
            array_push($checkVerifyStatus, "aadhar_name");
        }
        if(!empty($userDoc->aadhar_number) && ($userDoc->aadhar_number != null)) {
            array_push($checkVerifyStatus, "aadhar_number");
        }
        if(!empty($userDoc->pan_name) && ($userDoc->pan_name != null)) {
            array_push($checkVerifyStatus, "pan_name");
        }
        if(!empty($userDoc->pan_number) && ($userDoc->pan_number != null)) {
            array_push($checkVerifyStatus, "pan_number");
        }

        if(count($checkVerifyStatus) >= 9) {
            $userDoc->isverified = 1;
            $userDoc->save();
            return true;
        }else{
            return false;
        }
        
    }


}
