<?php

namespace App\Http\Controllers;

use App\Product;
use App\RetailerProduct;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use DB;
use Illuminate\Support\Facades\Log;

class RetailerProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function productrequest()
    {
        $activeTab = 'manage_retailerproduct';
        $RetailerProductData = RetailerProduct::join('products','retailer_product.productid','=','products.id')->join('category','products.category_id','=','category.id')->join('subcategory','products.subcategory_id','=','subcategory.id')->join('brand','products.brand_id','=','brand.id')->select('retailer_product.*','products.name as product_name','category.name as category_name','subcategory.name as subcategory_name','brand.name as brand_name')->get();
        return View('admin/retailer/product/index',compact('RetailerProductData','activeTab'));
    }

    public function index(){
        $activeTab = 'manage_retailerproduct';
        $user = Auth::user();
        $userid = $user->id;
        $RetailerProductData =  DB::select( DB::raw("SELECT p.*, rp.price as rprice,rp.id as rid,rp.qty,c.name as category_name,sc.name as subcategory_name,b.name as brand_name
        FROM products p 
        JOIN category c ON p.category_id = c.id
        JOIN subcategory sc ON p.subcategory_id = sc.id
        JOIN brand b ON p.brand_id = b.id
        LEFT JOIN retailer_product rp
        ON p.id = rp.productid AND rp.userid = '$userid'") );
        return View('admin/retailer/product/index3',compact('RetailerProductData','activeTab'));
        //return view('admin/retailer/product/index2');
      }

  // Fetch records
  public function getUsers(){
    // Call getuserData() method of Page Model
    $userData['data'] = RetailerProduct::getuserData();

    echo json_encode($userData);
    exit;
  }

 

  // Update record
  public function updateRetailerProduct(Request $request){

    $rprice = $request->input('rprice');
    $productid = $request->input('productid');
    $qty = $request->input('qty');
    $editid = $request->input('editid');
    $user = Auth::user();
    $userid = $user->id;
    $date = date('Y-m-d h:i:s');

    if($rprice !='' && $qty != '' ){
      $data = array('price'=>$rprice,"qty"=>$qty,"productid"=>$productid,"userid"=>$userid);

      // Call updateData() method of Page Model
      RetailerProduct::updateData($editid, $data);
      echo 'Update successfully.';
    }else{
      echo 'Fill all fields.';
    }

    exit; 
  }

  public function updateRetailerProduct1(Request $request)
  {
         
   
    
        foreach ($request->addmore as $key => $value)
         {

            $request->validate([
            'addmore.*.rprice' => 'required|lte:addmore.*.price',
            'addmore.*.qty' => 'required',
            
        ],['addmore.*.rprice.lte' => 'Retailer price  must be not greater than product price.']);

            $rprice = $value['rprice'];
            
            $productid = $value['productid'];
            $qty = $value['qty'];
            $editid = $value['productid'];
            $user = Auth::user();
            $userid = $user->id;
            //$date = date('Y-m-d h:i:s');

            
              $data = array('price'=>$rprice,"qty"=>$qty,"productid"=>$productid,"userid"=>$userid);

              // Call updateData() method of Page Model
              RetailerProduct::updateData($editid, $data);
              echo 'Update successfully.';
            
        }
    
     return back()->with('success', 'Record Created Successfully.');

     
  }

  // Delete record
  public function deleteUser($id=0){
    // Call deleteData() method of Page Model
    Page::deleteData($id);

    echo "Delete successfully";
    exit;
  }
    
    function str_random($length = 16)
    {
        return Str::random($length);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activeTab = 'manage_retailerproduct';
        $products = DB::table("products")->get();
        
        return View('admin/retailer/product/create',compact('products','activeTab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $user = Auth::user();
        $product = new RetailerProduct;
        $product->qty = $request->input('qty');
        $product->price = $request->input('price');
        $product->productid = $request->input('product');
        $product->userid = $user->id;
        
        $product->status = $request->input('status') != "" ? $request->input('status') : 0;
        $product->created_at = $date;
        
        if(!$product->save()) {
            //Redirect error
            return redirect()->route('retailerproduct')->with('error','Failed to retailer update product.');
        }

        

        if(!$product->save()) {
            //Redirect error
            return redirect()->route('retailerproduct')->with('error','Failed to update retailer product.');
        }

         

        //Redirect success
        return redirect()->route('retailerproduct')->with('success','retailer product added successfully.');
    
    }

    public function changeRetailerProductStatus(Request $request) {
        $id = $request->id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($id)) {
            $product = RetailerProduct::where('id',$id)
                                ->update(['status' => $status]);

            if($product) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

     

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activeTab = 'manage_retailerproduct';
        $products = DB::table("products")->get();
        
        $retailerproduct = RetailerProduct::find($id);
        return View('admin/retailer/product/create',compact('products','activeTab','retailerproduct'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $product = RetailerProduct::find($id);
            $product->qty = $request->input('qty');
            $product->productid = $request->input('product');
            $product->price = $request->input('price');
            
            $product->status = $request->input('status') != "" ? $request->input('status') : 0;
            $product->updated_at = $date;

            
            if($product->save()) {
                //Redirect success
                return redirect()->route('retailerproduct')->with('success','Retailer Product updated successfully.');
            }

            //Redirect error
            return redirect()->route('retailerproduct')->with('error','Failed to update retailer product.');


        } catch (Exception $e) {
            Log::error("Failed to update retailer product.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
    }

    
}
