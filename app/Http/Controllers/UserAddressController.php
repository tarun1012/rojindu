<?php

namespace App\Http\Controllers;

use App\UserAddress;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\TwoFactor;


use DB;
use Illuminate\Support\Facades\Log;

class UserAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_address';
         $user = Auth::user();
        $AddressData = UserAddress::join('country','user_address.country','=','country.id')->join('state','user_address.state','=','state.id')->join('city','user_address.city','=','city.id')->select('user_address.*','country.name as country_name','state.name as state_name','city.name as city_name')->where('user_address.userid',$user->id)->get();
        return View('admin/address/index',compact('AddressData','activeTab'));
    }
    
    function str_random($length = 16)
    {
        return Str::random($length);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activeTab = 'manage_address';
        $country = DB::table("country")->get();
        $state = DB::table("state")->get();
        $city = DB::table("city")->get();
        return View('admin/address/create',compact('country','state','activeTab','city'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $user = Auth::user();
        $address = new UserAddress;
        $address->userid = $user->id;
        $address->address = $request->input('address');
        $address->country = $request->input('country');
        $address->state = $request->input('state');
        $address->city = $request->input('city');
        $address->locality = $request->input('locality');
        $address->pincode = $request->input('pincode');
        $address->status = $request->input('status') != "" ? $request->input('status') : 0;
         $address1 = UserAddress::where('userid',$user->id)->where('is_default','=','1');
         if ($address1 -> count() > 0) {
            $address->is_default = 0;
        }else{
            $address->is_default = 1;
        }
        $address->created_at = $date;

        





        if(!$address->save()) {
            //Redirect error
            return redirect()->route('address')->with('error','Failed to update address.');
        }


        

        //Redirect success
        return redirect()->route('address')->with('success','address added successfully.');
    
    }

    public function changeAddressStatus(Request $request) {
        $address_id = $request->address_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($address_id)) {
            $address = UserAddress::where('id',$address_id)
                                ->update(['status' => $status]);

            if($address) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }

    public function changeAddressD(Request $request) {
        $address_id = $request->address_id;
        $status = ($request->is_default == 1 ? 0 : 1);
        if(!empty($address_id)) {
            $address = UserAddress::where('id',$address_id)
                                ->update(['is_default' => $status]);
            if($request->is_default == '0'){
               $address1 = UserAddress::where('id','!=',$address_id)
                                    ->update(['is_default' => '0']);
                            }

            if($address) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    public function getCityList(Request $request) {
        $city = DB::table("city")
            ->where("stateid",$request->c_id)
            ->pluck("name","id");
            return response()->json($city);
    }
     public function getStateList(Request $request) {
        $city = DB::table("state")
            ->where("countryid",$request->c_id)
            ->pluck("name","id");
            return response()->json($city);
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activeTab = 'manage_address';
        $country = DB::table("country")->get();
        $state = DB::table("state")->get();
        $city = DB::table("city")->get();
        
        $address = UserAddress::find($id);
        return View('admin/address/create',compact('state','activeTab','country','address','city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $address = UserAddress::find($id);
            $address->address = $request->input('address');
            $address->country = $request->input('country');
            $address->state = $request->input('state');
            $address->city = $request->input('city');
            $address->locality = $request->input('locality');
            $address->pincode = $request->input('pincode');
            $address->status = $request->input('status') != "" ? $request->input('status') : 0;
            $address->updated_at = $date;


            if($address->save()) {
                //Redirect success
                return redirect()->route('address')->with('success','Address updated successfully.');
            }

            //Redirect error
            return redirect()->route('address')->with('error','Failed to update address.');


        } catch (Exception $e) {
            Log::error("Failed to update address.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
    }

    
}
