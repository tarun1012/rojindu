<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\Role;
use App\Modules;
use DB;

class PermissionController extends Controller
{
    public function showPermissions() {
        $Permissions =  DB::select("
            select tm.id as moduleId,tm.name as moduleName,tm.status as moduleStatus,
            tr.id as roleId,tr.name as roleName,
            tp.*
            from modules as tm
            cross join roles as tr
            left join permissions as tp
            on tm.id=tp.moduleid and tr.id=tp.roleid where tm.status = 1 and tr.id = 1
        ");
        $activeTab = 'manage_permissions';
        
        $get_roles = DB::table('roles')->get();
    
        return view('admin.permissions.index',compact('Permissions','activeTab','get_roles'));
    }

    public function fetchPermission(Request $request) {
        $RolePermissions =  DB::select("
            select tm.id as moduleId,tm.name as moduleName,tm.status as moduleStatus,
            tr.id as roleId,tr.name as roleName,
            tp.*
            from modules as tm
            cross join roles as tr
            left join permissions as tp
            on tm.id=tp.moduleid and tr.id=tp.roleid where tm.status = 1 and tr.id = '".$request->roleId."'
        ");
        $current_role = $request->roleId;
        return view('admin.permissions.index',compact('RolePermissions','current_role'));
    }

    public function addPermission(Request $request) {
        if($request->roleId != "" && $request->moduleId != "" && $request->value != "" && $request->type != "") {
            
            $roleId = $request->roleId;
            $moduleId = $request->moduleId;
            $value = $request->value;
            $type = $request->type;
            
            $check_perm = Permission::where(['roleid'=>$roleId,'moduleid'=>$moduleId])->get();

            //Check if permission type is add
            if($type == "add") {
                /**
                 * Value = 1 Then insert "add" permission
                 * Value = 0 Then update "add" permission
                 */                
                if($value == 1 && count($check_perm) < 1) {
                    $permission = new Permission;
                    
                    $permission->roleid = $roleId;
                    $permission->moduleid = $moduleId;
                    $permission->add = $value;
                    $permission->edit = null;
                    $permission->delete = null;
                    $permission->view = null;

                    if($permission->save()) {
                        echo "success";
                    } else {
                        echo "fail";
                    } 

                } else if($value == 0) {
                    
                    $data = array(
                        'add' => $value
                    );

                    $permission = Permission::where([
                        'roleid' => $roleId,
                        'moduleid' => $moduleId
                    ])->update($data);

                    if($permission) {
                        echo "success";
                    } else {
                        echo "fail";
                    }
                } else if($value == 1 && count($check_perm) > 0) {
                    
                    $data = array(
                        'add' => $value
                    );

                    $permission = Permission::where([
                        'roleid' => $roleId,
                        'moduleid' => $moduleId
                    ])->update($data);

                    if($permission) {
                        echo "success";
                    } else {
                        echo "fail";
                    }
                }

                
            } 
            
            if($type == "edit") {
                /**
                 * Value = 1 Then insert "edit" permission
                 * Value = 0 Then update "edit" permission
                 */
                if($value == 1 && count($check_perm) < 1) {
                    $permission = new Permission;
                    
                    $permission->roleid = $roleId;
                    $permission->moduleid = $moduleId;
                    $permission->add = null;
                    $permission->edit = $value;
                    $permission->delete = null;
                    $permission->view = null;

                    if($permission->save()) {
                        echo "success";
                    } else {
                        echo "fail";
                    } 
                } else if($value == 0) {
                    
                    $data = array(
                        'edit' => $value
                    );

                    $permission = Permission::where([
                        'roleid' => $roleId,
                        'moduleid' => $moduleId
                    ])->update($data);

                    if($permission) {
                        echo "success";
                    } else {
                        echo "fail";
                    }
                } else if($value == 1 && count($check_perm) > 0) {

                    $data = array(
                        'edit' => $value
                    );

                    $permission = Permission::where([
                        'roleid' => $roleId,
                        'moduleid' => $moduleId
                    ])->update($data);

                    if($permission) {
                        echo "success";
                    } else {
                        echo "fail";
                    }
                }
            }

            if($type == "delete") {
                /**
                 * Value = 1 Then insert "edit" permission
                 * Value = 0 Then update "edit" permission
                 */
                if($value == 1 && count($check_perm) < 1) {
                    $permission = new Permission;
                    
                    $permission->roleid = $roleId;
                    $permission->moduleid = $moduleId;
                    $permission->add = null;
                    $permission->edit = null;
                    $permission->delete = $value;
                    $permission->view = null;

                    if($permission->save()) {
                        echo "success";
                    } else {
                        echo "fail";
                    } 
                } else if($value == 0) {
                    
                    $data = array(
                        'delete' => $value
                    );

                    $permission = Permission::where([
                        'roleid' => $roleId,
                        'moduleid' => $moduleId
                    ])->update($data);

                    if($permission) {
                        echo "success";
                    } else {
                        echo "fail";
                    }
                } else if($value == 1 && count($check_perm) > 0) {

                    $data = array(
                        'delete' => $value
                    );

                    $permission = Permission::where([
                        'roleid' => $roleId,
                        'moduleid' => $moduleId
                    ])->update($data);

                    if($permission) {
                        echo "success";
                    } else {
                        echo "fail";
                    }
                }
            }

            if($type == "view") {
                /**
                 * Value = 1 Then insert "edit" permission
                 * Value = 0 Then update "edit" permission
                 */
                if($value == 1 && count($check_perm) < 1) {
                    $permission = new Permission;
                    
                    $permission->roleid = $roleId;
                    $permission->moduleid = $moduleId;
                    $permission->add = null;
                    $permission->edit = null;
                    $permission->delete = null;
                    $permission->view = $value;

                    if($permission->save()) {
                        echo "success";
                    } else {
                        echo "fail";
                    } 
                } else if($value == 0) {
                    
                    $data = array(
                        'view' => $value
                    );

                    $permission = Permission::where([
                        'roleid' => $roleId,
                        'moduleid' => $moduleId
                    ])->update($data);

                    if($permission) {
                        echo "success";
                    } else {
                        echo "fail";
                    }
                } else if($value == 1 && count($check_perm) > 0) {

                    $data = array(
                        'view' => $value
                    );

                    $permission = Permission::where([
                        'roleid' => $roleId,
                        'moduleid' => $moduleId
                    ])->update($data);

                    if($permission) {
                        echo "success";
                    } else {
                        echo "fail";
                    }
                }
            }
        }
    }
    
}
