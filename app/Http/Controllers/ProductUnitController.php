<?php

namespace App\Http\Controllers;

use App\ProductUnit;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;

class ProductUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_product_unit';
        $ProductUnitData = ProductUnit::all();
        return View('admin/product_unit/index2',compact('ProductUnitData','activeTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activeTab = 'manage_product_unit';
        return View('admin/product_unit/create',compact('activeTab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $product_unit = new ProductUnit;
        $product_unit->name = $request->input('name');
        $product_unit->status = $request->input('status') != "" ? $request->input('status') : 0;
        $product_unit->created_at = $date;

        

        if(!$product_unit->save()) {
            //Redirect error
            return redirect()->route('product_unit')->with('error','Failed to update product unit details.');
        }

        //Redirect success
        return redirect()->route('product_unit')->with('success','product unit added successfully.');
    
    }

    public function changeProductUnitStatus(Request $request) {
        $product_unit_id = $request->product_unit_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($product_unit_id)) {
            $product_unit = ProductUnit::where('id',$product_unit_id)
                                ->update(['status' => $status]);

            if($product_unit) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activeTab = 'manage_product_unit';
        $product_unit = ProductUnit::find($id);
        return View('admin/product_unit/create',compact('product_unit','activeTab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $product_unit = ProductUnit::find($id);
            $product_unit->name = $request->input('name');
            $product_unit->status = $request->input('status') != "" ? $request->input('status') : 0;
            $product_unit->updated_at = $date;

            

            if($product_unit->save()) {
                //Redirect success
                return redirect()->route('product_unit')->with('success','Product unit updated successfully.');
            }

            //Redirect error
            return redirect()->route('product_unit')->with('error','Failed to update product unit.');


        } catch (Exception $e) {
            Log::error("Failed to update product unit.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
    }

    public function changeProductUnitStatus_datatable(Request $request, $id) {
        $ProductUnit = ProductUnit::find($id);

        if($ProductUnit->status == 1) {
            $ProductUnit->status = 0;
        }elseif($ProductUnit->status == 0) {
            $ProductUnit->status = 1;
        }
        $ProductUnit->save();

        return redirect()->route('product_unit')->with('success', 'ProductUnit Status Updated');

         
        

    }

    public function allProductUnit(Request $request)
    {
        
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            3=> 'status',
                        );
  
        $totalData = ProductUnit::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = ProductUnit::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts = ProductUnit::where('name','LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                         ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
            

            $totalFiltered = ProductUnit::where('name','LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                         ->orWhere('content', 'LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('product_unit.edit',$post->id); 
                $edit = route('product_unit.edit', $post->id);
               
                
                $st = route('change.productunit_datatable.status', $post->id);

                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->name;
                
                
                //$nestedData['status'] = $post->status;
                if($post->status == '1')
                {
                    $nestedData['status'] = '<span class="badge badge-success">Active</span>';
                }else
                {
                    $nestedData['status'] = '<span class="badge badge-danger">Paused</span>';
                }

                $options = "";

                if($post->status == '1')
                {
                    

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-warning" data-toggle="tooltip" title="Pause Unit"> <i class="fa fa-power-off"></i> </a>&nbsp;';
                }else
                {
                   // $options = '<button type="button" id="changeProductStatus" class="btn btn-rounded btn-sm btn-success changeProductStatus" data-product_id="' . $post->id . '" data-status="' . $post->status . '" data-toggle="tooltip" title="Activate product"><i class="fa fa-refresh"></i></button>&nbsp;';

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-success changeCategoryStatus" data-toggle="tooltip" title="Activate Unit"> <i class="fa fa-refresh"></i> </a>&nbsp;';
                }
                 
                 if(get_user_permission("manage_product_unit","edit"))
                {
                 $options .= '<a href="'.$edit.'" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit Unit"><i class="fa fa-edit"></i></a>';
                 }

                $nestedData['options'] = $options;
                
                /*$nestedData['body'] = substr(strip_tags($post->detail),0,50)."...";
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";*/
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
        
    }
}
