<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Barcode;



class LabelsController extends Controller
{
    /**
     * All Utils instance.
     *
     */
   

    /**
     * Constructor
     *
     * @param TransactionUtil $TransactionUtil
     * @return void
     */
   

    /**
     * Display labels
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $business_id = $request->session()->get('user.business_id');
        $purchase_id = $request->get('purchase_id', false);
        $product_id = $request->get('product_id', false);

        //Get products for the business
        $products = [];
        
            $products =  Product::where('id', $product_id)->get();
        

        $barcode_settings = Barcode::where('business_id', $business_id)
                                ->orWhereNull('business_id')
                                ->pluck('name', 'id');

                               // dd($product_id);

        return view('admin.labels.show')
            ->with(compact('products', 'barcode_settings'));
    }

    /**
     * Returns the html for product row
     *
     * @return \Illuminate\Http\Response
     */
    public function addProductRow(Request $request)
    {
        if ($request->ajax()) {
            $product_id = $request->input('product_id');
            $variation_id = $request->input('variation_id');
            $business_id = $request->session()->get('user.business_id');
            
            if (!empty($product_id)) {
                $index = $request->input('row_count');
                 $products =  Product::find($product_id)->get();
                
                return view('admin.labels.partials.show_table_rows')
                        ->with(compact('products', 'index'));
            }
        }
    }

    public function getProducts()
    {
        //echo '1';
        
            $term = request()->term;


            

            //$business_id = request()->session()->get('user.business_id');
            $q =  Product::where('name', 'like', '%' . $term .'%')->get();

           // dd($q); 
                
            $products_array = [];
            foreach ($q as $product) {
                $products_array[$product->product_id]['name'] = $product->name;
                $products_array[$product->product_id]['sku'] = $product->id;
                $products_array[$product->product_id]['type'] = $product->brand_id;
                
            }

            $result = [];
            $i = 1;
            $no_of_records = $q->count();
            if (!empty($products_array)) {
                foreach ($products_array as $key => $value) {
                    
                        $result[] = [ 'id' => $i,
                                    'text' => $value['name'] . ' - ' . $value['sku'],
                                    'variation_id' => 0,
                                    'product_id' => $value['sku']
                                ];
                    
                    $name = $value['name'];
                    
                    $i++;
                }
            }
            
            return json_encode($result);


        
    }

    /**
     * Returns the html for labels preview
     *
     * @return \Illuminate\Http\Response
     */
    public function preview(Request $request)
    {
        if ($request->ajax()) {
            try {
                $products = $request->get('products');
                $print = $request->get('print');
                $barcode_setting = $request->get('barcode_setting');
                $business_id = $request->session()->get('user.business_id');

                $barcode_details = Barcode::find($barcode_setting);
                //print_r($barcode_details);exit;
                $business_name = $request->session()->get('business.name');

                $product_details = [];
                $total_qty = 0;
                foreach ($products as $value) {
                    $details = $this->productUtil->getDetailsFromVariation($value['variation_id'], $business_id, null, false);
                    $product_details[] = ['details' => $details, 'qty' => $value['quantity']];
                    $total_qty += $value['quantity'];
                }

                $page_height = null;
                if ($barcode_details->is_continuous) {
                    $rows = ceil($total_qty/$barcode_details->stickers_in_one_row) + 0.4;
                    $barcode_details->paper_height = $barcode_details->top_margin + ($rows*$barcode_details->height) + ($rows*$barcode_details->row_distance);
                }

                $html = view('admin.labels.partials.preview')
                    ->with(compact('print', 'product_details', 'business_name', 'barcode_details', 'page_height'))->render();

                $output = ['html' => $html,
                                'success' => true,
                                'msg' => ''
                            ];
            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

                $output = ['html' => '',
                        'success' => false,
                        'msg' =>  __('lang_v1.barcode_label_error')
                    ];
            }

            return $output;
        }
    }
}
