<?php

namespace App\Http\Controllers;

use App\SubCategory;
use Illuminate\Http\Req;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log; 

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activeTab = 'manage_subcategory';
        $SubCategoryData = SubCategory::join('category','subcategory.category_id','=','category.id')->select('subcategory.*','category.name as categoryname')->get();
        return View('admin/subcategory/index2',compact('SubCategoryData','activeTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activeTab = 'manage_subcategory';
        $categories = DB::table("category")->get();
        return View('admin/subcategory/create',compact('categories','activeTab'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = date('Y-m-d h:i:s');
        $subcategory = new SubCategory;
        $subcategory->name = $request->input('name');
        $subcategory->category_id = $request->input('category');
        $subcategory->status = $request->input('status') != "" ? $request->input('status') : 0;
        $subcategory->created_at = $date;

        

        if(!$subcategory->save()) {
            //Redirect error
            return redirect()->route('subcategory')->with('error','Failed to update subcategory details.');
        }

        //Redirect success
        return redirect()->route('subcategory')->with('success','subcategory added successfully.');
    
    }

    public function changeSubCategoryStatus(Request $request) {
        $subcategory_id = $request->subcategory_id;
        $status = ($request->status == 1 ? 0 : 1);
        if(!empty($subcategory_id)) {
            $category = SubCategory::where('id',$subcategory_id)
                                ->update(['status' => $status]);

            if($category) {
                echo "success";
            } else {
                echo "fail";
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activeTab = 'manage_subcategory';
        $categories = DB::table("category")->get();
        $subcategory = SubCategory::find($id);
        return View('admin/subcategory/create',compact('subcategory','activeTab','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $date = date('Y-m-d h:i:s');
            $subcategory = SubCategory::find($id);
            $subcategory->name = $request->input('name');
            $subcategory->category_id = $request->input('category');
            $subcategory->status = $request->input('status') != "" ? $request->input('status') : 0;
            $subcategory->updated_at = $date;

            

            if($subcategory->save()) {
                //Redirect success
                return redirect()->route('subcategory')->with('success','Subcategory updated successfully.');
            }

            //Redirect error
            return redirect()->route('subcategory')->with('error','Failed to update subcategory.');


        } catch (Exception $e) {
            Log::error("Failed to update subcategory.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        
    }

    public function allSubCategory(Request $request)
    {
        
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2 =>'category_name',
                            3=> 'status',
                        );
  
        $totalData = SubCategory::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = SubCategory::join('category','subcategory.category_id','=','category.id')->select('subcategory.*','category.name as categoryname')
                         ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts = SubCategory::join('category','subcategory.category_id','=','category.id')->select('subcategory.*','category.name as categoryname')
                         ->where('name','LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                         ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
            

            $totalFiltered = SubCategory::join('category','subcategory.category_id','=','category.id')->select('subcategory.*','category.name as categoryname')
                         ->where('name','LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                         ->orWhere('content', 'LIKE',"%{$search}%")
                         ->orWhere('status', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('subcategory.edit',$post->id); 
                $edit = route('subcategory.edit', $post->id);
               
                
                $st = route('change.subcategory_datatable.status', $post->id);

                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->name;
                $nestedData['categoryname'] = $post->categoryname;
                
                
                //$nestedData['status'] = $post->status;
                if($post->status == '1')
                {
                    $nestedData['status'] = '<span class="badge badge-success">Active</span>';
                }else
                {
                    $nestedData['status'] = '<span class="badge badge-danger">Paused</span>';
                }

                $options = "";

                if($post->status == '1')
                {
                    

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-warning" data-toggle="tooltip" title="Pause SubCategory"> <i class="fa fa-power-off"></i> </a>&nbsp;';
                }else
                {
                   // $options = '<button type="button" id="changeProductStatus" class="btn btn-rounded btn-sm btn-success changeProductStatus" data-product_id="' . $post->id . '" data-status="' . $post->status . '" data-toggle="tooltip" title="Activate product"><i class="fa fa-refresh"></i></button>&nbsp;';

                    $options = '<a href="'.$st.'" class="btn btn-rounded btn-sm btn-success changeCategoryStatus" data-toggle="tooltip" title="Activate SubCategory"> <i class="fa fa-refresh"></i> </a>&nbsp;';
                }
                 
                 if(get_user_permission("manage_subcategory","edit"))
                {
                 $options .= '<a href="'.$edit.'" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit SubCategory"><i class="fa fa-edit"></i></a>';
                 }

                $nestedData['options'] = $options;
                
                /*$nestedData['body'] = substr(strip_tags($post->detail),0,50)."...";
                $nestedData['created_at'] = date('j M Y h:i a',strtotime($post->created_at));
                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";*/
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
        
    }

    public function changeSubCategoryStatus_datatable(Request $request, $id) {
        $SubCategory = SubCategory::find($id);

        if($SubCategory->status == 1) {
            $SubCategory->status = 0;
        }elseif($SubCategory->status == 0) {
            $SubCategory->status = 1;
        }
        $SubCategory->save();

        return redirect()->route('subcategory')->with('success', 'SubCategory Status Updated');

         
        

    }
}
