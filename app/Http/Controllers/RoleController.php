<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;


class RoleController extends Controller
{
      public function showRoles(Request $request) {
        $roles = Role::all();
        $activeTab = 'manage_roles';
        return view('admin.roles.show-roles',compact('roles','activeTab'));
    }


    public function createRoles(Request $request) {
		$activeTab = 'manage_roles';
        return view('admin.roles.save-roles',compact('activeTab'));
      
    }

    public function saveRoles(Request $request) { 
        
        $request->validate([
            'name' => 'required|unique:roles|max:255',
            'status' => 'required',
        ]);

        $roles = new Role;
        $roles->name    =   $request->name;
        $roles->status    =   $request->status != "" ? $request->status : 0;
        $roles->save();

        return redirect()->route('roles')->with('success', 'Add Role Successfully');

    }

    public function editRoles(Request $request, $id) {
        $role = Role::find($id);
        
        if($role == null) {
            return redirect()->back()->with('error', 'Invalid Route Direction');
        }
		$activeTab = 'manage_roles';
        return view('admin.roles.save-roles',compact('role','activeTab'));
        //return view('roles.save-roles')->with('role', $role);

    }

    public function updateRoles(Request $request, $id) {

        $role = Role::find($id);
        if($role == null) {
            return redirect()->back()->with('error', 'Invalid Route Direction');
        }

        $role->name         = $request->name;
        $role->status    =   $request->status != "" ? $request->status : 0;
        $role->save();
        
        return redirect()->route('roles')->with('success', 'Update Role Successfully');
    }

    public function changeRoleStatus(Request $request, $id) {
        
        $role = Role::find($id);

        if($role->status == 1) {
            $role->status = 0;
        }elseif($role->status == 0) {
            $role->status = 1;
        }
        $role->save();

        return redirect()->route('roles')->with('success', 'Role Status Updated');

    }
}
