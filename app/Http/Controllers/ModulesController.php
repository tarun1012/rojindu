<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules;
use App\Permission;


class ModulesController extends Controller
{
   public function showModules(Request $request) {
        $modules    =   Modules::all();
        $activeTab = 'manage_modules';
        return view('admin.modules.show-module',compact('modules','activeTab'));
    }

    public function createModules(Request $request) {
        // dd($request->all());
		$activeTab = 'manage_modules';
        return view('admin.modules.save-module',compact('activeTab'));
    }

    public function saveModules(Request $request) {
        
        $request->validate([
            'name' => 'required|unique:roles|max:255',
            'status' => 'required',
        ]);

        $date = date('Y-m-d h:i:s');

        $modules = new Modules;
        $modules->name    =   $request->name;
        $modules->status    =   $request->status != "" ? $request->status : 0;
        $modules->save();

        $permission = new Permission;
        $permission->roleid = 0;
        $permission->moduleid = $modules->id;
        $permission->add = 1;
        $permission->edit = 1; 
        $permission->delete = 1;
        $permission->view = 1;
        $permission->created_at = $date;
        $permission->save();

        return redirect()->route('modules')->with('success', 'Add modules Successfully');

    }

    public function editModule(Request $request, $id) {
        $module = Modules::find($id);
        
        if($module == null) {
            return redirect()->back()->with('error', 'Invalid Route Direction');
        }
		
		$activeTab = 'manage_modules';
        return view('admin.modules.save-module',compact('module','activeTab'));

       // return view('modules.save-module')->with('module', $module);

    }

    public function updateModules(Request $request, $id) {

        $modules = Modules::find($id);
        if($modules == null) {
            return redirect()->back()->with('error', 'Invalid Route Direction');
        }

        $modules->name         = $request->name;
        $modules->status       = $request->status != "" ? $request->status : 0;
        $modules->save();
        
        return redirect()->route('modules')->with('success', 'Update modules Successfully');
    }

    public function changeModuleStatus(Request $request, $id) {
        
        $modules = Modules::find($id);

        if($modules->status == 1) {
            $modules->status = 0;
        }elseif($modules->status == 0) {
            $modules->status = 1;
        }
        $modules->save();

        return redirect()->route('modules')->with('success', 'Modules Status Updated');

    }

}
