<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Excel;

class ImportExcelController extends Controller
{
    function index()
    {
     $data = DB::table('products')->orderBy('id', 'DESC')->get();
     return view('admin.product.import_excel', compact('data'));
    }


    public function import(Request $request) 
    {

    	$this->validate($request, [
      'select_file'  => 'required|mimes:csv'
     ]);
        Excel::import(new UsersImport,request()->file('select_file'));
           
        return back();
    }

    /*function import(Request $request)
    {
     $this->validate($request, [
      'select_file'  => 'required|mimes:xls,xlsx'
     ]);

     $path = $request->file('select_file')->getRealPath();
     //dd($path);

     $data = Excel::import($path)->get();

     $date = date('Y-m-d h:i:s');

     if($data->count() > 0)
     {
      foreach($data->toArray() as $key => $value)
      {
       foreach($value as $row)
       {
        $insert_data[] = array(
         'name'  => $row['name'],
         'category_id'   => $row['category_id'],
         'subcategory_id'   => $row['subcategory_id'],
         'brand_id'    => $row['brand_id'],
         'price'  => $row['price'],
         'unit'   => $row['unit'],
         'detail'   => $row['detail'],
         'created_by'   => '1',
         'status'   => '1',
          'created_at' => $date
          
        );
       }
      }

      if(!empty($insert_data))
      {
       DB::table('products')->insert($insert_data);
      }
     }
     return back()->with('success', 'Excel Data Imported successfully.');
    }*/
}