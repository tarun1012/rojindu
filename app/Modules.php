<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    public function getPermissions() {
        return $this->hasMany('App\Permission', 'moduleid', 'id');
    }
}
