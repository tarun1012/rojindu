<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    protected $table = "permissions";

    public function getModule() {
        return $this->hasMany('App\Modules', 'id', 'moduleid');
    }
}
