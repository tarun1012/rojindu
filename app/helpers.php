<?php

use App\User;


    function random_code(){
 
        return rand(1111, 9999);
    }
    function is_referal_user(){
        $user = Auth::user();
        $user_role = array(1,2,3,4,5);
        if(in_array($user->roleid,$user_role)){
            return true;
        }else{
            return false;
        }
    }

    function get_countries() {
        return DB::table('country')->get();
    } 
    
    function get_state() {
        return DB::table('state')->get();
    }

    function get_city() {
        return DB::table('city')->get();
    }

    function get_setting_option($key) {
        return DB::table('sy_option')->where('key_id',$key)->first();
    }

    function shorten_length($string,$length) {
        return strlen($string) > $length ? substr($string,0,$length)."..." : $string;
    }

    function getRole() {
        
        $role = DB::table('roles')->where('id', Auth::user()->roleid)->first();
        if(Auth::user()->roleid == 0){
            return 'Admin';
        }else{
        return $role->name;
        }
    }

    function get_roles() {
        return DB::table('roles')->get();
    }

    function getCountry() {
        $country = DB::table('country')->select('name')->where("id", Auth::user()->country)->first();
        
       if($country != ""){
        return $country->name;
    }
    }

    function getState() {
        $state = DB::table('state')->select('name')->where("id", Auth::user()->state)->first();
        if($state != ""){
        return $state->name;
    }
       // return $state->name;
    }

    function get_activation_date() {
        return date('m M Y',strtotime(Auth::user()->created_at));
    }

    function getCity() {
        $city = DB::table('city')->select('name')->where("id", Auth::user()->city)->first();
       if($city != ""){
        return $city->name;
    }
    }

    function get_customer_reviews() {
        $Reviews = DB::table('user_reviews')
                    ->leftJoin('users','user_reviews.userid','=','users.id')
                    ->where('user_reviews.status','=','1')
                    ->where('users.status','=','1')
                    ->select('user_reviews.*','users.id as userid','users.name as fullname','users.status as user_status','users.profilepic as profilepic')
                    ->limit(5)
                    ->get();
        return $Reviews;
    }

    function get_user_permission($module,$type) {
        $Permission = DB::select(
            "
            SELECT 
                perm.*,
                module.id as moduleid,module.name as moduleName 
            FROM permissions as perm
                JOIN modules as module on perm.moduleid = module.id 
            WHERE perm.roleid = '".Auth::user()->roleid."' and module.name = '".$module."' and perm." .$type. "=1"
        ); 
        //dd($Permission);
        return count($Permission) > 0 ? true : false;
    }
    
    function get_option($key) {
        $option = DB::table('sy_option')->select('value')->where("key_id", $key)->first();
        return $option->value;
    }
    
    function generateCode($limit){
    $code = '';
    for($i = 0; $i < $limit; $i++) { $code .= mt_rand(0, 9); }
    return $code;
}


function sendsms($number, $message_body, $return = '0') {


$SMS_API_KEY = 'HUUA9K8ESITWFUHI6Q48EYLTDJXYTYOA';
$SMS_SECRET_KEY = 'LWRWIA3R4QOMNJQ8';
$SMS_SENDER_ID = 'SYatra';

$textmessage = urlencode($message_body);

$url="https://www.sms4india.com/api/v1/sendCampaign";


$curl = curl_init();
curl_setopt($curl, CURLOPT_POST, 1);// set post data to true
curl_setopt($curl, CURLOPT_POSTFIELDS, "apikey=".$SMS_API_KEY."&secret=".$SMS_SECRET_KEY."&usetype= prod&phone=".$number."&senderid=".$SMS_SENDER_ID."&message=".$textmessage);// post data
// query parameter values must be given without squarebrackets.
 // Optional Authentication:
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);
curl_close($curl);

//if(!$result){ $result = file_get_contents($smsgatewaydata); }
 if($result) { 
return 'Sent'; 
 }
}

function total_rows($table, $where = array())

    {
        
        if (is_array($where)) {
           if (sizeof($where) > 0) {
                //$CI->db->where($where);
                return DB::table($table)->where($where)->count();
            }
        } else if (strlen($where) > 0) {

            
            return DB::table($table)->where($where)->count();

        }

        return DB::table($table)->count();
        //return $CI->db->count_all_results($table);
    }
    
 function uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    function LaraFileUpload($request, $fieldName, $folder) {
        
        if($request->hasFile($fieldName)) {

            $image = $request->file($fieldName);
            $fileNewName = $fieldName.'_'.time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path($folder.'/'), $fileNewName);

            return $fileNewName;
        }else{
            return "invalid file type";
        }

    }   

    function count_user_by_role($role) {
        $query = DB::table('users')
                    ->distinct('id')
                    ->whereIn('roleid',$role)
                    ->count('id');
        return $query;
    }

    function count_membership_type($type) {
        $query = DB::table('user_membership')
                    ->distinct('id')
                    ->where('membershiptypeid',$type)
                    ->where('status',1)
                    ->count('id');
        return $query;
    }

    function count_membership_type_by_referal($type) {
        $query = DB::table('user_membership')
                    ->distinct('id')
                    ->where('membershiptypeid',$type)
                    ->where('referal_code',Auth::user()->referal_code)
                    ->count('id');
        return $query;
    }
    
    
     function count_membership_type_amount_by_referal($type,$referal_code) {
        return DB::table('subscription')
                    ->join('user_membership','subscription.membershipid','=','user_membership.id')
                    ->where('user_membership.membershiptypeid',$type)
                    ->where('user_membership.status',1)
                    ->where('subscription.status',1)
                    ->where('user_membership.referal_code',Auth::user()->referal_code)
                    ->sum('subscription.amount');
       // return $query;
    }

    function count_card_users() {
        $query = DB::table('member_package')
                    ->distinct('membershipid')
                    ->where('status','=',1)
                    ->count('membershipid');
        return $query;
    }

    function calculate_sum($table,$where,$sum_column) {
        if (is_array($where)) {
            if (sizeof($where) > 0) {
                //$CI->db->where($where);
                return DB::table($table)->where($where)->sum($sum_column);
            }
        } else if (strlen($where) > 0) {

            
            return DB::table($table)->where($where)->sum($sum_column);

        }

        return DB::table($table)->count($sum_column);
    }

    function referal_user_members_packages() {
        return DB::table('user_membership')
                    ->leftJoin('member_package as pack','user_membership.id','=','pack.membershipid')
                    ->where('user_membership.status','=',1)
                    ->where('pack.status','=',1)
                    ->where('user_membership.referal_code','=',Auth::user()->referal_code)
                    ->groupBy('user_membership.id')
                    ->count('pack.membershipid');
    }
    
    function referal_user_members_pending_packages() {
        
        return DB::table('user_membership')
                ->leftJoin('member_package','user_membership.id','=','member_package.membershipid')
                ->whereNull('member_package.membershipid')
                ->where('user_membership.referal_code','=',Auth::user()->referal_code)
                ->count('user_membership.id');
    }
    
     function advisor_user_is_verified($referalcode) {
        return DB::table('user_membership')
                    ->leftJoin('member_package as pack','user_membership.id','=','pack.membershipid')
                    ->where('user_membership.status','=',1)
                    ->where('pack.status','=',1)
                    ->where('user_membership.referal_code','=',$referalcode)
                    ->count('pack.membershipid');
    }
    
    function referal_user_membership_pending(){
        return DB::table('user_membership')
                    ->whereIn('user_membership.status',[0,2])
                    ->where('user_membership.referal_code','=',Auth::user()->referal_code)
                    ->count('user_membership.id');
    }
    
    function pm_upgrade_membership($membershipid){
       $upgrade_data = array();
        $subscriptions= DB::table('subscription')->join('membership_type','subscription.membershiptypeid', '=', 'membership_type.id')->where('subscription.membershipid', $membershipid)->orderBy('subscription.id','desc')->select('subscription.*','membership_type.name','membership_type.amount as previous_amount')->get();
           if($subscriptions != null && count($subscriptions) > 1) {
            $upgrade_data['previous_membership'] =$subscriptions[1]->name .' to '.$subscriptions[0]->name;
            $upgrade_data['upgrade_date'] = date('d M Y',strtotime($subscriptions[0]->date));
          }
          
          return $upgrade_data;
        
    }
    
    
  function get_pachage_tour($package_id){
      
      return DB::table('member_package_tour')
                    ->where('memberpackageid',$package_id)
                    ->get();
  }

function get_role_by_id($id) {
        $role = DB::table('roles')->where('id',$id)->first();
        if(!empty($role)) {
            return $role->name;
        } else {
            return [];
        }
    }

