<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserRequestStatus extends Model
{
    protected $table = 'tbluserrequeststatus';

    public function getUsername() {
        return $this->hasOne('App\User', 'id', 'userid');
    }
}
