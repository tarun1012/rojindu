<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";

    protected $fillable = ['name','category_id','subcategory_id','brand_id','price','unit','detail'];
}
