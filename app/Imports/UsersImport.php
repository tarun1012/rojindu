<?php
   
namespace App\Imports;
   
use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
    
class UsersImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
    	$date = date('Y-m-d h:i:s');
        return new Product([
            'name'  => $row['name'],
	         'category_id'   => $row['category_id'],
	         'subcategory_id'   => $row['subcategory_id'],
	         'brand_id'    => $row['brand_id'],
	         'price'  => $row['price'],
	         'unit'   => $row['unit'],
	         'detail'   => $row['detail'],
	         'created_by'   => '1',
	         'status'   => 1,
	          'created_at' => $date
        ]);
    }
}