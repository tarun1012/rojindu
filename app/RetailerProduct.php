<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;


class RetailerProduct extends Model
{
    protected $table = "retailer_product";

    public static function getuserData($id=null){

    	$user = Auth::user();
        $userid = $user->id;

     $value = DB::select( DB::raw("SELECT p.*, rp.price as rprice,rp.id as rid,rp.qty 
FROM products p
LEFT JOIN retailer_product rp
ON p.id = rp.productid AND rp.userid = '$userid'") ); 
     return $value;



   }

   public static function insertData($data){

     $value=DB::table('users')->where('username', $data['username'])->get();
     if($value->count() == 0){
       $insertid = DB::table('users')->insertGetId($data);
       return $insertid;
     }else{
       return 0;
     }

   }

   public static function updateData($id,$data){

   //	dd($data);
    $user = Auth::user();
    $userid = $user->id;
    $date = date('Y-m-d h:i:s');
   	$value=DB::table('retailer_product')->where('productid', $data['productid'])->where('userid', $userid)->get();
   	if($value->count() == 0){
   		$data['created_at'] = $date;
       $insertid = DB::table('retailer_product')->insertGetId($data);
       return $insertid;
     }else{
     	 $value1=DB::table('retailer_product')->where('productid', $data['productid'])->where('userid', $userid)->pluck('id');
     	$data['updated_at'] = $date;
       DB::table('retailer_product')->where('id', $value1)->update($data);
     }
      
   }

   public static function deleteData($id=0){
      DB::table('RetailerProduct')->where('id', '=', $id)->delete();
   } 
}
