    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/menu.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/custom-animation.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/custom.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('front/css/responsive.css')}}">

    <!-- Favicon -->
    <link href="{{asset('front/images/favicon.png')}}" rel="shortcut icon" type="image/png">