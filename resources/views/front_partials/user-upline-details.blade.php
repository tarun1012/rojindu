<div class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
		<div class="modal-header">
		<h4 class="modal-title" id="modalTitle">Upline Details Of {{$Username->firstname. " " .$Username->lastname}}</h4>
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	    </div>
	    <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{route('export.upline.details', $Username->id)}}" class="btn btn-sm btn-yatra"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="UplineTable" class="table table-hover nowrap display " style="width:100%;    overflow: auto;    ">
                                    <thead class="text-center">
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Contact Number</th>
                                            <th>Email</th>
                                            <th>Position</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (!empty($List))
                                            <?php $i = 1; ?>
                                            @foreach ($List as $data)
                                                <tr class="text-center">
                                                    <td>{{$i}}</td>
                                                    <td>{{$data['firstname']." ".$data['lastname']}}</td>
                                                    <td>{{$data['contactno']}}</td>
                                                    <td>{{$data['email']}}</td>
                                                    <td>{{get_role_by_id($data['roleid'])}}</td>
                                                </tr>
                                            <?php $i++; ?>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
	</div>
</div>

