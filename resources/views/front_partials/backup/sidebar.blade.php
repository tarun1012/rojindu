<!-- <div class="col-lg-3 col-sm-3 p-0"> -->
<nav id="sidebar" class="sticky-top">
    <div class="px-3 mt-2">

        <!-- <div class="custom-menu">
            <button type="button" id="sidebarCollapse" class="btn btn-primary">
                <i class="fa fa-bars"></i>
                <span class="sr-only">Toggle Menu</span>
            </button>
        </div> -->



        <img src="{{asset('front/images/user.png')}}" class="img logo rounded-circle mb-3">
        <h5>{{ Auth::user()->username }}</h5>
        <div class="role">
            {{ getRole() }}
        </div>
        <hr>
        <ul class="list-unstyled components mt-3 mb-5">
            <li class="{{!empty($activeTab) ? ($activeTab == "dashboard" ? "active" : "") : ""}}">
                <a href="{{route('customer.dashboard')}}">Dashboard</a>
            </li>

            @if (Auth::user()->roleid == 7)

            <li class="{{!empty($activeTab) ? ($activeTab == "membership" ? "active" : "") : ""}}" style="display:{{!get_user_permission("upgrade_membership") ? "none" : ""}}">
                <a href="{{route('membership')}}">Membership</a>
            </li>

            <li class="{{!empty($activeTab) ? ($activeTab == "my_packages" ? "active" : "") : ""}}" style="display:{{!get_user_permission("my_packages") ? "none" : ""}}">
                <a href="{{url('dashboard/membership_package')}}">My Packages</a>
            </li>

            <li class="{{!empty($activeTab) ? ($activeTab == "my_reviews" ? "active" : "") : ""}}" style="display:{{!get_user_permission("my_reviews") ? "none" : ""}}">
                <a href="{{route('customer.reviews')}}">My Reviews</a>
            </li>

            <li class="{{!empty($activeTab) ? ($activeTab == "change_password" ? "active" : "") : ""}}" style="display:{{!get_user_permission("change_password") ? "none" : ""}}">
                <a href="{{route('change.password')}}">Change Password</a>
            </li>

            @elseif(Auth::user()->roleid == 6)

            <li class="{{!empty($activeTab) ? ($activeTab == "change_password" ? "active" : "") : ""}}" style="display:{{!get_user_permission("change_password") ? "none" : ""}}">
                <a href="{{route('change.password')}}">Change Password</a>
            </li>

            @elseif(Auth::user()->roleid == 0)

            <li class="{{!empty($activeTab) ? ($activeTab == "manage_users" ? "active" : "") : ""}}">
                <a href="{{ route('users') }}">Advisor Users</a>
            </li>

            <li class="{{!empty($activeTab) ? ($activeTab == "customers_report" ? "active" : "") : ""}}">
                <a href="{{ route('report.customers') }}">Free Customers</a>
            </li>

            <li class="{{!empty($activeTab) ? ($activeTab == "premium_members_report" ? "active" : "") : ""}}">
                <a href="{{ route('report.premium.members') }}">Premium Members</a>
            </li>
            <li class="{{!empty($activeTab) ? ($activeTab == "manage_membership_type" ? "active" : "") : ""}}">
                <a href="{{route('membershiptype')}}">Membership Type</a>
            </li>


            <li>
                <a href="#request" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Request</a>
                <ul class="collapse list-unstyled" id="request">

                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_membership_request" ? "active" : "") : ""}}">
                        <a href="{{route('manage_membership_request')}}">Membership Request</a>
                    </li>

                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_package_request" ? "active" : "") : ""}}">
                        <a href="{{route('manage_buy_package_request')}}">Buy Package Request</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#tours" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Tours</a>
                <ul class="collapse list-unstyled" id="tours">
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_tours" ? "active" : "") : ""}}">
                        <a href="{{route('tours')}}">Tours</a>
                    </li>
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_tour_packages" ? "active" : "") : ""}}">
                        <a href="{{route('tours.packages')}}">Tour Packages</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#settings" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Settings</a>
                <ul class="collapse list-unstyled" id="settings">
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_reviews" ? "active" : "") : ""}}">
                        <a href="{{route('manage.reviews')}}">Reviews</a>
                    </li>
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_banners" ? "active" : "") : ""}}">
                        <a href="{{route('banner')}}">Banners</a>
                    </li>
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_roles" ? "active" : "") : ""}}">
                        <a href="{{ route('roles') }}">Roles</a>
                    </li>
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_modules" ? "active" : "") : ""}}">
                        <a href="{{ route('modules') }}">Modules</a>
                    </li>
                    <li class="{{!empty($activeTab) ? ($activeTab == "manage_permissions" ? "active" : "") : ""}}">
                        <a href="{{ route('permissions') }}">Manage Permissions</a>
                    </li>

                    <li class="{{!empty($activeTab) ? ($activeTab == "change_cmd_password" ? "active" : "") : ""}}">
                        <a href="{{route('change.cmd.password')}}">Change CMD Password</a>
                    </li>
                    <li class=" {{!empty($activeTab) ? ($activeTab == "settings" ? "active" : "") : ""}}">
                        <a href="{{route('settings')}}">Settings</a>
                    </li>
                </ul>
            </li>



            {{-- <li >
                    <a  href="{{ route('admin.document.varification') }}">KYC Verification</a>
            </li>--}}

            

		@elseif(Auth::user()->roleid == 10)

            <li class="{{!empty($activeTab) ? ($activeTab == "manage_users" ? "active" : "") : ""}}">
                <a href="{{ route('users') }}">Advisor Users</a>
            </li>

            <li class="{{!empty($activeTab) ? ($activeTab == "customers_report" ? "active" : "") : ""}}">
                <a href="{{ route('report.customers') }}">Free Customers</a>
            </li>
			<li class="{{!empty($activeTab) ? ($activeTab == "premium_members_report" ? "active" : "") : ""}}">
                    <a href="{{ route('report.premium.members') }}">Premium Members</a>
                </li>
              
          
			
           



@elseif(Auth::user()->roleid == 7)

            <li class="{{!empty($activeTab) ? ($activeTab == "manage_advisor_users" ? "active" : "") : ""}}">
                <a href="{{ route('users.advisor') }}">Advisor Users</a>
            </li>
			 <li class="{{!empty($activeTab) ? ($activeTab == "premium_membership_report" ? "active" : "") : ""}}" style="display:{{is_referal_user() == false ? 'none' : ''}}">
                <a href="{{route('report.premium.membership')}}">Premium Membership Report</a>
            </li>
 @endif
            <li class="{{!empty($activeTab) ? ($activeTab == "my_profile" ? "active" : "") : ""}}">
                <a href="{{route('myprofile.view')}}">My Profile</a>
            </li>

           


            <li class="">
                <a href="{{ route('admin.logout') }}" class="reject">Logout</a>
            </li>
        </ul>





        <!-- <div class="footer invisible">
                <p>
                                    Copyright &copy;<script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib.com</a>
                                  </p>
            </div> -->

    </div>
</nav>

<!-- </div> -->