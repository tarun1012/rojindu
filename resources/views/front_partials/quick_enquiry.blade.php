<div class="quick-inquiry">
    <div class="box">
        <div class="box-button">
            <button class="btn btn-yatra rotate w-100">
                <span> Quick Inquiry</span>
            </button>
        </div>
        <div class="container">
            <form class="row p-3" id="QuickEnquiry" method="POST">
                @csrf
                <div class="form-group col-lg-12">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Name*" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email ID*" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <input type="text" name="contact" id="contact" class="form-control" placeholder="Contact*" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <select class="form-control" name="age">
                        <option value="">Age group</option>
                        <option value="student">Student</option>
                        <option value="20-30">20-30</option>
                        <option value="30-40">30-40</option>
                        <option value="40-50">40-50</option>
                        <option value="50-60">50-60</option>
                    </select>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <select class="form-control" id="country" name="country">
                        <option value="">Country</option>
                        @if (!empty($Country))
                        @foreach ($Country as $country)
                        <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                        @endif
                    </select>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <select class="form-control" id="state" name="state">
                        <option value="">State</option>
                    </select>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <select class="form-control" id="city" name="city">
                        <option value="city">City</option>
                    </select>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <textarea name="message" id="message" class="form-control" placeholder="Your message*" name="message" cols="30" rows="2"></textarea>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <button class="btn btn-shukul mx-3 sendEnquiry" type="button">Send</button>
                <p class="enquiry-msg"></p>
            </form>
        </div>
    </div>

</div>