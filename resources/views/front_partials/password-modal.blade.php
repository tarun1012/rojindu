<div id="view-password-modal" class="modal fade"  
    tabindex="-1" role="dialog" 
    aria-labelledby="myModalLabel" 
    aria-hidden="true" style="display: none;">
     <div class="modal-dialog"> 
          <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" 
                    data-dismiss="modal" 
                    aria-hidden="true">
                    ×
                    </button> 
            </div> 
            <div class="modal-body text-center"> 
                <div class="row">
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control" name="user-password" readonly id="user-password" />
                    </div>
                    <div class="form-group col-md-6">
                        <button type="button" class="btn btn-secondary" id="password-btn">Copy Password</button>
                    </div>
                </div>
            </div>  
         </div> 
      </div>
</div><!-- /.modal -->  