	<div class="modal-dialog modal-xl" role="document">

	<div class="modal-content">
		<div class="modal-header">
		<h4 class="modal-title" id="modalTitle">User Details</h4>
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		      
	    </div>
	    <div class="modal-body">
      		<div class="row">
      			
	      			<div class="col-sm-6">
					
					<h4 class="mb-2"><b><u>Personal Details</u></b></h4>
	      				<p><b>Name:</b> {{$user->name}}</p>
	      				<p><b>Email ID:</b> {{$user->email}}</p>
	      				<p><b>Contact NO:</b> {{$user->contactno}}</p>
	      				<p><b>Age Group:</b> {{$user->ageGroup}}</p>
	      				<p><b>Date of Birth:</b> {{ date('d-m-Y', strtotime($user->dob))}}</p>
	      				@if($user->roleid != 6 || $user->roleid != 10 )<p><b>referal_code:</b> {{$user->referal_code}}</p>@endif
						
					</div>

	      			<div class="col-sm-6 ">
						<h4 class="mb-2"><b><u>Location Details</u></b></h4>
	      				<p><b>Address:</b> {{$user->address}}</p>
	      				@if($user->locality != '')<p><b>locality:</b> {{$user->locality}}</p>@endif
	      				<p><b>Profile Image:</b></p><br>
						<img class="img logo mb-3" onerror="this.src='{{asset('front/images/user.png')}}';" src="{!! asset('profiles/'.$user->profilepic) !!}" style="width:120px;">
	      				
					</div>
					
	      			
	      			
      			
	      		</div>
      			
      		      	
			
			</div>
      
	</div>
	</div>

