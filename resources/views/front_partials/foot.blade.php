    <script src="{{asset('front/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('front/js/popper.min.js')}}"></script>
    <script src="{{asset('front/js/bootstrap.min.js')}}"></script>


    <!-- Optional JavaScript -->
    <script src="{{asset('front/js/bootstrap-dropdownhover.min.js')}}"></script>
    <script src="{{asset('front/js/bootstrap-slider.js')}}"></script>
    <script src="{{asset('front/js/jquery.flexslider-min.js')}}"></script>
    <script src="{{asset('front/js/slick.min.js')}}"></script>
    <script src="{{asset('front/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('front/js/css3-animate-it.js')}}"></script>
    <script src="{{asset('front/js/magnific-popup.min.js')}}"></script>
    <script src="{{asset('front/js/jquery.fancybox.js')}}"></script>
    <script src="{{asset('front/js/player-min.js')}}"></script>
    <script src="{{asset('front/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('front/js/additional-methods.min.js')}}"></script>

    <script src="{{asset('front/js/custom-validation.js')}}"></script>
    <script src="{{asset('front/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('front/js/jquery.marquee.min.js')}}"></script>

    <!-- Custom JavaScript -->
    <script src="{{asset('front/js/script.js')}}"></script>
    <script src="{{asset('front/js/custom-scripts.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('.marquee').marquee({
                duplicated: true,
                duration: 7000,
            });
			var activeTab = "{{(Session::has('activeTab') ? 'show' : 'hide')}}";
            $('#login').modal(activeTab);
            $('.btn-show-password').click(function() {
                var pass = $(this).data("user-password");
                $("#user-password").val(pass);
            });
            $('#password-btn').click(function() {
                var copyText = document.getElementById("user-password");
                copyText.select();
                copyText.setSelectionRange(0, 99999); 
                document.execCommand("copy");
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).on('submit', '#login-form', function(e) {

                e.preventDefault();
                console.log($(this).serialize());

                $.ajax({
                        method: $(this).attr('method'),
                        url: $(this).attr('action'),
                        data: $(this).serialize(),
                        dataType: "json"
                    })

                    .done(function(data) {
                        if (data.status == 'success') {
                            //alert('success')
                            $("#login-form")[0].reset();
                            $('#myModal').modal('hide');
                            if (data.redirect == 'onpage') {
                                location.reload();
                            } else {
                                window.location.href = "{{url('/dashboard')}}";
                            }

                        } else {
                            $('.alert-msg').html('<div class="alert alert-danger alert-block"><button type="button" class="close" data-dismiss="alert">×</button><strong>Invalid username or password</strong></div>');
                        }

                        console.log('done-' + data);
                    })
                    .fail(function(data) {
                        console.log('fail-' + data);
                        $.each(data.responseJSON, function(key, value) {
                            var input = '#formRegister input[name=' + key + ']';
                            $(input + '+small').text(value);
                            $(input).parent().addClass('has-error');
                        });
                    });

            });

        });
    </script>