    <link rel="icon" href="http://admin.pixelstrap.com/endless/assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="http://admin.pixelstrap.com/endless/assets/images/favicon.png" type="image/x-icon">
    <title>Endless - Premium Admin Template</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="http://admin.pixelstrap.com/endless/assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="http://admin.pixelstrap.com/endless/assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="http://admin.pixelstrap.com/endless/assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="http://admin.pixelstrap.com/endless/assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="http://admin.pixelstrap.com/endless/assets/css/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="http://admin.pixelstrap.com/endless/assets/css/chartist.css">
    <link rel="stylesheet" type="text/css" href="http://admin.pixelstrap.com/endless/assets/css/prism.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="http://admin.pixelstrap.com/endless/assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="http://admin.pixelstrap.com/endless/assets/css/style.css">
    <link id="color" rel="stylesheet" href="http://admin.pixelstrap.com/endless/assets/css/light-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="http://admin.pixelstrap.com/endless/assets/css/responsive.css">