<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="description" content="" />
  <meta name="keywords" content="creative, portfolio, agency, template, theme, designed, html5, html, css3, responsive, onepage" />
  <meta name="author" content="Set Private Limited" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title>Online Tours & Travel @ Rojindu</title>
  <link rel="stylesheet" href="{{asset('front/css/all.min.css')}}">
  @include('front_partials.head')

  @yield('css')

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Rojindu
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                            <li class="nav-item">
                                    <a class="nav-link" href="{{ route('RetailerRegister') }}">Retailer Register</a>
                                </li>
                        @else
                        <li class="nav-item dropdown no-arrow ">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 ">@if (Auth::check()) My Account @else Login @endif</span>
            <img class="img-profile rounded-circle" onerror="this.src='{{asset('front/images/user.png')}}';" src="{!! asset('profiles/'.Auth::user()->profilepic) !!}" style="width:2rem;">
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
           
             <!--  <a class="dropdown-item" href="{{url('myprofile/view')}}">My Profile </a>
           
           
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{url('change/password')}}">Change Password </a> -->
            
            
             @if (get_user_permission("add_address","view"))
             <a class="nav-link" href="{{ route('address') }}">Add Address</a>
             @endif
             @if (get_user_permission("change_password","view"))
             <div class="dropdown-divider"></div>
             <a class="nav-link" href="{{ route('changepassword') }}">Change Password</a>
              @endif
              @if (get_user_permission("my_profile","view"))
             <div class="dropdown-divider"></div>
             <a class="nav-link" href="{{ route('myprofile.view') }}">My Profile</a>
              @endif
              
             <div class="dropdown-divider"></div>
             <a class="nav-link" href="{{ route('admin.logout') }}">{{ __('Logout') }}</a>

          </div>
        </li>
                            
                            
                        @endguest

                        
                    </ul>

                </div>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('shop') }}">SHOP</a>
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle"
                       href="#" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false"
                    >
                        <span class="badge badge-pill badge-dark">
                            <i class="fa fa-shopping-cart"></i>
                        </span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="width: 450px; padding: 0px; border-color: #9DA0A2">
                        <ul class="list-group" style="margin: 20px;">
                            @include('partials.cart-drop')
                        </ul>

                    </div>
                </li>
            </ul>
        </div>
            </div>
        </nav>

        @yield('content')


    @include('front_partials.foot')

  </div>
  <!-- End Page Wrapper  -->

  <a href="#" class="scrollup"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>

  @yield('model')
  @yield('js')

</body>

</html>
