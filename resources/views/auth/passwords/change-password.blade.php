@extends('front_layouts.admin-layout')

@section('content')

<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="w-100 profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    @include('tools.errors')
                    <form method="POST" action="{{ route('change.password.request')}}">
                        @csrf
                        <div class="card">
                            <div class="card-header">Change Password</div>
                            <div class="card-body row">
                                <div class="form-group col-md-4">
                                    <label>Old Password</label>
                                    <input type="password" name="old_password" id="old_password" class="form-control" placeholder="Enter Current Password" required>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>New Password</label>
                                    <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter New Password" required>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Confirm Password</label>
                                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Enter Confirmed Password" required>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="text-right">
                           
                                <button type="submit" class="btn btn-shukul">Save</button>
                            
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<!--
<section class="py-0 profile">
    <div class="container-fluid">
        <div class="row">
            @include('front_partials.sidebar')
            <div class="col-lg-9">

            </div>

        </div>
    </div>
</section> -->
@endsection