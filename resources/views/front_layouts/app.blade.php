<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="description" content="" />
    <meta name="keywords"
        content="creative, portfolio, agency, template, theme, designed, html5, html, css3, responsive, onepage" />
    <meta name="author" content="Set Private Limited" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Online Tours & Travel @ Shukulyatra</title>

    @include('front_partials.head')

    @yield('css')

</head>

<body>

    <!-- Start Page Wrapper  -->
    <div class="page-wrapper">

        @include('front_layouts.head')

        @yield('content')

        @include('front_layouts.foot')

        @include('front_partials.foot')

    </div>
    <!-- End Page Wrapper  -->

    <a href="#" class="scrollup"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>

    @yield('model')
    @yield('js')

</body>
</html>