<!-- Header Section Start -->
<header class="header header-style-1 clearfix">

    <div class="top-bar ">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <ul class="contact-info h-100">
                        <li class="w-50">
                            <marquee> Honeymoon Goa Tour - 6 NIGHTS / 7 DAYS</marquee>
                        </li>

                        <li class="toll">
                            <div>Tollfree -</div><span class="tollfree"> 1800-120-008866</span>
                        </li>

                    </ul>
                </div>
                <div class="col-md-5">
                    <div class="social-icons">
                        <ul>
                            <li><a href="https://twitter.com/shukulyatra" target="_blank"><i class=" fa
                                    fa-twitter"></i></a></li>
                            <li><a href="https://instagram.com/shukulyatrainfo" target="_blank"><i class="fa fa-instagram"></i></a>
                            </li>

                            <li><a href="https://www.facebook.com/shukulyatra" target="_blank"><i class="fa fa-facebook"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="menu-style bg-transparent menu-hover-2  clearfix">
        <!-- main-navigation start -->
        <div class="main-navigation main-mega-menu animated">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- header dropdown buttons end-->
                    <a class="navbar-brand" href="{{route('home')}}">
                        <img id="logo_img" src="{{asset('front/images/logo.png')}}" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-1" aria-controls="navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbar-collapse-1">

                        <!-- main-menu -->
                        <ul class="navbar-nav ml-xl-auto">
                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Company</a>

                                <div class="dropdown-menu left-1">
                                    <div class="dropdown">
                                        <a class="dropdown-item" href="{{route('company.about.us')}}">About Us</a>
                                        <a class="dropdown-item" href="{{route('company.management')}}">Management </a>
                                        <a class="dropdown-item" href="{{route('company.vision.mission')}}">Vision & Mission</a>
                                        <a class="dropdown-item" href="{{route('company.values')}}">Our Values </a>
                                        <a class="dropdown-item" href="{{route('company.groups')}}">Group of Companies</a>
                                        <a class="dropdown-item" href="{{route('company.legal.docs')}}">Legal Documents</a>
                                        <a class="dropdown-item" href="{{route('company.bankinfo')}}">Bank Info</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Packages</a>

                                <div class="dropdown-menu left-1">
                                    <div class="dropdown">
                                        <a class="dropdown-item" href="{{route('coming-soon')}}">Premium Packages</a>
                                        <a class="dropdown-item" href="{{route('coming-soon')}}">Exclusive Packages</a>
                                        <a class="dropdown-item" href="{{route('coming-soon')}}">Special Packages</a>
                                        <a class="dropdown-item" href="{{route('coming-soon')}}">Regular Offers</a>
                                        <a class="dropdown-item" href="{{route('coming-soon')}}">Amenity Offers</a>
                                        <a class="dropdown-item" href="{{route('coming-soon')}}">Latest Offers</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">General Tours</a>
                                <div class="dropdown-menu left-1">
                                    <div class="dropdown">
                                        <a class="dropdown-item" href="{{route('our-trips')}}">Our trips</a>
                                        <a class="dropdown-item" href="{{route('popular-trips')}}">Our popular
                                            tour</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('our.advisor')}}" class="nav-link">Our Advisors</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('gallery')}}" class="nav-link">Gallery</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('contact.us')}}" class="nav-link">Contact</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</a>
                                <div class="dropdown-menu left-1">
                                    <div class="dropdown">
                                        <a class="dropdown-item" href="{{route('blogs')}}">Our Blogs</a>
                                        <a class="dropdown-item" href="{{route('faq')}}">FAQ's</a>
                                        <!-- <a class="dropdown-item" href="popular-tour.html">Download</a> -->
                                        <div class="dropdown">
                                            <a class="dropdown-toggle dropdown-item" data-toggle="dropdown" href="#">Download</a>

                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{route('coming-soon')}}">Basic</a>
                                                <a class="dropdown-item" href="{{route('coming-soon')}}">Brochures</a>
                                                <a class="dropdown-item" href="{{route('coming-soon')}}">Important notes</a>
                                                <a class="dropdown-item" href="{{route('coming-soon')}}">Tips & Guidelines</a>
                                                <a class="dropdown-item" href="{{route('coming-soon')}}">New Updates</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <!-- header dropdown buttons -->
                        <div class="dropdown-buttons">
                            <div class="btn-group">
                                @if (Auth::check()) <a href="{{route('customer.dashboard')}}" class="btn btn-yatra btn-sm">
                                    My Account </a> @else <button type="button" class="btn btn-yatra  dropdown-toggle btn-sm" id="header-drop-4" data-toggle="modal" data-target="#login"> Login @endif</button>

                            </div>
                        </div>
                        <!-- main-menu end -->
                    </div>
                </div>
            </nav>
        </div>
        <!-- main-navigation end -->
    </div>
</header>
<!-- Header Section End -->

<!-- Login Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0 p-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-5 m-4">
                <div class="text-center mb-5">
                    <img src="{{asset('front/images/logo.png')}}" class="img-fluid" style="width: 215px">
                </div>
                <form class="form-column" id="login-form" method="POST" action="{{ route('admin.login.request') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="alert-msg"></div>

                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control rounded" id="username" name="username" aria-describedby="user" placeholder="Enter Username">
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password
                            <!--(<a href="javascript:void(0)" data-dismiss="modal"
                                data-toggle="modal" data-target="#forgot">Forgot?</a>)--></label>
                        <input type="password" name="password" class="form-control rounded" id="password" placeholder="Enter Password">
                        <div class="invalid-feedback">
                            Looks fail!
                        </div>
                    </div>


                    <button type="submit" class="btn btn-block btn-lg btn-yatra mt-5">Sign In</button>
                    <div class="text-center"> <a type="submit" href="{{url('register')}}" class="btn btn-link" style="font-size: 1.25rem;    color: #091426;">Signup</a></div>
                </form>


            </div>

        </div>
    </div>
</div>

<!-- Forgot Modal -->
<div class="modal fade" id="forgot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0 p-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-5 m-4">
                <div class="text-center mb-5">
                    <h3>Forgot?</h3>
                </div>
                <form class="form-column">
                    <div class="form-group">
                        <label for="reference">Username /Reference ID</label>
                        <input type="text" class="form-control rounded" id="reference" aria-describedby="user" placeholder="Enter Username or Reference ID">
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>

                    <div id="form-messages1"> Msg here Forgot Link Send or anything</div>

                    <button type="submit" class="btn btn-block btn-lg btn-yatra mt-5">Send</button>
                    <div class="text-center"> <a type="submit" href="javascript:void(0)" data-dismiss="modal" data-toggle="modal" data-target="#login" class="btn btn-link">Back to login</a>
                    </div>
                </form>


            </div>

        </div>
    </div>
</div>