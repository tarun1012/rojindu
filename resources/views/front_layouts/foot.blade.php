<!-- Footer Style Seven Start -->
<footer class="footer bg-faded py-5 bg-theme-color-2">
    <div class="container">
        <div class="row">

            <div class="footer-item col-12">
                <h6 class="mb-4 text-center">Subscribe to our <span>newsletter</span> for updates and special offers!</h6>
                <div class="mb-5">
                    <div class="input-group subscribe-style-two m-auto w-75">
                        <input type="email" class="form-control input-subscribe" placeholder="Your Email...">
                        <span class="input-group-btn">
                            <button class="btn btn-subscribe" type="button">Subscribe</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="footer-item footer-widget-one">
                        <img class="footer-logo mb-3" src="{{asset('front/images/logo.png')}}" alt="">
                        <p>Shukulyatra is a tours and travel company that redefines travel goals through fantastic value
                            deals.</p>
                        <hr>
                        <h6>Follow<span> Us</span></h6>
                        <ul class="social-icon bg-transparent bordered-theme">
                            <li><a href="https://twitter.com/shukulyatra" target="_blank"><i class=" fa
                                    fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/shukulyatrainfo/" target="_blank"><i class="fa fa-instagram"></i></a>
                            </li>

                            <li><a href="https://www.facebook.com/shukulyatra" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="footer-item">
                        <div class="footer-title">
                            <h4>Member <span>Services</span></h4>
                            <div class="border-style-3"></div>
                        </div>
                        <ul class="footer-list">
                            <li><a href="{{route('register')}}">Sign up</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#login">Login</a></li>
                            <li><a href="{{route('contact.us')}}">Help and Support</a></li>

                            <!-- <li><a href="#">Visit our help section</a></li>
                            <li><a href="#">Donation Fund</a></li> -->
                        </ul>

                    </div>
                </div>


                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="footer-item">
                        <div class="footer-title">
                            <h4>Useful <span>Links</span></h4>
                            <div class="border-style-3"></div>
                        </div>
                        <ul class="footer-list">
                            <li><a href="{{route('company.about.us')}}">About Us</a></li>
                            <li><a href="{{route('company.management')}}">Team</a></li>
                            <li><a href="{{route('terms.condition')}}">Terms & Conditions</a></li>
                            <li><a href="{{route('privacy.policy')}}">Privacy Policy</a></li>

                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="footer-item">
                        <div class="footer-title">
                            <h4>Quick <span>Contact</span></h4>
                            <div class="border-style-3"></div>
                        </div>
                        <ul class="footer-list footer-contact mb-5">
                            <li>Tollfree- 1800-120-008866</li>
                            <li><i class="fa fa-phone" aria-hidden="true"></i> +91 75740 95555</li>
                            <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:"> info@shukulyatra.com</a></li>
                        </ul>
                        {{-- <div class="footer-item">
                            <h6>News <span>letter</span></h6>
                            <form method="POST" id="NewsLetterForm">
                                @csrf
                                <div class="input-group subscribe-style-two">
                                    <input type="email" id="newsletter_email" name="newsletter_email" class="form-control input-subscribe" placeholder="Email">
                                    <span class="newsletter-error-msg"></span>
                                    <span class="input-group-btn">
                                        <button class="btn btn-subscribe subscribe-newsletter" type="button">Subscribe</button>
                                    </span>
                                </div>
                                <span class="newsletter-msg"></span>
                            </form>
                        </div> --}}
                        <div class="footer-item">
                            <h6>Download <span>our brand new app</span></h6>
                            <!-- <div class="input-group subscribe-style-two">
                                <input type="email" class="form-control input-subscribe" placeholder="Email">
                                <span class="input-group-btn">
                                    <button class="btn btn-subscribe" type="button">Subscribe</button>
                                </span>
                            </div> -->
                            <ul class="d-flex">
                                <li class="mr-1"><a href="" target="_blank"><img src="{{asset('front/images/android.png')}}" class="img-fluid"></a></li>
                                <li class=""><a href="" target="_blank"><img src="{{asset('front/images/ios.png')}}" class="img-fluid"></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<section class="footer-copy-right text-white p-0">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <p>© {{date('Y')}}, All Rights Reserved</p>
                <p> Developed by <a href="https://daddyscode.com" target="_blank"> Daddyscode</a></p>
            </div>
        </div>
    </div>
</section>
<!-- Footer Style Seven End -->