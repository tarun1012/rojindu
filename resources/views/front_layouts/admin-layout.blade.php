<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="description" content="" />
  <meta name="keywords" content="creative, portfolio, agency, template, theme, designed, html5, html, css3, responsive, onepage" />
  <meta name="author" content="Set Private Limited" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title>Online Tours & Travel @ Rojindu</title>
  <link rel="stylesheet" href="{{asset('front/css/all.min.css')}}">
  @include('front_partials.head')

  @yield('css')

</head>

<body>

  <!-- Start Page Wrapper  -->
  <div class="page-wrapper">
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-2 static-top shadow2">
      <a href="{{url('/')}}" style="font-size: 20px;font-weight: 800;color: #5ba92a;">Rojindu</a>
      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow ">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 ">@if (Auth::check()) My Account @else Login @endif</span>
            <img class="img-profile rounded-circle" onerror="this.src='{{asset('front/images/user.png')}}';" src="{!! asset('profiles/'.Auth::user()->profilepic) !!}" style="width:2rem;">
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                @if (get_user_permission("my_profile","view"))
                @if(Auth::user()->roleid == '11')
               <a class="dropdown-item" href="{{route('rmyprofile.view')}}">My Profile </a>
               @else
                 <a class="dropdown-item" href="{{route('amyprofile.view')}}">My Profile </a>
                @endif
              @endif
                 @if(get_user_permission("change_password","edit"))
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{route('change.password')}}">Change Password </a>
              @endif 
            
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('admin.logout') }}"> Logout </a>
          </div>
        </li>

      </ul>

     
     

    </nav>

    @yield('content')


    @include('front_partials.foot')

  </div>
  <!-- End Page Wrapper  -->

  <a href="#" class="scrollup"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>

  @yield('model')
  @yield('js')

</body>

</html>