@extends('front_layouts.admin-layout')
@section('css')
<link rel="stylesheet" href="{{asset('front/css/dropify.min.css')}}">
@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span> 
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('product')}}" class="text-muted"><i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            @if (!empty($product))
                            <form method="post" action="{{ route('product.update', $product->id) }}" enctype="multipart/form-data">
                                <h3 class="mb-3">Product Information:</h3>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="tour-name">Image: <span class="text-danger">*</span></label>
                                        <input type="file" name="image" class="dropify" value="" data-default-file="{{asset('uploads/product/'.$product->image)}}" accept="image/*" />
                                        <input type="hidden" name="product_path" value="{{$product->image}}" />
                                    </div>
                                    <span class="banner_image_msg"></span>
                                </div>

                                 <div class="form-row">
                                    <div class="form-group col-md-3">

                                                <label for="Category">Category</label>
                                                <select class="form-control" id="category" name="category">
                                                    <option value="">Choose category</option>
                                                    @foreach ($categories as $category)
                                                    <option value="{{$category->id}}" {{ isset($product) ? (($product->category_id == $category->id) ? 'selected' : '') : '' }}> {{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                               
                                            </div>

                                            <div class="form-group col-md-3">

                                                <label for="Category">SubCategory</label>
                                                <select class="form-control" id="subcategory" name="subcategory">
                                                    <option value="">Choose subcategory</option>
                                                    @foreach ($subcategories as $subcategory)
                                                    <option value="{{$subcategory->id}}" {{ isset($product) ? (($product->subcategory_id == $subcategory->id) ? 'selected' : '') : '' }}> {{ $subcategory->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                            <div class="form-group col-md-3">

                                                <label for="Category">Brand</label>
                                                <select class="form-control" id="brand" name="brand">
                                                    <option value="">Choose Brand</option>
                                                    @foreach ($brands as $brand)
                                                    <option value="{{$brand->id}}" {{ isset($product) ? (($product->brand_id == $brand->id) ? 'selected' : '') : '' }}> {{ $brand->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                            <div class="form-group col-md-3">

                                                <label for="Category">Unit</label>
                                                <select class="form-control" id="unit" name="unit">
                                                    <option value="">Choose Unit</option>
                                                    @foreach ($units as $unit)
                                                    <option value="{{$unit->id}}" {{ isset($product) ? (($product->unit == $unit->id) ? 'selected' : '') : '' }}> {{ $unit->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                </div>
                               
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name">Name: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$product->name}}" id="name" name="name" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="price">Price: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$product->price}}" id="price" name="price" />
                                    </div>
                                   
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="content">Content:</label>
                                        <textarea type="text" class="form-control" placeholder="" id="product-content" name="product-content">{{$product->detail}}</textarea>
                                    </div>
                                </div>

                                <h3 class="mb-3 mt-3"> Image:</h3>


                                @if (!empty($ProductGallery))
                                <input type="hidden" name="tour_gallery_path[]" value="{{$ProductGallery}}" />
                                <input type="hidden" name="csrf-token" content="{{csrf_token()}}" />
                                <label for="gallery_images">Gallery Image:</label>
                                <div class="form-row">

                                    @foreach ($ProductGallery as $item)
                                    <div class="form-group col-md-3 gallery-image-block position-relative" data-gallery_row_id="{{$item->id}}">
                                        <img src="{{asset('uploads/gallery_images/'.$item->image)}}">
                                        <button type="button" data-gallery_id="{{$item->id}}" data-gallery_image="{{$item->image}}" class="btn position-absolute btn-sm btn-danger btn-rounded delete_gallery_image" style="top:-10px;right:3px"><i class="fa fa-remove"></i></button>
                                    </div>
                                    @endforeach
                                </div>
                                @else

                                @endif

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="tour-name">Gallery: <span class="text-danger">*</span></label>
                                        <input type="file" name="product_gallery[]" class="dropify" multiple />
                                    </div>
                                </div>
                                
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="status">Status: <span class="text-danger">*</span></label>
                                        <label class="switch ">
                                            <input type="checkbox" value="1" class="form-check-input" {{$category->status == 1 ? "checked" : ""}} name="status" id="status">
                                            <span class="slider round"></span>
                                        </label>
                                       
                                    </div>
                                </div>
                                @else
                                <form action="{{route('product.store')}}" method="post" id="ProductForm" enctype="multipart/form-data">
                                    <h3 class="mb-3">Product Information:</h3>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="tour-name">Image: <span class="text-danger">*</span></label>
                                            <input type="file" name="image" class="dropify" required accept="image/*" />
                                        </div>
                                        <span class="banner_image_msg"></span>
                                    </div>

                                    <div class="form-row">  
                                    <div class="form-group col-md-3">

                                                <label for="Category">Category</label>
                                                <select class="form-control" id="category" name="category">
                                                    <option value="">Choose category</option>
                                                    @foreach ($categories as $category)
                                                    <option value="{{$category->id}}"> {{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>

                                            <div class="form-group col-md-3">

                                                <label for="Category">SubCategory</label>
                                                <select class="form-control" id="subcategory" name="subcategory">
                                                    <option value="">Choose Subcategory</option>
                                                    @foreach ($subcategories as $subcategory)
                                                    <option value="{{$subcategory->id}}"> {{ $subcategory->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>

                                            <div class="form-group col-md-3">

                                                <label for="Category">Brand</label>
                                                <select class="form-control" id="brand" name="brand">
                                                    <option value="">Choose Brand</option>
                                                    @foreach ($brands as $brand)
                                                    <option value="{{$brand->id}}"> {{ $brand->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                             <div class="form-group col-md-3">

                                                <label for="Category">Unit</label>
                                                <select class="form-control" id="unit" name="unit">
                                                    <option value="">Choose Unit</option>
                                                    @foreach ($units as $unit)
                                                    <option value="{{$unit->id}}" > {{ $unit->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="name">Name: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="" value="" id="name" name="name" />
                                        </div>

                                         <div class="form-group col-md-12">
                                            <label for="price">Price: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="" value="" id="price" name="price" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="content">Content:</label>
                                        <textarea type="text" class="form-control" placeholder="" id="product-content" name="product-content"></textarea>
                                    </div>
                                </div>

                                <h3 class="mb-3 mt-3">Product Gallery:</h3>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="tour-name">Featured Image: <span class="text-danger">*</span></label>
                                        <input type="file" name="product_gallery[]" class="dropify" multiple />
                                    </div>
                                </div>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="status">Status: <span class="text-danger">*</span></label>
                                            <label class="switch ">
                                                <input type="checkbox" value="1" class="form-check-input" name="status" id="status">
                                                <span class="slider round"></span>
                                            </label>
                                            <!-- <div class="form-check">
                                                    <input type="checkbox" value="1" class="form-check-input" name="status" id="status">
                                                    <label class="form-check-label" for="exampleCheck1">Active</label>
                                                </div> -->
                                        </div>
                                    </div>
                                    @endif
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-12 text-right">
                                            <button type="submit" class="btn btn-yatra">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection

@section('js')
<script src="{{asset('front/js/dropify.min.js')}}"></script>
<script src="{{asset('front/plugins/ckeditor/ckeditor.js')}}"></script>
<script>
     $('.delete_gallery_image').click(function() {
        if (confirm('Are you sure you want to delete?')) {
            var id = $(this).attr("data-gallery_id");
            var path = $(this).attr("data-gallery_image");
            var token = $('input[name="csrf-token"]').attr('content');

            $.ajax({
                url: "{{url('/product-gallery/delete')}}/" + id,
                type: 'POST',
                data: {
                    "id": id,
                    "path": path,
                    "_token": token,
                },
                success: function(res) {
                    if (res == "success") {
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        alert("Failed to delete gallery image item.");
                    }
                }
            });
        }
    });

    jQuery.validator.addMethod("validUrl", function(value, element) {
        return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
    }, "Invalid URL.");

    $('#ProductForm').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
            if (element.attr("name") == "image") {
                error.insertAfter(".banner_image_msg");
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            name: "required",
            price: "required",
            unit: "required",
            brand: "required",
            subcategory: "required",
            category: "required",
            
            status: "required"
        }
    });

    $(document).ready(function() {
        $('.dropify').dropify();
        CKEDITOR.replace('product-content');
    });

     $('#category').change(function() {
            var cID = $(this).val();
            if (cID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-subcategory-list')}}?c_id=" + cID,
                    success: function(res) {
                        console.log(res);
                        if (res) {
                            $("#subcategory").empty();
                            $("#subcategory").append('<option value="">Choose subcategory</option>');
                            $.each(res, function(key, value) {
                                $("#subcategory").append('<option value="' + key + '">' + value + '</option>');
                            });
                            
                        } else {
                            $("#subcategory").empty();
                            $("#subcategory").append('<option value="">Choose State</option>');
                            
                        }
                    }
                });
            } else {
                $("#subcategory").empty();
                $("#subcategory").append('<option value="">Choose subcategory</option>');
                
            }
        });
</script>
@endsection