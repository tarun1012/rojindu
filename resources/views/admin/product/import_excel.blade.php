@extends('front_layouts.admin-layout')
@section('css')
<link rel="stylesheet" href="{{asset('front/css/dropify.min.css')}}">
@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span> 
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>

            <br />
   @if(count($errors) > 0)
    <div class="alert alert-danger">
     Upload Validation Error<br><br>
     <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif

    @if($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('product')}}" class="text-muted"><i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                                
                                <form action="{{ route('import') }}" method="post" id="ProductForm" enctype="multipart/form-data">
                                    <h3 class="mb-3">Product Information:</h3>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="tour-name">Select File for Upload(.xls, .xslx) <span class="text-danger">*</span></label>
                                            <input type="file" name="select_file" />
                                        </div>
                                        <span class="banner_image_msg"></span>
                                    </div>

                                    
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-12 text-right">
                                            <button type="submit" name="upload" class="btn btn-yatra">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection

@section('js')
<script src="{{asset('front/js/dropify.min.js')}}"></script>
<script src="{{asset('front/plugins/ckeditor/ckeditor.js')}}"></script>
<script>
     $('.delete_gallery_image').click(function() {
        if (confirm('Are you sure you want to delete?')) {
            var id = $(this).attr("data-gallery_id");
            var path = $(this).attr("data-gallery_image");
            var token = $('input[name="csrf-token"]').attr('content');

            $.ajax({
                url: "{{url('/product-gallery/delete')}}/" + id,
                type: 'POST',
                data: {
                    "id": id,
                    "path": path,
                    "_token": token,
                },
                success: function(res) {
                    if (res == "success") {
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        alert("Failed to delete gallery image item.");
                    }
                }
            });
        }
    });

    jQuery.validator.addMethod("validUrl", function(value, element) {
        return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
    }, "Invalid URL.");

    $('#ProductForm').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
            if (element.attr("name") == "image") {
                error.insertAfter(".banner_image_msg");
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            name: "required",
            price: "required",
            unit: "required",
            brand: "required",
            subcategory: "required",
            category: "required",
            
            status: "required"
        }
    });

    $(document).ready(function() {
        $('.dropify').dropify();
        CKEDITOR.replace('product-content');
    });

     $('#category').change(function() {
            var cID = $(this).val();
            if (cID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-subcategory-list')}}?c_id=" + cID,
                    success: function(res) {
                        console.log(res);
                        if (res) {
                            $("#subcategory").empty();
                            $("#subcategory").append('<option value="">Choose subcategory</option>');
                            $.each(res, function(key, value) {
                                $("#subcategory").append('<option value="' + key + '">' + value + '</option>');
                            });
                            
                        } else {
                            $("#subcategory").empty();
                            $("#subcategory").append('<option value="">Choose State</option>');
                            
                        }
                    }
                });
            } else {
                $("#subcategory").empty();
                $("#subcategory").append('<option value="">Choose subcategory</option>');
                
            }
        });
</script>
@endsection