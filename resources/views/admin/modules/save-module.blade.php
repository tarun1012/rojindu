@extends('front_layouts.admin-layout')
@section('content')

<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class=" profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('modules')}}" class="text-muted"><i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h5>Create Modules</h5>
                        </div>

                        @if(isset($module))
                        <form class="form theme-form" method="POST" action="{{ route('module.request.update', $module->id) }}">
                            @else
                            <form class="form theme-form" method="POST" action="{{ route('module.request') }}">
                                @endif
                                @csrf
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input class="form-control" id="name" type="text" name="name" placeholder="Enter Module Name" value="{{ $module->name ?? ''}}">
                                                @error('name')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="status">Status: <span class="text-danger">*</span></label>
                                            <label class="switch ">
                                                <input type="checkbox" value="1" class="form-check-input" {{ isset($module) ? (($module->status == 1) ? 'checked' : '') : '' }} name="status" id="status">
                                                <span class="slider round"></span>
                                            </label>
                                            <!-- <div class="form-check">
                                                    <input type="checkbox" value="1" class="form-check-input" {{ isset($module) ? (($module->status == 1) ? 'checked' : '') : '' }} name="status" id="status">
                                                    <label class="form-check-label" for="exampleCheck1">Active</label>
                                                </div> -->
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-shukul" type="submit"> {{ isset($module) ? 'Update' : 'Submit' }} </button>
                                    <a href="{{ URL::previous() }}" class="btn btn-light">Cancel</a>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection