@extends('front_layouts.admin-layout')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="card">
                        <div class="card-header p-0">
                            <ul class="nav nav-tabs profiletab" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?= ($activeTab == "profile" ? "active" : ""); ?>" id="home-tab" href="{{route('rmyprofile.view')}}" role="tab" aria-controls="home" aria-selected="true">My Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?= ($activeTab == "kyc_info" ? "active" : ""); ?>" id="profile-tab" href="{{route('myprofile.view.kyc_info')}}" role="tab" aria-controls="profile" aria-selected="false">Kyc
                                        Info</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?= ($activeTab == "kyc_history" ? "active" : ""); ?>" id="contact-tab" href="{{route('myprofile.view.kyc_history')}}" role="tab" aria-controls="contact" aria-selected="false">Kyc
                                        History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" href="#invoice" role="tab" aria-controls="contact" aria-selected="false">Invoice</a>
                                </li> 
                            </ul>
                        </div>
                    </div>
                    <div class="pt-2">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show <?= ($activeTab == "profile" ? "active" : ""); ?>" id="profile" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header d-flex justify-content-between align-items-center">
                                                <div>My Profile</div>
                                                <div class="form-group mb-0">
                                                    <div class="contact-textarea text-center">
                                                        @if (get_user_permission("my_profile","edit"))
                                                        <a href="{{ route('rprofile.update')}}" class="btn btn-shukul btn-sm">Edit Profile</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">

                                                <div class="changeprofile">
                                                    <div>
                                                        <img onError="this.onerror=null;this.src='{{asset("front/images/user.png")}}';" alt="Saraswatha Granules" class="img-responsive ng-star-inserted" src="{{ asset('profiles/'.Auth::user()->profilepic)}}">
                                                    </div>
                                                    <div class="ml-3">
                                                        <h2>{{ Auth::user()->email }}</h2>
                                                        <div class="mute">{{ getRole() }}</div>
                                                    </div>

                                                     @if (get_user_permission("retailer_close_account_req","view"))

                                                    <div class="col-md-6 text-right">
                                                         <button class="btn btn-success btn-sm btn-rounded changeRequest mr-1" data-id="{{Auth::user()->id}}" data-status="1">Send Request To Close Account</button>
                                                    </div>
                                                     @endif

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 d-flex align-self-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">User Info</div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label class="col-4 col-form-label">Name</label>
                                                    <label class="col-8 col-form-label"> : {{ Auth::user()->name }}</label>
                                                    <label class="col-4 col-form-label">Mobile</label>
                                                    <label class="col-8 col-form-label">: {{ Auth::user()->contactno }}</label>
                                                    <label class="col-4 col-form-label">Email</label>
                                                    <label class="col-8 col-form-label">: {{ Auth::user()->email }}</label>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 d-flex align-self-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">Address Info</div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label class="col-3 col-form-label">Locality</label>
                                                    <label class="col-9 col-form-label"> : {{ Auth::user()->locality }}</label>
                                                    <label class="col-3 col-form-label">Country</label>
                                                    <label class="col-9 col-form-label">: {{ getCountry() }}</label>
                                                    <label class="col-3 col-form-label">State</label>
                                                    <label class="col-9 col-form-label">: {{getState()}}</label>
                                                    <label class="col-3 col-form-label">City</label>
                                                    <label class="col-9 col-form-label">: {{getCity()}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show <?= ($activeTab == "kyc_info" ? " active" : ""); ?>" id="info" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header text-right">
                                                <a type="button" href="{{ route('profile.editbankdetail') }}" class="btn btn-shukul btn-sm my-2">Edit Info</a>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-lg-6 d-flex align-self-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">Bank Info</div>
                                            <div class="card-body">

                                                <div class="form-group row">
                                                    <label class="col-lg-6 col-form-label">Bank Name</label>
                                                    <label class="col-lg-6 col-form-label"> : {!! $userDoc->bank_name ?? '' !!} </label>
                                                    <label class="col-lg-6 col-form-label">Account Name</label>
                                                    <label class="col-lg-6 col-form-label">: {!! $userDoc->account_name ?? '' !!} </label>
                                                    <label class="col-lg-6 col-form-label">Account Number</label>
                                                    <label class="col-lg-6 col-form-label">: {!! $userDoc->account_number ?? '' !!} </label>
                                                    <label class="col-lg-6 col-form-label">IFSC</label>
                                                    <label class="col-lg-6 col-form-label">: {!! $userDoc->ifsc_code ?? '' !!}</label>
                                                </div>
                                                <div class="form-group d-flex">
                                                    @if(!empty($bank_document))
                                                    @foreach(json_decode($bank_document->image) as $key => $file)
                                                    <img src="{!! asset('documents').'/'.$file !!}" style="width:100px;height:100px;" class="img-fluid my-2 mr-2" width="80px" id="myImg">
                                                    @endforeach
                                                    @else
                                                    <img src="{{ asset('front/images/placeholder.jpg') }}" style="width:100px;height:100px;" class="img-fluid my-2 mr-2" width="80px" id="myImg">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 d-flex align-self-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">Aadhar Info</div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label class="col-lg-6 col-form-label">Aadhar Name</label>
                                                    <label class="col-lg-6 col-form-label"> : {!! $userDoc->aadhar_name ?? '' !!} </label>
                                                    <label class="col-lg-6 col-form-label">Aadhar Number</label>
                                                    <label class="col-lg-6 col-form-label">: {!! $userDoc->aadhar_number ?? '' !!} </label>
                                                </div>
                                                <div class="form-group d-flex">
                                                    @if(!empty($aadhar_document))
                                                    @foreach(json_decode($aadhar_document->image) as $key => $file)
                                                    <img src="{!! asset('documents').'/'.$file !!}" style="width:100px;height:100px;" class="img-fluid my-2 mr-2" width="80px" id="myImg">
                                                    @endforeach
                                                    @else
                                                    <img src="{{ asset('front/images/placeholder.jpg') }}" style="width:100px;height:100px;" class="img-fluid my-2 mr-2" width="80px" id="myImg">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 d-flex align-self-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">Pancard Info</div>
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <label class="col-lg-6 col-form-label">Pancard Name</label>
                                                    <label class="col-lg-6 col-form-label"> : {!! $userDoc->pan_name ?? '' !!}</label>
                                                    <label class="col-lg-6 col-form-label">Pan Number</label>
                                                    <label class="col-lg-6 col-form-label">: {!! $userDoc->pan_number ?? '' !!}</label>
                                                </div>
                                                <div class="form-group d-flex">
                                                    @if(!empty($pan_document))
                                                    @foreach(json_decode($pan_document->image) as $key => $file)
                                                    <img src="{!! asset('documents').'/'.$file !!}" style="width:100px;height:100px;" class="img-fluid my-2 mr-2" width="80px" id="myImg">
                                                    @endforeach
                                                    @else
                                                    <img src="{{ asset('front/images/placeholder.jpg') }}" style="width:100px;height:100px;" class="img-fluid my-2 mr-2" width="80px" id="myImg">
                                                    @endif
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-6 d-flex align-self-stretch">
                                        <div class="card w-100">

                                            <div class="card-body text-center">
                                                <div class="py-4">
                                                    <img src="{{asset('front/images/invalid.png')}}" style="width:100px;" class="img-fluid">
                                                    <!-- valid image -->
                                                    <!-- <img src="images/valid.png" class="img-fluid"> -->
                                                    <h5 class="mt-4">All KYC is required</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show <?= ($activeTab == "kyc_history" ? "active" : ""); ?>" id="history" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="row">
                                    <div class="col-lg-12 d-flex align-slef-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">Kyc History</div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="kycTable" class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Request ID</th>
                                                                <th>Username</th>
                                                                <th>Field Name</th>
                                                                <th>Old Value</th>
                                                                <th>New Value</th>
                                                                <th>Request Date</th>
                                                                <th>Status</th>
                                                                <th>Admin Comment</th>
                                                                <th>Verified Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (!empty($userDocInfo))
                                                                @foreach ($userDocInfo as $infoDoc)
                                                            <tr>
                                                                <td> REQ_{{ $infoDoc->id }} </td>
                                                                <td> {{ $infoDoc->getUsername->email}} </td>
                                                                <td> {{ $infoDoc->fieldname }} </td>
                                                                <td>
                                                                    @if($infoDoc->type == 'file')
                                                                    @if($infoDoc->oldvalue)
                                                                    <div class="row">
                                                                        @foreach (json_decode($infoDoc->oldvalue) as $olddocFile)
                                                                        <div class="col-md-6">
                                                                            <a href="{!! asset('documents').'/'.$olddocFile !!}" class="icon lightbox-image"><img src="{!! asset('documents').'/'.$olddocFile !!}" style="width:50px; height:50px;"></a>
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                    @else
                                                                    N/A
                                                                    @endif
                                                                    @else
                                                                    {{ ($infoDoc->oldvalue) ? $infoDoc->oldvalue : 'N/A' }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if($infoDoc->type == 'file')
                                                                    @if($infoDoc->newvalue)
                                                                    <div class="row">
                                                                        @foreach (json_decode($infoDoc->newvalue) as $newFile)
                                                                        <div class="col-md-6">
                                                                            <a href="{!! asset('documents').'/'.$newFile !!}" class="icon lightbox-image"><img src="{!! asset('documents').'/'.$newFile !!}" style="width:50px; height:50px;"></a>
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                    @else
                                                                    N/A
                                                                    @endif
                                                                    @else
                                                                    {{ ($infoDoc->newvalue) ? $infoDoc->newvalue : 'N/A' }}
                                                                    @endif
                                                                </td>
                                                                <td> {{ $infoDoc->created_at }} </td>
                                                                <td> {{ $infoDoc->status }} </td>
                                                                <td> {{ $infoDoc->admincomment }} </td>
                                                                <td>{{ ($infoDoc->updated_at) ? $infoDoc->updated_at : "N/A" }}</td>
                                                            </tr>
                                                            @endforeach
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane fade" id="invoice" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="row">
                                    <div class="col-lg-12 d-flex align-slef-stretch">
                                        <div class="card w-100">
                                            <div class="card-header">Invoice Receipt</div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table ">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Package Name</th>
                                                                <th scope="col">Status</th>
                                                                <th scope="col">Used Tour</th>
                                                                <th scope="col">Pending Tour</th>
                                                                <th scope="col">Payment</th>
                                                                <th scope="col">Active Date</th>
                                                                <th scope="col">Expiry Date</th>
                                                                <th scope="col">Invoice</th>

                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Standard</td>
                                                                <td><span calss="success">Active</span></td>
                                                                <td>2</td>
                                                                <td>1</td>
                                                                <td><span class="success">Done</span></td>
                                                                <!-- <td><span class="reject">Done</span></td> -->
                                                                <td>12/12/12</td>
                                                                <td>12/12/12</td>
                                                                <td><i class="fa fa-file-pdf-o reject pointer" aria-hidden="true"></i>
                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#kycTable').DataTable();
    });

    $('.changeRequest').click(function() {
        var id = $(this).attr("data-id");
        var status = $(this).attr("data-status");
        Swal.fire({
        title: 'Are you sure?',
        text: "You want to close account.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, close account!'
        }).then((isConfirm) => {
            if(isConfirm.value == true) {
                $.ajax({
                    type: "GET",
                    url: "{{url('close-account-request-status')}}?id=" + id + "&status=" + status,
                    success: function(res) {
                        //location.reload();
                        if (res.result == "success") {
                            $('.msg').addClass("alert alert-success");
                            $('.msg').text("Status changed successfully.");
                            $('.msg').delay(800).fadeOut('slow');
                            setTimeout(function() {
                                location.reload();
                            }, 2000);
                            location.reload();
                        } else {
                            $('.msg').addClass("alert alert-danger");
                            $('.msg').text("Failed to change status.");
                            $('.msg').delay(500).fadeOut('slow');
                        }
                        location.reload();
                    }
                });
            }
        });
    });
</script>

@endsection