@extends('front_layouts.admin-layout')
@section('css')
<link rel="stylesheet" href="{{asset('front/css/dropify.min.css')}}">
@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span> 
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('productrequest')}}" class="text-muted"><i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            @if (!empty($productrequest))
                            <form method="post" action="{{ route('productrequest.update', $productrequest->id) }}" enctype="multipart/form-data">
                                <h3 class="mb-3">Product Request Information:</h3>
                                
                               
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name">Name: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$productrequest->name}}" id="name" name="name" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="price">Price: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$productrequest->price}}" id="price" name="price" />
                                    </div>
                                   
                                </div>
                                  

                                
                                @else
                                <form action="{{route('productrequest.store')}}" method="post" id="ProductRequestForm" enctype="multipart/form-data">
                                    <h3 class="mb-3">Product Request Information:</h3>
                                    
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Name: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="Enter Product Name" value="" id="name" name="name" />
                                        </div>

                                         <div class="form-group col-md-6">
                                            <label for="price">Price: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="Enter Price" value="" id="price" name="price" />
                                        </div>
                                        
                                    </div>
 
                                    
                                    @endif
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-12 text-right">
                                            <button type="submit" class="btn btn-yatra">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection

@section('js')
<script src="{{asset('front/js/dropify.min.js')}}"></script>
<script src="{{asset('front/plugins/ckeditor/ckeditor.js')}}"></script>
<script>
    

    jQuery.validator.addMethod("validUrl", function(value, element) {
        return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
    }, "Invalid URL.");

    $('#ProductRequestForm').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
            if (element.attr("name") == "image") {
                error.insertAfter(".banner_image_msg");
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            name: "required",
            price: "required"
            
        }
    });

    $(document).ready(function() {
        $('.dropify').dropify();
        CKEDITOR.replace('product-content');
    });

    
</script>
@endsection