@extends('front_layouts.admin-layout')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span> 
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            @include('tools.errors')
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>Retailer Product </div>
                            @if (get_user_permission("manage_productrequest","add"))
                            <a href="{{route('productrequest.create')}}" class="btn btn-sm btn-rounded btn-yatra">Add Product Request</a>
                            @endif
                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="mt-3 mb-3 msg"></div>
                                <table id="ProductRequestTable" class="table table-hover  display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                           
                                            <th>Product</th>
                                            
                                            <th>Price</th>
                                            
                                            <th>Status</th>
                                            @if (get_user_permission("manage_productrequest","edit"))
                                            <th>Action</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($ProductRequestData  as $productrequest)
                                        <tr>
                                            <td>{{$productrequest->name}}</td>
                                            
                                            <td>{{$productrequest->price}}</td>
                                           
                                            
                                            <td>
                                                @if ($productrequest->status == 1)
                                                <span class="badge badge-success">Active</span>
                                                @else
                                                <span class="badge badge-danger">Paused</span>
                                                @endif
                                            </td>
                                            <td>
                                                
                                                @if (get_user_permission("manage_productrequest","edit"))
                                                <a href="{{ route('productrequest.edit', $productrequest->id) }}" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit retailer product"><i class="fa fa-edit"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#ProductRequestTable').DataTable({
            scrollY: '60vh',
            "scrollX": true
        });
    });

    
</script>
@endsection