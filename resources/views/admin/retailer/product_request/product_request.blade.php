@extends('front_layouts.admin-layout')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')

<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                @include('tools.errors')
                <div class="col-lg-12 d-flex align-slef-stretch">
                    <div class="card w-100">
                        <div class="card-header">Product Request</div>
                        <div class="card-body">
                            <div class="">
                                <table id="product_request_table" class="table table-hover nowrap display" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">Price</th>
                                            
                                            <th scope="col">Action</th>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach ($products as $product)

                                        <tr>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->price}}</td>
                                            
                                            <td>
                                                <button class="btn btn-success btn-sm btn-rounded changeRequest mr-1" data-id="{{$product->id}}" data-status="1">Approve</button>
                                                <button class="btn btn-danger btn-sm btn-rounded  changeRequest" data-id="{{$product->id}}" data-status="2">Reject</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>





@section('js')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#product_request_table').DataTable({
            scrollY: '60vh',
            "scrollX": true
        });
    });

    $('.changeRequest').click(function() {
        var id = $(this).attr("data-id");
        var status = $(this).attr("data-status");
        Swal.fire({
        title: 'Are you sure?',
        text: "You want to change request status.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, change it!'
        }).then((isConfirm) => {
            if(isConfirm.value == true) {
                $.ajax({
                    type: "GET",
                    url: "{{url('product-request-status')}}?id=" + id + "&status=" + status,
                    success: function(res) {
                        //location.reload();
                        if (res.result == "success") {
                            $('.msg').addClass("alert alert-success");
                            $('.msg').text("Status changed successfully.");
                            $('.msg').delay(800).fadeOut('slow');
                            setTimeout(function() {
                                location.reload();
                            }, 2000);
                            location.reload();
                        } else {
                            $('.msg').addClass("alert alert-danger");
                            $('.msg').text("Failed to change status.");
                            $('.msg').delay(500).fadeOut('slow');
                        }
                        location.reload();
                    }
                });
            }
        });
    });
</script>

@endsection

@endsection