@extends('front_layouts.admin-layout')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span> 
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            @include('tools.errors')
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>Retailer Product </div>
                            @if (get_user_permission("manage_retailerproduct","add"))
                            <a href="{{route('retailerproduct.create')}}" class="btn btn-sm btn-rounded btn-yatra">Add Retailer Product</a>
                            @endif
                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="mt-3 mb-3 msg"></div>
                                <table id="ReatilerProductTable" class="table table-hover  display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                           
                                            <th>Product</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>Brand</th>
                                            <th>Price</th>
                                            <th>Qty</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($RetailerProductData  as $retailerproduct)
                                        <tr>
                                            <td>{{$retailerproduct->product_name}}</td>
                                            <td>{{$retailerproduct->category_name}}</td>
                                            <td>{{$retailerproduct->subcategory_name}}</td>
                                            <td>{{$retailerproduct->brand_name}}</td>
                                            <td>{{$retailerproduct->price}}</td>
                                            <td>{{$retailerproduct->qty}}</td>
                                            
                                            <td>
                                                @if ($retailerproduct->status == 1)
                                                <span class="badge badge-success">Active</span>
                                                @else
                                                <span class="badge badge-danger">Paused</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($retailerproduct->status == 1)
                                                <button type="button" class="btn btn-rounded btn-sm btn-warning changeRetailerProductStatus" data-id="{{$retailerproduct->id}}" data-status="{{$retailerproduct->status}}" data-toggle="tooltip" title="Pause retailer product"><i class="fa fa-power-off"></i></button>
                                                @else
                                                <button type="button" class="btn btn-rounded btn-sm btn-success changeRetailerProductStatus" data-id="{{$retailerproduct->id}}" data-status="{{$retailerproduct->status}}" data-toggle="tooltip" title="Activate retailer product"><i class="fa fa-refresh"></i></button>
                                                @endif
                                                @if (get_user_permission("manage_retailerproduct","edit"))
                                                <a href="{{ route('retailerproduct.edit', $retailerproduct->id) }}" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit retailer product"><i class="fa fa-edit"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#RetailerProductTable').DataTable({
            scrollY: '60vh',
            "scrollX": true
        });
    });

    $('.changeRetailerProductStatus').click(function() {
        var id = $(this).attr("data-id");
        var status = $(this).attr("data-status");
        $.ajax({
            type: "GET",
            url: "{{url('change-retailerproduct-status')}}?id=" + id + "&status=" + status,
            success: function(res) {
                console.log(res);
                if (res == "success") {
                    $('.msg').addClass("alert alert-success");
                    $('.msg').text("Status changed successfully.");
                    $('.msg').delay(800).fadeOut('slow');
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else {
                    $('.msg').addClass("alert alert-danger");
                    $('.msg').text("Failed to change status.");
                    $('.msg').delay(500).fadeOut('slow');
                }
            }
        });
    });
</script>
@endsection