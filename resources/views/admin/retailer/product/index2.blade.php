@extends('front_layouts.admin-layout')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

@endsection
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')

    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span> 
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            @include('tools.errors')
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>Retailer Product </div>
                           <!--  -->
                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="mt-3 mb-3 msg"></div>
                                <table id="ReatilerProductTable" class="table table-hover  display nowrap RetailerProductTable" style="width:100%">
                                    <thead>
                                <tr>
                                  <th>Product</th>
                                    <th>Price</th>
                                    
                                    <th>Retailer Price</th>
                                    <th>Qty</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                                 @foreach ($RetailerProductData  as $retailerproduct)
                                <tr>
                                    <td>{{$retailerproduct->name}}</td>
                                    
                                    <td>{{$retailerproduct->price}}</td>
                                   
                                    <td align='center'><input type='rprice' value='{{$retailerproduct->rprice}}' class="form-control col-md-3" id='rprice_{{$retailerproduct->id}}'></td>
                                    <td align='center'><input type='qty' class="form-control col-md-3" value='{{$retailerproduct->qty}}' id='qty_{{$retailerproduct->id}}'></td>
                                    <td align='center'><input type='button' value='Update' data-product-price='{{$retailerproduct->price}}' class='update btn btn-success btn-sm btn-rounded' data-product-id='{{$retailerproduct->id}}'  data-id='{{$retailerproduct->rid}}' ></td>
                                </tr>
                                @endforeach
                                
                              </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('.RetailerProductTable').DataTable({
            scrollY: '60vh',
            "scrollX": true
        });
    });

    $('.changeRetailerProductStatus').click(function() {
        var id = $(this).attr("data-id");
        var status = $(this).attr("data-status");
        $.ajax({
            type: "GET",
            url: "{{url('change-retailerproduct-status')}}?id=" + id + "&status=" + status,
            success: function(res) {
                console.log(res);
                if (res == "success") {
                    $('.msg').addClass("alert alert-success");
                    $('.msg').text("Status changed successfully.");
                    $('.msg').delay(800).fadeOut('slow');
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else {
                    $('.msg').addClass("alert alert-danger");
                    $('.msg').text("Failed to change status.");
                    $('.msg').delay(500).fadeOut('slow');
                }
            }
        });
    });

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(document).ready(function(){

  // Fetch records
 // fetchRecords();


  

});

// Update record
$(document).on("click", ".update" , function() {
  var edit_id = $(this).data('id');
  var product_id = $(this).data('product-id');
  var product_price = $(this).data('product-price');

  var rprice = $('#rprice_'+product_id).val();
  var qty = $('#qty_'+product_id).val();
  /*console.log(product_id);
  console.log(rprice);
  console.log(qty);
  console.log(CSRF_TOKEN)*/;

  if(product_price >= rprice)
  {

        if(rprice != '' && qty != ''){
        $.ajax({
          url: 'retailerproduct-update',
          type: 'post',
          data: {_token: CSRF_TOKEN,editid: edit_id,rprice: rprice,qty: qty,productid:product_id},
          success: function(response){
            alert(response);
            //fetchRecords();
          }
        });
      }else{
        alert('Fill all fields');
      }

  }else{
       alert('Your Price is greater than Product Price ');
  }

  
});




</script>
@endsection