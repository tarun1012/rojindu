@extends('front_layouts.admin-layout')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')

    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span> 
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            @include('tools.errors')

            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>Retailer Product </div>
                           <!--  -->

                           
                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive" style="overflow-x: initial;">
                                <div class="mt-3 mb-3 msg"></div>
                                <form action="{{ route('retailerproduct_update') }}" id="form1" method="POST">
                                   @csrf
                                <table id="ReatilerProductTable" class="table table-hover  display nowrap RetailerProductTable" style="width:100%; ">
                                    <thead>
                                <tr>
                                  <th>Product</th>
                                  <th>Category</th>
                                  <th>Sub Category</th>
                                  <th>Brand</th>
                                  <th>Image</th>
                                  <th>Price</th>
                                  <th style="text-align: center;">Retailer Price</th>
                                  <th style="text-align: center;">Qty</th>
                                  
                                </tr>
                              </thead>
                              <tbody>
                                 @foreach ($RetailerProductData  as $retailerproduct)
                                <tr>
                                    <td>{{$retailerproduct->name}} <input type='hidden' name="addmore[{{$retailerproduct->id}}][productid]" value='{{$retailerproduct->id}}'  id='productid_{{$retailerproduct->id}}'>
                                      <input type='hidden' name="addmore[{{$retailerproduct->id}}][name]" value='{{$retailerproduct->name}}'  id='productname_{{$retailerproduct->id}}'>
                                    </td>

                                      <td>{{$retailerproduct->category_name}}</td>
                                      <td>{{$retailerproduct->subcategory_name}}</td>
                                      <td>{{$retailerproduct->brand_name}}</td>

                                      <td> <img src="{{asset('uploads/product/'.$retailerproduct->image)}}" style="width: 100px !important;" alt="{{$retailerproduct->image}}"></td>
                                    
                                    <td>{{$retailerproduct->price}} 
                                      <input type='hidden' name="addmore[{{$retailerproduct->id}}][price]" value='{{$retailerproduct->price}}'  id='productprice_{{$retailerproduct->id}}'>

                                    </td>
                                    
                                   
                                    <td align='center'><input type='text' name="addmore[{{$retailerproduct->id}}][rprice]" value='{{$retailerproduct->rprice}}' data-product-price="{{$retailerproduct->price}}" class="form-control col-md-6  rprice" id='rprice_{{$retailerproduct->id}}' required="" ><span class="text-danger .span">*{!! $errors->first('addmore.'.$retailerproduct->id.'.rprice', '<div class="error-block">:message</div>') !!}</span>
                                      

                                      
                                    </td>

                                    <td align='center'><input type='text' name="addmore[{{$retailerproduct->id}}][qty]" class="form-control col-md-6 " value='{{$retailerproduct->qty}}' id='qty_{{$retailerproduct->id}}' required=""><span class="text-danger ">*</span>
                                    </td>
                                    
                                </tr>
                                @endforeach
                                
                              </tbody>
                                </table>

                                <div class="row">
                                  <div class="col-md-12 col-lg-12">
                                      <div class="card">
                                          <div class="card-header d-flex justify-content-between align-items-center">
                                             <div align="center" class="form-group col-md-12 text-center">
                                              <button type="submit" id="btnSubmit " class="btn btn-yatra btnSubmit">Save</button>
                                            </div>
                                          </div>

                                      </div>

                                  </div>
                              </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>

  $(document).ready(function() {
        $('.RetailerProductTable').DataTable({
            scrollY: '60vh',
            "scrollX": true
        });
    });

  jQuery.validator.addMethod("validUrl", function(value, element) {
        return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
    }, "Invalid URL.");
    
$('#form1').validate({


        
        rules: {
            'addmore[][rprice]': "required",
            
            
        }

    });

$('.rprice').keyup(function(){

  var product_price = $(this).data('product-price');
  console.log(product_price);
  if ($(this).val() > product_price){
    //alert("Retailer price  must be not greater than Product Price");
     $(this).parent().find('span').text('Retailer price  must be not greater than product price');
    //$(this).val(product_price);
    $(".btnSubmit").attr("disabled", true);
  }else{
     $(this).parent().find('span').text('');
    $(".btnSubmit").attr("disabled", false);
  }
});






</script>
@endsection