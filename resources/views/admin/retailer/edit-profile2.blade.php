@extends('front_layouts.admin-layout')
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="form-group mb-0">
                                <div class="contact-textarea text-right">
                                    <a href="{{ route('rmyprofile.view')}}" class="btn btn-secondary btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('tools.errors')
                <div class="col-lg-12  d-flex align-self-stretch">
                    <form method="POST" action="{{ route('rprofile.update.request', Auth::user()->id) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-12 d-flex align-items-stretch">
                                <div class="card w-100">
                                    <div class="card-header">User Details</div>
                                    <div class="card-body row">

                                        <div class="form-group col-md-6">
                                            <label>First Name</label>
                                            <input type="text" name="firstname" id="firstname" class="form-control" placeholder="Enter First Name"  value="{{ $user->firstname ?? '' }}" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>Last Name</label>
                                            <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Enter Last Name"  value="{{ $user->lastname ?? '' }}" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label> Name</label>
                                            <input type="text" name="name" id="name" class="form-control" placeholder="Enter  Name"  value="{{ $user->name ?? '' }}" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>Mobile</label>
                                            <input type="text" name="contactno" id="contactno" class="form-control" placeholder="Enter Mobile Number"  value="{{ $user->contactno ?? '' }}" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="contactno">Whatsapp Number</label>
                                            <input class="form-control"  id="whatsapp" type="text" maxlength="12" minlength="10" name="whatsapp" placeholder="Enter Whatsapp Number" value="{{ $user->whatsappno ?? ''}}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>Email</label>
                                            <input type="email" name="email" readonly id="email" class="form-control" placeholder="Enter Email ID" value="{{ $user->email ?? ''}}" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        
                                        <div class="form-group col-md-6">
                                        	<label>Gender</label>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                	
                                                    <input type="radio" class="form-check-input" {{(!empty($user) ? $user->gender == "M" ? "checked" : "" : "")}} name="gender" value="M">Male
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" {{(!empty($user) ? $user->gender == "F" ? "checked" : "" : "")}} name="gender" value="F">Female
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="Pincode"> DOB </label>
                                            <input class="form-control" id="dob" type="date" name="dob" placeholder="Select DOB" value="{{ $user->dob ?? ''}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 d-flex align-items-stretch">
                                <div class="card w-100">
                                    <div class="card-header">Address Details</div>
                                    <div class="card-body row">
                                        
                                        <div class="form-group col-md-4">
                                            <label>Country</label>
                                            <select class="form-control" name="country" id="country" >
                                                @foreach ($countries as $country)
                                                <option value="{{$country->id}}" {{ isset($user) ? (($user->country == $country->id) ? 'selected' : '') : '' }}> {{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label>State</label>
                                            <select class="form-control" name="state" id="state" >
                                                @foreach ($states as $state)
                                                <option value="{{ $state->id }}" {{ isset($user) ? (($user->city == $state->id) ? 'selected' : '') : '' }}>{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label>City</label>
                                            <select class="form-control" name="city" id="city" >
                                                @foreach ($cities as $city)
                                                <option value="{{ $city->id }}" {{ isset($user) ? (($user->city == $city->id) ? 'selected' : '') : '' }}>{{ $city->name }}</option>
                                                @endforeach

                                            </select>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="locality">Locality</label>
                                            <input class="form-control"  id="locality" type="text" name="locality" placeholder="Enter Locality" value="{{ $user->locality ?? ''}}">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="locality">Shop Name</label>
                                            <input class="form-control"  id="shop_name" type="text" name="shop_name" placeholder="Enter Shop Name" value="{{ $user->shop_name ?? ''}}">
                                        </div>

                                        <div class="form-group col-12">
                                            <label>Address</label>
                                            <textarea name="shop_address"  id="shop_address" class="form-control" required>{!! $user->shop_address ?? '' !!}</textarea>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        

                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12 text-center">
                                <div class="card ">
                                    <div class="card-header d-flex justify-content-between align-items-center">
                                        <label class="upload  col-form-label">
                                            <span>Upload Profile Pic</span>
                                            <input type="file" name="profilepic" value="Upload Profile Pic">
                                            <!-- <img src="{!! asset('profiles/'.$user->profilepic) !!}" onError="this.onerror=null;this.src='{{asset("'front/images/user.png'")}}';" style="height:100px; width:100px"> -->
                                        </label>
                                        <div class="form-group mb-0">
                                            <div class="contact-textarea text-center">
                                                <button class="btn btn-shukul btn-sm" type="submit" value="Submit Form">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>



                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>



@endsection

@section('js')
<script>
    $(document).ready(function() {
        $.validator.addMethod('Validemail', function(value, element) {
            return this.optional(element) || value.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/);
        }, "Please enter a valid email address.");
        $('#userForm').validate({
            rules: {

                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                },

                email: {
                    required: true,
                    Validemail: true,
                },

                contactno: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 12,
                },
                profilepic: {
                    required: true,
                    extension: "jpeg|jpg|gif|png"
                },
                ageGroup: {
                    required: true
                },
                address: {
                    required: true
                },
                country: {
                    required: true
                },
                state: {
                    required: true
                },
                city: {
                    required: true
                },
                status: {
                    required: true
                },
            }
        });

        $('#country').change(function() {
            var countryID = $(this).val();
            if (countryID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-state-list1')}}?country_id=" + countryID,
                    success: function(res) {
                        if (res) {
                            $("#state").empty();
                            $("#state").append('<option value="">Choose State</option>');
                            $.each(res, function(key, value) {
                                $("#state").append('<option value="' + key + '">' + value + '</option>');
                            });
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        } else {
                            $("#state").empty();
                            $("#state").append('<option value="">Choose State</option>');
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        }
                    }
                });
            } else {
                $("#state").empty();
                $("#state").append('<option value="">Choose State</option>');
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
            }
        });

        $('#state').on('change', function() {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-city-list1')}}?c_id=" + stateID,
                    success: function(res) {
                    	//console.log(res);
                        if (res) {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                            $.each(res, function(key, value) {
                                $("#city").append('<option value="' + key + '">' + value + '</option>');
                            });
                        } else {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        }
                    }
                });
            } else {
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
            }

        });

    });

    function showStates(countryId) {
        console.log("country ID :: ", countryId);

        if (countryId) {
            $.ajax({
                type: "GET",
                url: "{{url('get-state-list1')}}?country_id=" + countryId,
                success: function(res) {
                    if (res) {
                        $("#state").empty();
                        $("#state").append('<option>Choose State</option>');
                        $.each(res, function(key, value) {
                            $("#state").append('<option value="' + key + '">' + value + '</option>');
                        });
                    } else {
                        $("#state").empty();
                        $("#city").empty();
                    }
                }
            });
        }

    }

    function showCities(stateId) {
        console.log('State Id :: ', stateId);

        if (stateId) {
            $.ajax({
                type: "GET",
                url: "{{url('get-city-list1')}}?state_id=" + stateId,
                success: function(res) {
                    if (res) {
                        $("#city").empty();
                        $("#city").append('<option>Choose State</option>');
                        $.each(res, function(key, value) {
                            $("#city").append('<option value="' + key + '">' + value + '</option>');
                        });
                    } else {
                        $("#city").empty();
                    }
                }
            });
        } else {
            $("#city").empty();
        }
    }
</script>
@endsection