@extends('front_layouts.admin-layout')

@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{!empty($user) ? "Update" : "Create"}} Retailer</h5>
                        </div>
                    </div>
                    @if(isset($user))
                    <form id="userForm" class="form theme-form" method="POST" action="{{ route('retailer.request.update', $user->id) }}" enctype="multipart/form-data">
                        @else
                        <form id="userForm" class="user-form form theme-form" method="POST" action="{{ route('retailer.request') }}" enctype="multipart/form-data">
                            @endif
                            @csrf

                            <div class="form-row">
                                <div class="col-lg-12">
                                    

                                    <div class="card">
                                        <div class="card-body row">

                                            <div class="form-group col-md-6">
                                                <label for="name">First Name</label>
                                                <input class="form-control" {{!empty($user) ? "readonly" : ""}} id="firstname" type="text" name="firstname" placeholder="Enter First Name" value="{{ $user->firstname ?? ''}}">
                                                @error('firstname')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="name">Last Name</label>
                                                <input class="form-control" {{!empty($user) ? "readonly" : ""}} id="lastname" type="text" name="lastname" placeholder="Enter Last Name" value="{{ $user->lastname ?? ''}}">
                                                @error('lastname')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="email"> Email </label>
                                                <input class="form-control" id="email" type="email" name="email" {{!empty($user) ? "readonly" : ""}} placeholder="Enter Email Address" value="{{ $user->email ?? ''}}">
                                                @error('email')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6" style="display : {{!empty($user) ? "none" : ""}} ">
                                                <label for="password" >Password</label>
                                                <input class="form-control" id="password" type="{{!empty($user) ? "text" : "password"}}" {{!empty($user) ? "readonly" : ""}} name="password" style="display : {{!empty($user) ? "none" : ""}} " placeholder="Enter Password" value="">
                                                @error('password')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            

                                            <div class="form-group col-md-6">
                                                <label for="contactno">Contact Number</label>
                                                <input class="form-control" id="contactno" type="text" maxlength="12" minlength="10" name="contactno" placeholder="Enter Contact Number" value="{{ $user->contactno ?? ''}}">
                                                @error('contactno')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-6">
                                                <div class="form-check">
                                                    <input type="checkbox" {{(!empty($user->whatsappno) ? ($user->whatsappno == $user->contactno ? "checked" : "") : "checked")}} value="1" class="form-check-input" name="IsWhatsapp" id="IsWhatsapp">
                                                    <label class="form-check-label" for="exampleCheck1">Is Whatsapp number same as contact number?</label>
                                                </div>
                                                <div class="whatsapp-block" style="display:{{(!empty($user->whatsappno) ? ($user->whatsappno == $user->contactno ? "none" : "") : "none")}};">
                                                    <input class="form-control" id="whatsapp" type="text" maxlength="12" minlength="10" name="whatsapp" placeholder="Enter Whatsapp Number" value="{{ $user->whatsappno ?? ''}}">
                                                    @error('whatsapp')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            

                                            <div class="form-group col-md-6">
                                                <label for="profilepic">Profile </label>

                                                <input type="file" class="form-control" id="profilepic" type="file" name="profilepic">

                                                @error('profilepic')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                                @if(isset($user))
                                                <img src="{!! asset('profiles/'.$user->profilepic) !!}" style="width: 100px; height: 100px;">
                                                @endif

                                                <!-- <input class="form-control" id="profilepic" type="file" name="profilepic"> -->
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="Pincode"> DOB </label>
                                                <input class="form-control" id="dob" type="date" name="dob" placeholder="Select DOB" value="{{ $user->dob ?? ''}}">
                                                @error('dob')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input" {{(!empty($user) ? $user->gender == "M" ? "checked" : "" : "")}} name="gender" value="M">Male
                                                    </label>
                                                </div>
                                                <div class="form-check-inline">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input" {{(!empty($user) ? $user->gender == "F" ? "checked" : "" : "")}} name="gender" value="F">Female
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-body row">

                                            <div class="form-group col-md-12">
                                                <label for="address">Address </label>
                                                <input type="text" class="form-control" id="address" name="address" value="{!! $user->address ?? '' !!}" />
                                                @error('address')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-4">

                                                <label for="country">Country</label>
                                                <select class="form-control" id="country" name="country">
                                                    <option value="">Choose Country</option>
                                                    @foreach ($countries as $country)
                                                    <option value="{{$country->id}}" {{ isset($user) ? (($user->country == $country->id) ? 'selected' : '') : '' }}> {{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('country')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label for="state">State</label>
                                                <select class="form-control" id="state" name="state">
                                                    <option value="">Choose State</option>
                                                    @if(isset($user))
                                                    @foreach ($states as $state)
                                                    <option value="{{ $state->id }}" {{ isset($user) ? (($user->city == $state->id) ? 'selected' : '') : '' }}>{{ $state->name }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                @error('state')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="city">City</label>
                                                <select class="form-control" id="city" name="city">
                                                    <option value="">Choose City</option>
                                                    @if(isset($user))
                                                    @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}" {{ isset($user) ? (($user->city == $city->id) ? 'selected' : '') : '' }}>{{ $city->name }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                @error('city')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="locality">Locality</label>
                                                <input class="form-control" id="locality" type="text" name="locality" placeholder="Enter Locality" value="{{ $user->locality ?? ''}}">
                                                @error('locality')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="Pincode"> Shop Name </label>
                                                <input class="form-control" id="shop_name" type="text" name="shop_name" placeholder="Enter Shop Name" value="{{ $user->shop_name ?? ''}}">
                                                @error('shop_name')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="shop_address">Shop Address </label>
                                                <input type="text" class="form-control" id="shop_address" name="shop_address" value="{!! $user->shop_address ?? '' !!}" />
                                                @error('shop_address')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="row card-body">
                                            <div class="form-group col-md-6">
                                                <div class="form-check">
                                                    <input type="checkbox" value="1" class="form-check-input" {{ isset($user) ? (($user->status == 1) ? 'checked' : '') : '' }} name="status" id="status">
                                                    <label class="form-check-label" for="exampleCheck1">Active?</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-body text-right">
                                            <button class="btn btn-shukul mr-2" type="submit"> {{ isset($user) ? 'Update' : 'Submit' }} </button>
                                            <a href="{{ route('retailer') }}" class="btn btn-light">Cancel</a>
                                        </div>
                                    </div>



                                </div>

                            </div>

                        </form>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function() {
        $('#contactno').keypress(function(key) {
            if (key.charCode < 48 || key.charCode > 57) return false;
        });
        $('#whatsapp').keypress(function(key) {
            if (key.charCode < 48 || key.charCode > 57) return false;
        });
        
        $('#IsWhatsapp').change(function() {
            if (!$('#IsWhatsapp').is(':checked')) {
                $('.whatsapp-block').show();
                $("#whatsapp").prop('required', true);
            } else {
                $('.whatsapp-block').hide();
            }
        });

        $.validator.addMethod('Validemail', function(value, element) {
            return this.optional(element) || value.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/);
        }, "Please enter a valid email address.");

        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space not allowed");


        $('#userForm').validate({
            errorPlacement: function(error, element) {
                error.insertAfter(element)
                error.addClass('text-danger');
            },
            messages: {
               email: {
                    remote: 'email already taken.',
                },
            },
            rules: {

                email: {
                    required: true,
                    Validemail: true,
                    remote: {
                        type: 'post',
                        url: "{{ URL('user/existusername') }}",
                        async: false,
                        async: false,
                        data: {
                            email: function() {
                                console.log('1');
                                return "@if(!empty($user)) @else " + $("input[id='email']").val() + " @endif";
                            },
                            id: "@if(isset($user)){{$user->id}}@endif",
                            "_token": "{{ csrf_token() }}"
                        },
                        async: false
                    }

                },
                firstname: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                },
                lastname: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                },
                
                locality: "required",
                dob: "required",
                sword: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                },
                contactno: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 12,
                },
                whatsappno: {
                    digits: true,
                    minlength: 10,
                    maxlength: 12,
                },
                profilepic: {
                    extension: "jpeg|jpg|gif|png"
                },
                
                address: {
                    required: true
                },
                shop_name: {
                    required: true
                },
                shop_address: {
                    required: true
                },
                country: {
                    required: true
                },
                state: {
                    required: true
                },
                city: {
                    required: true
                },
                status: {
                    required: true
                },
            }
        });


        


        $("#parent_user").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "{{ route('search.parent.user') }}",
                    data: {
                        'parent_user': request.term,
                        '_token': "{{csrf_token()}}",
                        'id': '{{!empty($user) ? $user->id : 0}}'
                    },
                    type: "POST",
                    success: function(result) {
                        response(jQuery.parseJSON(result));
                    }
                });
            },
            select: function(event, ui) {
                $('#parent_user').val(ui.item.label); // display the selected text
                $('#parent_user_id').val(ui.item.value); // save selected id to input
                return false;
            },
            minLength: 3
        });
    });

$('#country').change(function() {
            var countryID = $(this).val();
            if (countryID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-state-list1')}}?c_id=" + countryID,
                    success: function(res) {
                        if (res) {
                            $("#state").empty();
                            $("#state").append('<option value="">Choose State</option>');
                            $.each(res, function(key, value) {
                                $("#state").append('<option value="' + key + '">' + value + '</option>');
                            });
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        } else {
                            $("#state").empty();
                            $("#state").append('<option value="">Choose State</option>');
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        }
                    }
                });
            } else {
                $("#state").empty();
                $("#state").append('<option value="">Choose State</option>');
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
            }
        });

        $('#state').on('change', function() {
            var stateID = $(this).val();
            if (stateID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-city-list1')}}?c_id=" + stateID,
                    success: function(res) {
                        //console.log(res);
                        if (res) {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                            $.each(res, function(key, value) {
                                $("#city").append('<option value="' + key + '">' + value + '</option>');
                            });
                        } else {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                        }
                    }
                });
            } else {
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
            }

        });


</script>
@endsection