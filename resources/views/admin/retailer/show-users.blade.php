@extends('front_layouts.admin-layout')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.css">
@endsection
@section('content')

<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @include('tools.errors')
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>Retailer </div>
                            <div class="">
                                
                                 @if (get_user_permission("manage_users","add"))
                                    <a href="{{ route('retailer.create') }}" class="btn btn-yatra btn-sm btn-rounded">Add Retailer</a>
                                    @endif
                                
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="rolesTable" class="table table-hover nowrap display " style="width:100%;    overflow: auto;">
                                    <thead class="text-center">
                                        <tr>
                                            
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            
                                            <th>Referral Parent Name</th>
                                            <th>Status</th>
                                            <th>Created</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($users as $user)
                                        <tr>
                                            
                                            <td>{{ $user->name }} </td>
                                            <td>{{$user->email}}</td>
                                            <td>{{ $user->role_name }}</td>
                                            
                                            <td>{{$user->parent_firstname." ".$user->parent_lastname." - ".$user->parent_referal_code}}</td>
                                           <td>@if($user->status == 1) <span class="badge badge-success">Active</span> @else   <span class="badge badge-danger">Deactive</span> @endif</td>
										     <td>{{ date('d/m/Y', strtotime($user->created_at)) }}</td>
                                            <td>
                                                @if (get_user_permission("manage_users","edit"))
                                                    <a href="{{ route('edit.retailer', $user->id) }}" class="btn btn-success btn-sm btn-rounded">Edit</a>  
                                                    @endif  
                                                

                                               
                                                    
                                                

                                                
													
                                                
												 

                                                
                                                    @if($user->status == 1) <a href="{{ route('change.user.status', $user->id) }}" class="btn btn-danger btn-sm btn-rounded"> Deactive </a>@else <a href="{{ route('change.user.status', $user->id) }}" class="btn btn-success btn-sm btn-rounded"> Active </a>@endif
                                                

                                              
                                                   
                                             

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="view_user_modal" tabindex="-1" role="dialog" 
    aria-labelledby="gridSystemModalLabel">
</div>

<div class="modal fade" id="view_user_upline_modal" tabindex="-1" role="dialog" 
    aria-labelledby="gridSystemModalLabel">
</div>

@include('front_partials.password-modal')

@endsection
@section('js')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
<script>
    $(document).ready(function() {
        $('#rolesTable').DataTable({
            scrollY: '60vh',
            "scrollX": true
        });
    });
</script>
@endsection