@extends('front_layouts.admin-layout')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.css">
@endsection

@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    @include('tools.errors')
                    <div class="card">
                        <div class="card-header p-0">
                            <ul class="nav nav-tabs profiletab" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pending-tab" data-toggle="tab" href="#pending-kyc" role="tab" aria-controls="home" aria-selected="true">KYC Pending Request</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="rejected-tab" data-toggle="tab" href="#rejected-kyc" role="tab" aria-controls="profile" aria-selected="false">KYC Rejected Request</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="pt-2">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="pending-kyc" role="tabpanel" aria-labelledby="pending-kyc">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="alert alert-success" id="success_message" style="display:none">
                                                Rejection Done
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="PendingKYC" class="display" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Request ID</th>
                                                                <th>Username</th>
                                                                <th>Field Name</th>
                                                                <th>Old Value</th>
                                                                <th>New Value</th>
                                                                <th>Request Date</th>
                                                                <th>Status</th>
                                                                <th>Admin Comment</th>
                                                                <th>Verified By</th>
                                                                <th>Verified Date</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(count($PendingRequest)> 0)
                                                            @foreach ($PendingRequest as $infoDoc)
                                                            <tr>
                                                                <td> REQ_{{ $infoDoc->id }} </td>
                                                                <td> {{ $infoDoc->getUsername->email}} </td>
                                                                <td> {{ $infoDoc->fieldname }} </td>
                                                                <td>
                                                                    @if($infoDoc->type == 'file')
                                                                    @if($infoDoc->oldvalue)
                                                                    <div class="row">
                                                                        @foreach (json_decode($infoDoc->oldvalue) as $olddocFile)
                                                                        <div class="col-md-6">
                                                                            <a href="{!! asset('documents').'/'.$olddocFile !!}" class="icon lightbox-image"><img src="{!! asset('documents').'/'.$olddocFile !!}" style="width:50px; height:50px;"></a>
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                    @else
                                                                    N/A
                                                                    @endif
                                                                    @else
                                                                    {{ ($infoDoc->oldvalue) ? $infoDoc->oldvalue : 'N/A' }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if($infoDoc->type == 'file')
                                                                    @if($infoDoc->newvalue)
                                                                    <div class="row">
                                                                        @foreach (json_decode($infoDoc->newvalue) as $newFile)
                                                                        <div class="col-md-6">
                                                                            <a href="{!! asset('documents').'/'.$newFile !!}" class="icon lightbox-image"><img src="{!! asset('documents').'/'.$newFile !!}" style="width:50px; height:50px;"></a>
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                    @else
                                                                    N/A
                                                                    @endif
                                                                    @else
                                                                    {{ ($infoDoc->newvalue) ? $infoDoc->newvalue : 'N/A' }}
                                                                    @endif
                                                                </td>
                                                                <td> {{ $infoDoc->created_at }} </td>
                                                                <td> {{ $infoDoc->status }} </td>
                                                                <td> {{ $infoDoc->admincomment }} </td>

                                                                <td>{{$infoDoc->verifiedby}}</td>
                                                                <td>{{ ($infoDoc->updated_at) ? $infoDoc->updated_at : "N/A" }}</td>
                                                                <td>
                                                                    @if($infoDoc->status != "approved")
                                                                        <a class="btn btn-success btn-approve-kyc" onclick="approveKYC('{{$infoDoc->id}}');" class="btn btn-success"><i class="fa fa-check-circle"></i></a>
                                                                        <button type="button" class="btn btn-danger" onclick="openRejection({{$infoDoc->id}})"><i class="fa fa-ban"></i></button>
                                                                    @endif

                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show" id="rejected-kyc" role="tabpanel" aria-labelledby="rejected-kyc">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="alert alert-success" id="success_message" style="display:none">
                                                Rejection Done
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="RejectedKYC" class="display" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Request ID</th>
                                                                <th>Username</th>
                                                                <th>Field Name</th>
                                                                <th>Old Value</th>
                                                                <th>New Value</th>
                                                                <th>Request Date</th>
                                                                <th>Status</th>
                                                                <th>Admin Comment</th>
                                                                <th>Verified By</th>
                                                                <th>Verified Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(count($RejectedRequest)> 0)
                                                            @foreach ($RejectedRequest as $infoDoc)
                                                            <tr>
                                                                <td> REQ_{{ $infoDoc->id }} </td>
                                                                <td> {{ $infoDoc->getUsername->username}} </td>
                                                                <td> {{ $infoDoc->fieldname }} </td>
                                                                <td>
                                                                    @if($infoDoc->type == 'file')
                                                                    @if($infoDoc->oldvalue)
                                                                    <div class="row">
                                                                        @foreach (json_decode($infoDoc->oldvalue) as $olddocFile)
                                                                        <div class="col-md-6">
                                                                            <a href="{!! asset('documents').'/'.$olddocFile !!}" class="icon lightbox-image"><img src="{!! asset('documents').'/'.$olddocFile !!}" style="width:50px; height:50px;"></a>
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                    @else
                                                                    N/A
                                                                    @endif
                                                                    @else
                                                                    {{ ($infoDoc->oldvalue) ? $infoDoc->oldvalue : 'N/A' }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if($infoDoc->type == 'file')
                                                                    @if($infoDoc->newvalue)
                                                                    <div class="row">
                                                                        @foreach (json_decode($infoDoc->newvalue) as $newFile)
                                                                        <div class="col-md-6">
                                                                            <a href="{!! asset('documents').'/'.$newFile !!}" class="icon lightbox-image"><img src="{!! asset('documents').'/'.$newFile !!}" style="width:50px; height:50px;"></a>
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                    @else
                                                                    N/A
                                                                    @endif
                                                                    @else
                                                                    {{ ($infoDoc->newvalue) ? $infoDoc->newvalue : 'N/A' }}
                                                                    @endif
                                                                </td>
                                                                <td> {{ $infoDoc->created_at }} </td>
                                                                <td> {{ $infoDoc->status }} </td>
                                                                <td> {{ $infoDoc->admincomment }} </td>

                                                                <td>{{$infoDoc->verifiedby}}</td>
                                                                <td>{{ ($infoDoc->updated_at) ? $infoDoc->updated_at : "N/A" }}</td>
                                                            </tr>
                                                            @endforeach
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="rejectionFormModel" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Admin Rejection Comment</h4>
            </div>
            <form method="POST" action="{{ route('documentvarification.comment.request') }}" name="rejectionForm" id="rejectionForm">
                @csrf
                <div class="modal-body">
                    <div class="alert alert-danger" id="error_message" style="display:none">
                        for Rejection Comment is Required
                    </div>

                    <div class="form-group">
                        <label for="comment">Comment:</label>
                        <textarea class="form-control" id="comment" name="comment" required></textarea>
                    </div>
                    <input type="hidden" name="request_id" id="request_id" required>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="form-group btn btn-success" name="rejectionFormBtn" id="rejectionFormBtn">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
<script>
    $(document).ready(function() {
        $('#PendingKYC').DataTable();
        $('#RejectedKYC').DataTable();
    });

    function approveKYC(docid) {
        Swal.fire({
        title: 'Are you sure?',
        text: "You want to approve this kyc request.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, approve it!'
        }).then((isConfirm) => {
            if(isConfirm.value == true) {
                location.href="confirmation/"+docid+"/request";
            }
        });
    }

    function openRejection(requestId) {
        Swal.fire({
        title: 'Are you sure?',
        text: "You want to reject this kyc request.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, reject it!'
        }).then((isConfirm) => {
            if(isConfirm.value == true) {
                $("#rejectionForm").trigger('reset');
                $("#request_id").val(requestId);
                $('#rejectionFormModel').modal('show');
            }
        });
    }

    $("#rejectionForm").submit(function(event) {
        event.preventDefault();

        var post_url = $(this).attr("action");
        var request_method = $(this).attr("method");
        var form_data = $(this).serialize();

        console.log("form_data ", form_data, request_method, post_url);

        $.ajax({
            url: post_url,
            type: request_method,
            data: form_data
        }).done(function(response) {
            $("#server-results").html(response);
            console.log('Response :: ', response);
            if (response.code == 200 && response.status == 'success') {
                console.log("success");
                $('#success_message').show();
                setTimeout(function() {
                    $('#success_message').fadeOut("slow");
                }, 5000);
                $('#rejectionFormModel').modal('hide');
            } else {
                console.log("failed");
                console.log("Error Occured :: ", response.error);

                $('#error_message').show();
                setTimeout(function() {
                    $('#error_message').fadeOut("slow");
                }, 5000);

            }
        });
        location.reload(true);
    });
</script>
@endsection