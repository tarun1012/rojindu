@extends('front_layouts.admin-layout')
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            @include('tools.errors')
            <form method="POST" action="{{ route('bankdetail.request') }}" id="KYCForm" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="form-group mb-0">
                                    <div class="contact-textarea text-right">
                                        <a href="{{ route('myprofile.view.kyc_info')}}" class="btn btn-secondary btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 d-flex align-self-stretch">
                        <div class="card w-100">
                            <div class="card-header">Bank Info</div>
                            <div class="card-body">

                                <div class="form-group ">
                                    <label>Bank Name</label>
                                    <input type="text" value="{{$Docs->bank_name ?? ''}}" id="bank_name" name="bank_name" class="form-control">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label>Account Name</label>
                                    <input type="text" id="account_name" name="account_name" value="{{$Docs->account_name ?? ''}}" class="form-control">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label>Account Number</label>
                                    <input type="text" id="account_number" value="{{$Docs->account_number ?? ''}}" name="account_number" class="form-control">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label>IFSC</label>
                                    <input type="text" id="ifsc_code" value="{{$Docs->ifsc_code ?? ''}}" name="ifsc_code" class="form-control">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label>Bank Proof Type</label>
                                    <select class="form-control" name="bank_proof_type" id="bank_proof_type">
                                        <option value="cancel check" {{!empty($Docs->bank_proof_type) ? ($Docs->bank_proof_type == "cancel check" ? "selected" : "") : ""}}>Cancel Check</option>
                                        <option value="bank passbook" {{!empty($Docs->bank_proof_type) ? ($Docs->bank_proof_type == "bank passbook" ? "selected" : "") : ""}}>Bank Passbook</option>
                                    </select>
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <label class="upload">
                                    <span>Upload Doc File (maximum 3 files can upload)</span>
                                    <input type="file" id="bank_document" name="bank_document[]" multiple>
                                </label>
                                <div class="form-group">
                                    <span class="text-danger bank_msg" style="display:none;">You can upload a maximum of 3 files.</span>
                                </div>
                                <div class="form-group d-flex">
                                    @if (!empty($BankDoc))
                                        @foreach(json_decode($BankDoc->image) as $key => $file)
                                            <img src="{!! asset('documents').'/'.$file !!}" class="img-fluid my-2 mr-2" style="width:100px;height:100px;" id="myImg">
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6 d-flex align-self-stretch">
                        <div class="card w-100">
                            <div class="card-header">Aadhar Info</div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Aadhar Name</label>
                                    <input type="text" id="aadhar_name" value="{{$Docs->aadhar_name ?? ''}}" name="aadhar_name" class="form-control">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Aadhar Number</label>
                                    <input type="text" id="aadhar_number" value="{{$Docs->aadhar_number ?? ''}}" name="aadhar_number" class="form-control">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                                <label class="upload  ">
                                    <span>Upload Doc File (maximum 2 files can upload)</span>
                                    <input type="file" id="aadhar_document" name="aadhar_document[]" multiple>
                                </label>
                                <div class="form-group">
                                    <span class="text-danger aadhar_msg" style="display:none;">You can upload a maximum of 2 files.</span>
                                </div>
                                <div class="form-group d-flex">
                                    @if (!empty($AadharDoc))
                                        @foreach(json_decode($AadharDoc->image) as $key => $file)
                                            <img src="{!! asset('documents').'/'.$file !!}" class="img-fluid my-2 mr-2" style="width:100px;height:100px;" id="myImg">
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 d-flex align-self-stretch">
                        <div class="card w-100">
                            <div class="card-header">Pancard Info</div>
                            <div class="card-body">

                                <div class="form-group">
                                    <label>Pancard Name</label>
                                    <input type="text" id="pan_name" value="{{$Docs->pan_name ?? ''}}" name="pan_name" class="form-control">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Pancard Number</label>
                                    <input type="text" id="pan_number" value="{{$Docs->pan_number ?? ''}}" name="pan_number" class="form-control">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>

                                <label class="upload  ">
                                    <span>Upload Doc File (maximum 2 files can upload)</span>
                                    <input type="file" id="pan_document" name="pan_document[]" multiple>
                                </label>
                                <div class="form-group">
                                    <span class="text-danger pan_msg" style="display:none;">You can upload a maximum of 2 files.</span>
                                </div>
                                <div class="form-group d-flex">
                                    @if (!empty($PanDoc))
                                        @foreach(json_decode($PanDoc->image) as $key => $file)
                                            <img src="{!! asset('documents').'/'.$file !!}" class="img-fluid my-2 mr-2" style="width:100px;height:100px;" id="myImg">
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6 d-flex align-self-stretch">
                        <div class="card w-100">

                            <div class="card-body card-body text-center d-flex align-items-center justify-content-center">
                                <div>
                                    <img src="{{asset('front/images/invalid.png')}}" class="img-fluid">
                                    <h5 class="mt-4">All KYC is required</h5>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12 text-right">
                        <button type="submit" class="btn btn-shukul mb-2">Save all</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection

@section('js')
    <script>
        jQuery.validator.addMethod("pan", function(value, element)
        {
            return this.optional(element) || /^[A-Z]{5}\d{4}[A-Z]{1}$/.test(value);
        }, "Please enter a valid PAN");

        jQuery.validator.addMethod("aadhar", function(value, element)
        {
            return this.optional(element) || /^\d{12}$/.test(value);
        }, "Please enter a valid aadhar number");

        $('#KYCForm').validate({
            rules:{
                account_number:{
                    digits:true,
                    maxlength:18,
                    minlength:9
                },
                aadhar_number:{
                    aadhar:true
                },
                pan_number:{
                    pan:true
                },
                ifsc_code:{
                    maxlength:11
                }
            },
            submitHandler: function(form) {
                var $bank_doc = $("input[name='bank_document[]']");
                var $aadhar_doc = $("input[name='aadhar_document[]']");
                var $pan_doc = $("input[name='pan_document[]']");
                var flag = 0;
                var bank_length = parseInt($bank_doc.get(0).files.length);
                var aadhar_length = parseInt($aadhar_doc.get(0).files.length);
                var pan_length = parseInt($pan_doc.get(0).files.length);

                if (bank_length > 3 ) {
                    $('.bank_msg').show();
                    flag = 1;
                } 
                
                if (aadhar_length > 2) {
                    $('.aadhar_msg').show();
                    flag = 1;
                } 
                
                if (pan_length > 2) {
                    $('.pan_msg').show();
                    flag = 1;
                }  
                
                if(flag == 0) {
                    $('.bank_msg').hide();
                    $('.aadhar_msg').hide();
                    $('.pan_msg').hide();
                    $("#KYCForm").submit();
                }
            }
        })
    </script>
@endsection