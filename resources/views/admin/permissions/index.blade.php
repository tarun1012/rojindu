@extends('front_layouts.admin-layout')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')

<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            @include('tools.errors')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">

                                        <select name="role" id="role" class="form-control">
                                             @if (!empty(get_roles()))
                                            @foreach (get_roles() as $role)
                                            <option value="{{$role->id}}" {{!empty($current_role) == $role->id ? "selected=selected" : ""}}>{{$role->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <div class="mt-3 mb-3 msg"></div>
                                @if (!empty($Permissions))
                                <table id="PermissionsTable" class="table table-hover display nowrap table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Role</th>
                                            <th>Module</th>
                                            <th>Add</th>
                                            <th>Update</th>
                                            <th>Delete</th>
                                            <th>View</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (!empty($Permissions))
                                        @foreach ($Permissions as $perm)
                                        <tr>
                                            <td>{{$perm->roleName}}</td>
                                            <td>{{$perm->moduleName}}</td>
                                            <td>
                                                <input type="checkbox" name="AddPermission[]" onchange="changePermission('{{$perm->roleId}}','{{$perm->moduleId}}','{{$perm->add == 1 ? 0 : 1}}','add');" {{$perm->add == 1 ? "checked" : ""}} value="{{$perm->add}}" />
                                            </td>
                                            <td>
                                                <input type="checkbox" name="UpdatePermission[]" onchange="changePermission('{{$perm->roleId}}','{{$perm->moduleId}}','{{$perm->edit == 1 ? 0 : 1}}','edit');" {{$perm->edit == 1 ? "checked" : ""}} value="{{$perm->edit}}" />
                                            </td>
                                            <td>
                                                <input type="checkbox" name="DeletePermission[]" onchange="changePermission('{{$perm->roleId}}','{{$perm->moduleId}}','{{$perm->delete == 1 ? 0 : 1}}','delete');" {{$perm->delete == 1 ? "checked" : ""}} value="{{$perm->delete}}" />
                                            </td>
                                            <td>
                                                <input type="checkbox" name="ViewPermission[]" onchange="changePermission('{{$perm->roleId}}','{{$perm->moduleId}}','{{$perm->view == 1 ? 0 : 1}}','view');" {{$perm->view == 1 ? "checked" : ""}} value="{{$perm->view}}" />
                                            </td>
                                            <td>Action</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                @elseif(!empty($RolePermissions))
                                <table id="PermissionsTable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Role</th>
                                            <th>Module</th>
                                            <th>Add</th>
                                            <th>Update</th>
                                            <th>Delete</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (!empty($RolePermissions))
                                        @foreach ($RolePermissions as $perm)
                                        <tr>
                                            <td>{{$perm->roleName}}</td>
                                            <td>{{$perm->moduleName}}</td>
                                            <td>
                                                <input type="checkbox" name="AddPermission[]" onchange="changePermission('{{$perm->roleId}}','{{$perm->moduleId}}','{{$perm->add == 1 ? 0 : 1}}','add');" {{$perm->add == 1 ? "checked" : ""}} value="{{$perm->add}}" />
                                            </td>
                                            <td>
                                                <input type="checkbox" name="UpdatePermission[]" onchange="changePermission('{{$perm->roleId}}','{{$perm->moduleId}}','{{$perm->edit == 1 ? 0 : 1}}','edit');" {{$perm->edit == 1 ? "checked" : ""}} value="{{$perm->edit}}" />
                                            </td>
                                            <td>
                                                <input type="checkbox" name="DeletePermission[]" onchange="changePermission('{{$perm->roleId}}','{{$perm->moduleId}}','{{$perm->delete == 1 ? 0 : 1}}','delete');" {{$perm->delete == 1 ? "checked" : ""}} value="{{$perm->delete}}" />
                                            </td>
                                            <td>
                                                <input type="checkbox" name="ViewPermission[]" onchange="changePermission('{{$perm->roleId}}','{{$perm->moduleId}}','{{$perm->view == 1 ? 0 : 1}}','view');" {{$perm->view == 1 ? "checked" : ""}} value="{{$perm->view}}" />
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#PermissionsTable').DataTable({
            scrollY: '60vh',
            "scrollX": true
        });
    });

    $('#role').change(function() {
        var roleId = $(this).val();
        if (roleId != "") {
            location.href = "{{route('permission.fetch')}}?roleId=" + roleId;
        }
    });

    function changePermission(roleId, moduleId, value, type) {
        if (roleId != "" && moduleId != "" && value != "" && type != "") {
            $.ajax({
                type: "GET",
                url: "{{route('permission.add')}}?roleId=" + roleId + "&moduleId=" + moduleId + "&value=" + value + "&type=" + type,
                success: function(res) {
                    if (res == "success") {
                        location.reload();
                    }
                    console.log(res);
                }
            });
        }
    }
</script>
@endsection