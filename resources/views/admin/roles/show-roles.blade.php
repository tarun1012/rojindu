@extends('front_layouts.admin-layout')


@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div> Roles </div>
                            <a href="{{ route('create.roles') }}" class="btn btn-yatra btn-sm btn-rounded">Add Role</a>
                        </div>

                    </div>

                    <div class="card">


                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="rolesTable" class="display table table-hover nowrap " style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Created</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($roles as $role)
                                        <tr>
                                            <td>{{ $role->name }}</td>
                                            <td>@if($role->status == 1) Active @else Deactive @endif</td>
                                            <td>{{ $role->created_at }}</td>
                                            <td>
                                                <a href="{{ route('edit.roles', $role->id) }}" class="btn btn-success btn-sm btn-rounded ">Edit</a>
                                                @if($role->status == 1) <a href="{{ route('change.roles.status', $role->id) }}" class="btn btn-sm btn-rounded btn-danger"> Deactive </a>@else <a href="{{ route('change.roles.status', $role->id) }}" class="btn btn-sm btn-rounded btn-success"> Active </a>@endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#rolesTable').DataTable({
            scrollY: '60vh',
            "scrollX": true
        });
    });
</script>
@endsection