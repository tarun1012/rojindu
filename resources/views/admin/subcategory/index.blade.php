@extends('front_layouts.admin-layout')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar') 
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            @include('tools.errors')
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>SubCategory </div>
                            @if (get_user_permission("manage_subcategory","add"))
                            <a href="{{route('subcategory.create')}}" class="btn btn-sm btn-rounded btn-yatra">Add SubCategory</a>
                           @endif
                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="mt-3 mb-3 msg"></div>
                                <table id="SubCategoryTable" class="table table-hover  display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                           <th>Name</th>
                                            
                                            <th>Category</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($SubCategoryData as $subcategory)
                                        <tr>
                                            
                                            <td>{{$subcategory->name}}</td>
                                            <td>{{$subcategory->categoryname}}</td>
                                           
                                            
                                            <td>
                                                @if ($subcategory->status == 1)
                                                <span class="badge badge-success">Active</span>
                                                @else
                                                <span class="badge badge-danger">Paused</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($subcategory->status == 1)
                                                <button type="button" class="btn btn-rounded btn-sm btn-warning changeSubCategoryStatus" data-subcategory_id="{{$subcategory->id}}" data-status="{{$subcategory->status}}" data-toggle="tooltip" title="Pause subcategory"><i class="fa fa-power-off"></i></button>
                                                @else
                                                <button type="button" class="btn btn-rounded btn-sm btn-success changeSubCategoryStatus" data-subcategory_id="{{$subcategory->id}}" data-status="{{$subcategory->status}}" data-toggle="tooltip" title="Activate subcategory"><i class="fa fa-refresh"></i></button>
                                                @endif
                                                 @if (get_user_permission("manage_subcategory","edit"))
                                                <a href="{{ route('subcategory.edit', $subcategory->id) }}" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit subcategory"><i class="fa fa-edit"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#SubCategoryTable').DataTable({
            scrollY: '60vh',
            "scrollX": true
        });
    });

    $('.changeSubCategoryStatus').click(function() {
        var id = $(this).attr("data-subcategory_id");
        var status = $(this).attr("data-status");
        $.ajax({
            type: "GET",
            url: "{{url('change-subcategory-status')}}?subcategory_id=" + id + "&status=" + status,
            success: function(res) {
                if (res == "success") {
                    $('.msg').addClass("alert alert-success");
                    $('.msg').text("Status changed successfully.");
                    $('.msg').delay(800).fadeOut('slow');
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else {
                    $('.msg').addClass("alert alert-danger");
                    $('.msg').text("Failed to change status.");
                    $('.msg').delay(500).fadeOut('slow');
                }
            }
        });
    });
</script>
@endsection