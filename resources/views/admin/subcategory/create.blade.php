@extends('front_layouts.admin-layout')
@section('css')
<link rel="stylesheet" href="{{asset('front/css/dropify.min.css')}}">
@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar') 
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('subcategory')}}" class="text-muted"><i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            @if (!empty($subcategory))
                            <form method="post" action="{{ route('subcategory.update', $subcategory->id) }}" enctype="multipart/form-data">
                                <h3 class="mb-3">SubCategory Information:</h3>
                                <div class="form-row">
                                    <div class="form-group col-md-12">

                                                <label for="Category">Category</label>
                                                <select class="form-control" id="category" name="category">
                                                    <option value="">Choose category</option>
                                                    @foreach ($categories as $category)
                                                    <option value="{{$category->id}}" {{ isset($subcategory) ? (($subcategory->category_id == $category->id) ? 'selected' : '') : '' }}> {{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('category')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                </div>
                               
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="name">Name: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$subcategory->name}}" id="name" name="name" />
                                    </div>
                                   
                                </div>
                                
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="status">Status: <span class="text-danger">*</span></label>
                                        <label class="switch ">
                                            <input type="checkbox" value="1" class="form-check-input" {{$subcategory->status == 1 ? "checked" : ""}} name="status" id="status">
                                            <span class="slider round"></span>
                                        </label>
                                       
                                    </div>
                                </div>
                                @else
                                <form action="{{route('subcategory.store')}}" method="post" id="BannerForm" enctype="multipart/form-data">
                                    <h3 class="mb-3">SubCategory Information:</h3>
                                  <div class="form-row">  
                                    <div class="form-group col-md-12">

                                                <label for="Category">Category</label>
                                                <select class="form-control" id="category" name="category">
                                                    <option value="">Choose category</option>
                                                    @foreach ($categories as $category)
                                                    <option value="{{$category->id}}"> {{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="name">Name: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="" value="" id="name" name="name" />
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="status">Status: <span class="text-danger">*</span></label>
                                            <label class="switch ">
                                                <input type="checkbox" value="1" class="form-check-input" name="status" id="status">
                                                <span class="slider round"></span>
                                            </label>
                                            <!-- <div class="form-check">
                                                    <input type="checkbox" value="1" class="form-check-input" name="status" id="status">
                                                    <label class="form-check-label" for="exampleCheck1">Active</label>
                                                </div> -->
                                        </div>
                                    </div>
                                    @endif
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-12 text-right">
                                            <button type="submit" class="btn btn-yatra">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection

@section('js')
<script src="{{asset('front/js/dropify.min.js')}}"></script>
<script src="{{asset('front/plugins/ckeditor/ckeditor.js')}}"></script>
<script>
    jQuery.validator.addMethod("validUrl", function(value, element) {
        return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
    }, "Invalid URL.");

    $('#BannerForm').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
            if (element.attr("name") == "image") {
                error.insertAfter(".banner_image_msg");
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            name: "required",
            
            status: "required"
        }
    });

    $(document).ready(function() {
        $('.dropify').dropify();
        CKEDITOR.replace('banner-content');
    });
</script>
@endsection