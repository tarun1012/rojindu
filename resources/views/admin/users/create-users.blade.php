@extends('front_layouts.admin-layout')

@section('content')
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{!empty($user) ? "Update" : "Create"}} User</h5>
                        </div>
                    </div>
                    @if(isset($user))
                    <form id="userForm" class="form theme-form" method="POST" action="{{ route('user.request.update', $user->id) }}" enctype="multipart/form-data">
                        @else
                        <form id="userForm" class="user-form form theme-form" method="POST" action="{{ route('user.request') }}" enctype="multipart/form-data">
                            @endif
                            @csrf

                            <div class="form-row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body row ">
                                            <div class="form-group col-md-6">
                                                <label for="role">Role</label>
                                                <select name="role" id="role" class="form-control">
                                                    <option value="">Choose Role</option>
                                                    @if(!empty($roles))
                                                    @foreach($roles as $role)
                                                    <option value="{{$role->id}}" {{!empty($user) ? ($user->roleid == $role->id ? "selected" : "") :   $loop->first ? 'selected="selected"' : ''}}>{{$role->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                @error('role')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="role">Parent</label>
                                                <input type="text" class="form-control" id="parent_user" name="parent_user" value="{{!empty($user->parent_name) ? $user->parent_name.' - '.$user->parent_referal_code : ''}}" placeholder="Enter parent user name" />
                                                <input type="hidden" name="parent_user_id" value="{{!empty($user->parent_name) ? $user->parentid : ''}}" id="parent_user_id" style="display:none;" />
                                                @error('parent_user')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>


                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-body row">

                                            <div class="form-group col-md-6">
                                                <label for="name">First Name</label>
                                                <input class="form-control"  id="firstname" type="text" name="firstname" placeholder="Enter  Name" value="{{ $user->name ?? ''}}">
                                                @error('firstname')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                             <div class="form-group col-md-6">
                                                <label for="email"> Email </label>
                                                <input class="form-control" id="email" type="email" name="email" {{!empty($user) ? "readonly" : ""}} placeholder="Enter Email Address" value="{{ $user->email ?? ''}}">
                                                @error('email')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            
                                            
                                           

                                            

                                            
                                        </div>
                                    </div>

                                    

                                    <div class="card">
                                        <div class="row card-body">
                                            <div class="form-group col-md-6">
                                                <div class="form-check">
                                                    <input type="checkbox" value="1" class="form-check-input" {{ isset($user) ? (($user->status == 1) ? 'checked' : '') : '' }} name="status" id="status">
                                                    <label class="form-check-label" for="exampleCheck1">Active?</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-body text-right">
                                            <button class="btn btn-shukul mr-2" type="submit"> {{ isset($user) ? 'Update' : 'Submit' }} </button>
                                            <a href="{{ route('users') }}" class="btn btn-light">Cancel</a>
                                        </div>
                                    </div>



                                </div>

                            </div>

                        </form>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function() {
        $('#contactno').keypress(function(key) {
            if (key.charCode < 48 || key.charCode > 57) return false;
        });
        $('#whatsapp').keypress(function(key) {
            if (key.charCode < 48 || key.charCode > 57) return false;
        });
        $('#pincode').keypress(function(key) {
            if (key.charCode < 48 || key.charCode > 57) return false;
        });
        $('#IsWhatsapp').change(function() {
            if (!$('#IsWhatsapp').is(':checked')) {
                $('.whatsapp-block').show();
                $("#whatsapp").prop('required', true);
            } else {
                $('.whatsapp-block').hide();
            }
        });

        $.validator.addMethod('Validemail', function(value, element) {
            return this.optional(element) || value.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/);
        }, "Please enter a valid email address.");

        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space not allowed");


        $('#userForm').validate({
            errorPlacement: function(error, element) {
                error.insertAfter(element)
                error.addClass('text-danger');
            },
            messages: {
                email: {
                    remote: 'email already taken.',
                },
            },
            rules: {

                email: {
                    required: true,
                    Validemail: true,
                    remote: {
                        type: 'post',
                        url: "{{ URL('user/existusername') }}",
                        async: false,
                        async: false,
                        data: {
                            email: function() {
                                console.log('1');
                                return "@if(!empty($user)) @else " + $("input[id='email']").val() + " @endif";
                            },
                            id: "@if(isset($user)){{$user->id}}@endif",
                            "_token": "{{ csrf_token() }}"
                        },
                        async: false
                    }

                },
                firstname: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                },
                
                
                
                status: {
                    required: true
                },
            }
        });


        


        $("#parent_user").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "{{ route('search.parent.user') }}",
                    data: {
                        'parent_user': request.term,
                        '_token': "{{csrf_token()}}",
                        'id': '{{!empty($user) ? $user->id : 0}}'
                    },
                    type: "POST",
                    success: function(result) {
                        console.log(result);
                        response(jQuery.parseJSON(result));
                    }
                });
            },
            select: function(event, ui) {
                $('#parent_user').val(ui.item.label); // display the selected text
                $('#parent_user_id').val(ui.item.value); // save selected id to input
                return false;
            },
            minLength: 3
        });
    });
</script>
@endsection