@extends('front_layouts.admin-layout')

@section('css')



@endsection

@section('content')

<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">

           

            <div class="d-sm-flex align-items-center mb-3">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <h1 class="h5 mb-0 text-gray-800">Dashboard</h1>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->

            </div>
			
			
				
            
            <div class="row">
                <div class="col-12">
                    <div class="card dashboard">
                        <div class="card-body">
                            <div class="row">
                                <!-- Item -->
                                <div class="col-xl-4 col-12">
                                    <div class="item d-flex align-items-center ">
                                        <div class="icon bg-yatra"><i class="fas fa-user"></i></div>
                                       
                                        <!-- <div class="number"><strong>25</strong></div> -->
                                    </div>
                                </div>
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           

               
                





            </div>
        </div>

    </div>

</div>

@section('model')

@endsection
@section('js')
<script>
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
        $('#modal-default').modal('hide');

    }
</script>
@endsection
@endsection