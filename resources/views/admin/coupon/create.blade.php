@extends('front_layouts.admin-layout')
@section('css')
<link rel="stylesheet" href="{{asset('front/css/dropify.min.css')}}">
@endsection
@section('content') 
<div class="w-100 d-flex align-items-stretch">
    @include('front_partials.sidebar')
    <div id="content" class="profile">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('coupon')}}" class="text-muted"><i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            @if (!empty($coupon))
                            <form method="post" action="{{ route('coupon.update', $coupon->id) }}" enctype="multipart/form-data">
                                <h3 class="mb-3">Coupon Information:</h3>
                               
                               
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="category">Category: <span class="text-danger">*</span></label>
                                        <select class="form-control"  multiple="multiple" id="category" name="category[]">
                                                    <option value="">Choose Category</option>
                                                    
                                                    @foreach ($categories as $category)

                                                    @if(in_array($category->id, $cc))
                                                    <option value="{{ $category->id }}" selected="true">{{ $category->name }}</option>
                                                    @else
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endif 

                                                    @endforeach
                                                </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="product">Product: <span class="text-danger">*</span></label>
                                        <select class="form-control"  multiple="multiple" id="product" name="product[]">
                                                    <option value="">Choose Product</option>
                                                    @foreach ($products as $product)
                                                     @if(in_array($product->id, $cp))
                                                    <option value="{{ $product->id }}" selected="true">{{ $product->name }}</option>
                                                    @else
                                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                    @endif 
                                                    @endforeach
                                                </select>
                                    </div>
                                   
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name">Name: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$coupon->name}}" id="name" name="name" />
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="code">Code: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$coupon->code}}" id="code" name="code" />
                                    </div>
                                   
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="Discount">Discount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$coupon->discount}}" id="discount" name="discount" />
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="Discount Amount">Discount Amount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$coupon->discount_amount}}" id="discount_amount" name="discount_amount" />
                                    </div>
                                   
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="Min Amount">Min Amount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$coupon->min_amount}}" id="min_amount" name="min_amount" />
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="Max Amount">Max Amount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$coupon->max_amount}}" id="max_amount" name="max_amount" />
                                    </div>
                                   
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="detail">Content:</label>
                                        <textarea type="text" class="form-control" placeholder="" id="detail" name="detail">{{$coupon->detail}}</textarea>
                                    </div>
                                </div>
                                
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="status">Status: <span class="text-danger">*</span></label>
                                        <label class="switch ">
                                            <input type="checkbox" value="1" class="form-check-input" {{$coupon->status == 1 ? "checked" : ""}} name="status" id="status">
                                            <span class="slider round"></span>
                                        </label>
                                       
                                    </div>
                                </div>
                                @else
                                <form action="{{route('coupon.store')}}" method="post" id="CouponForm" enctype="multipart/form-data">
                                    <h3 class="mb-3">Coupon Information:</h3>

                                    <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="category">Category: <span class="text-danger">*</span></label>
                                        <select class="form-control"  multiple="multiple" id="category" name="category[]" required="">
                                                    <option value="">Choose Category</option>
                                                    @foreach ($categories as $category)
                                                    <option value="{{$category->id}}"> {{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="product">Product: <span class="text-danger">*</span></label>
                                        <select class="form-control"  multiple="multiple" id="product" name="product[]" required="">
                                                    <option value="">Choose Product</option>
                                                    @foreach ($products as $product)
                                                    <option value="{{$product->id}}" > {{ $product->name }}</option>
                                                    @endforeach
                                                </select>
                                    </div>
                                   
                                </div>
                                    
                                    
                                    <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name">Name: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="name" name="name" />
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="code">Code: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="code" name="code" />
                                    </div>
                                   
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="Discount">Discount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="discount" name="discount" />
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="Discount Amount">Discount Amount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="discount_amount" name="discount_amount" />
                                    </div>
                                   
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="Min Amount">Min Amount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="min_amount" name="min_amount" />
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="Max Amount">Max Amount: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="" id="max_amount" name="max_amount" />
                                    </div>
                                   
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="detail">Content:</label>
                                        <textarea type="text" class="form-control" placeholder="" id="detail" name="detail"></textarea>
                                    </div>
                                </div>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="status">Status: <span class="text-danger">*</span></label>
                                            <label class="switch ">
                                                <input type="checkbox" value="1" class="form-check-input" name="status" id="status">
                                                <span class="slider round"></span>
                                            </label>
                                            <!-- <div class="form-check">
                                                    <input type="checkbox" value="1" class="form-check-input" name="status" id="status">
                                                    <label class="form-check-label" for="exampleCheck1">Active</label>
                                                </div> -->
                                        </div>
                                    </div>
                                    @endif
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-12 text-right">
                                            <button type="submit" class="btn btn-yatra">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection

@section('js')
<script src="{{asset('front/js/dropify.min.js')}}"></script>
<script src="{{asset('front/plugins/ckeditor/ckeditor.js')}}"></script>
<script>
    jQuery.validator.addMethod("validUrl", function(value, element) {
        return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
    }, "Invalid URL.");

    $('#CouponForm').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
            
        },
        rules: {
            name: "required",
            
            detail: "required",
            code: "required",
            discount: "required",
            product: "required",
            category: "required",
            min_amount: "required",
            max_amount: "required",
            discount_amount: "required",
            status: "required"

        }
    });

    $(document).ready(function() {
        $('.dropify').dropify();
        CKEDITOR.replace('detail');
    });
</script>
@endsection