@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    
    <div id="content" class="profile col-lg-12">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span> 
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            @include('tools.errors')
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>Address </div>
                            @if (get_user_permission("add_address","add"))
                            <a href="{{route('address.create')}}" class="btn btn-sm btn-rounded btn-yatra">Add Address</a>
                            @endif
                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="mt-12 mb-12 msg"></div>
                                <table id="AddressTable" class="table table-hover  display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                           <th>Address</th>
                                            <th>Country</th>
                                            <th>State</th>
                                            <th>city</th>
                                            <th>locality</th>
                                            <th>Pincode</th>
                                            <th>Default</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($AddressData as $address)
                                        <tr>
                                            <td>{{$address->address}}</td>
                                            <td>{{$address->country_name}}</td>
                                            <td>{{$address->state_name}}</td>
                                            <td>{{$address->city_name}}</td>
                                            <td>{{$address->locality}}</td>
                                            <td>{{$address->pincode}}</td>
                                            <td>
                                                @if ($address->is_default == 1)
                                                <span class="badge badge-success">Yes</span>
                                                @else
                                                <span class="badge badge-danger">No</span>
                                                @endif
                                            </td>
                                            
                                            <td>
                                                @if ($address->status == 1)
                                                <span class="badge badge-success">Active</span>
                                                @else
                                                <span class="badge badge-danger">Paused</span>
                                                @endif
                                            </td>
                                            
                                            <td>
                                                @if ($address->status == 1)
                                                <button type="button" class="btn btn-rounded btn-sm btn-warning changeAddressStatus" data-address_id="{{$address->id}}" data-status="{{$address->status}}" data-toggle="tooltip" title="Pause address"><i class="fa fa-power-off"></i></button>
                                                @else
                                                <button type="button" class="btn btn-rounded btn-sm btn-success changeAddressStatus" data-address_id="{{$address->id}}" data-status="{{$address->status}}" data-toggle="tooltip" value="Status" title="Activate address"><i class="fa fa-refresh"></i></button>
                                                @endif

                                                 @if ($address->is_default == 1)
                                                <button type="button" class="btn btn-rounded btn-sm btn-warning changeAddressD" data-address_id="{{$address->id}}" data-status="{{$address->is_default}}" data-toggle="tooltip" title="Remove Default address"><i class="fa fa-power-off"></i></button>
                                                @else
                                                <button type="button" class="btn btn-rounded btn-sm btn-success changeAddressD" data-address_id="{{$address->id}}" data-status="{{$address->is_default}}" data-toggle="tooltip" value="Status" title="Set Default address"><i class="fa fa-refresh"></i></button>
                                                @endif
                                                @if (get_user_permission("add_address","edit"))
                                                <a href="{{ route('address.edit', $address->id) }}" class="btn btn-rounded btn-sm btn-info" data-toggle="tooltip" title="Edit address"><i class="fa fa-edit"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#AddressTable').DataTable({
            scrollY: '60vh',
            "scrollX": true
        });
    });

    $('.changeAddressStatus').click(function() {
       // alert('1');
        var id = $(this).attr("data-address_id");
        var status = $(this).attr("data-status");
        $.ajax({
            type: "GET",
            url: "{{url('change-address-status')}}?address_id=" + id + "&status=" + status,
            success: function(res) {
                if (res == "success") {
                    $('.msg').addClass("alert alert-success");
                    $('.msg').text("Status changed successfully.");
                    $('.msg').delay(800).fadeOut('slow');
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else {
                    $('.msg').addClass("alert alert-danger");
                    $('.msg').text("Failed to change status.");
                    $('.msg').delay(500).fadeOut('slow');
                }
            }
        });
    });

    $('.changeAddressD').click(function() {
       // alert('1');
        var id = $(this).attr("data-address_id");
        var status = $(this).attr("data-status");
        $.ajax({
            type: "GET",
            url: "{{url('change-address-d')}}?address_id=" + id + "&is_default=" + status,
            success: function(res) {
                console.log(res);
                if (res == "success") {
                    $('.msg').addClass("alert alert-success");
                    $('.msg').text("changed successfully.");
                    $('.msg').delay(800).fadeOut('slow');
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else {
                    $('.msg').addClass("alert alert-danger");
                    $('.msg').text("Failed to change.");
                    $('.msg').delay(500).fadeOut('slow');
                }
            }
        });
    });
</script>
@endsection