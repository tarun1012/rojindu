@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{asset('front/css/dropify.min.css')}}">
@endsection
@section('content')
<div class="w-100 d-flex align-items-stretch">
    
    <div id="content" class="profile col-md-12">
        <div class="container-fluid ml-2">
            <div class="d-sm-flex align-items-center mb-4">
                <div class="custom-menu mr-2 d-block d-lg-none ">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary ">
                        <i class="fa fa-bars"></i>
                        <span class="sr-only">Toggle Menu</span> 
                    </button>
                </div>
                <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('address')}}" class="text-muted"><i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            @if (!empty($address))
                            <form method="post" action="{{ route('address.update', $address->id) }}" enctype="multipart/form-data">
                                <h3 class="mb-3">Address Information:</h3>
                                

                                 <div class="form-row">
                                    <div class="form-group col-md-4">

                                                <label for="Category">Country</label>
                                                <select class="form-control" id="country" name="country">
                                                    <option value="">Choose Country</option>
                                                    @foreach ($country as $country)
                                                    <option value="{{$country->id}}" {{ isset($address) ? (($address->country == $country->id) ? 'selected' : '') : '' }}> {{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                               
                                            </div>

                                            <div class="form-group col-md-4">

                                                <label for="Category">State</label>
                                                <select class="form-control" id="state" name="state">
                                                    <option value="">Choose State</option>
                                                    @foreach ($state as $state)
                                                    <option value="{{$state->id}}" {{ isset($address) ? (($address->state == $state->id) ? 'selected' : '') : '' }}> {{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                            <div class="form-group col-md-4">

                                                <label for="Category">City</label>
                                                <select class="form-control" id="city" name="city">
                                                    <option value="">Choose City</option>
                                                    @foreach ($city as $city)
                                                    <option value="{{$city->id}}" {{ isset($address) ? (($address->city == $city->id) ? 'selected' : '') : '' }}> {{ $city->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                            
                                </div>
                               
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name">Locality: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$address->locality}}" id="locality" name="locality" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="price">Pincode: <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="" value="{{$address->pincode}}" id="pincode" name="pincode" />
                                    </div>
                                   
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="content">Address:</label>
                                        <textarea type="text" class="form-control" placeholder="" id="address" name="address">{{$address->address}}</textarea>
                                    </div>
                                </div>

                                
                                
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="status">Status: <span class="text-danger">*</span></label>
                                        <label class="switch ">
                                            <input type="checkbox" value="1" class="form-check-input" {{$address->status == 1 ? "checked" : ""}} name="status" id="status">
                                            <span class="slider round"></span>
                                        </label>
                                       
                                    </div>
                                </div>
                                @else
                                <form action="{{route('address.store')}}" method="post" id="AddressForm" enctype="multipart/form-data">
                                    <h3 class="mb-3">Address Information:</h3>
                                    

                                    <div class="form-row">  
                                    <div class="form-group col-md-4">

                                                <label for="Category">Country</label>
                                                <select class="form-control" id="country" name="country">
                                                    <option value="">Choose country</option>
                                                    @foreach ($country as $country)
                                                    <option value="{{$country->id}}"> {{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>

                                            <div class="form-group col-md-4">

                                                <label for="Category">State</label>
                                                <select class="form-control" id="state" name="state">
                                                    <option value="">Choose State</option>
                                                    @foreach ($state as $state)
                                                    <option value="{{$state->id}}"> {{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>

                                            <div class="form-group col-md-4">

                                                <label for="Category">City</label>
                                                <select class="form-control" id="city" name="city">
                                                    <option value="">Choose City</option>
                                                    @foreach ($city as $city)
                                                    <option value="{{$city->id}}"> {{ $city->name }}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                             
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="name">Locality: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="" value="" id="locality" name="locality" />
                                        </div>

                                         <div class="form-group col-md-12">
                                            <label for="price">Pincode: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="" value="" id="pincode" name="pincode" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="content">Address:</label>
                                        <textarea type="text" class="form-control" placeholder="" id="address" name="address"></textarea>
                                    </div>
                                </div>

                                
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="status">Status: <span class="text-danger">*</span></label>
                                            <label class="switch ">
                                                <input type="checkbox" value="1" class="form-check-input" name="status" id="status">
                                                <span class="slider round"></span>
                                            </label>
                                           
                                        </div>
                                    </div>
                                    @endif
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-12 text-right">
                                            <button type="submit" class="btn btn-yatra">Submit</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection

@section('js')
<script src="{{asset('front/js/dropify.min.js')}}"></script>
<script src="{{asset('front/plugins/ckeditor/ckeditor.js')}}"></script>
<script>
     $('.delete_gallery_image').click(function() {
        if (confirm('Are you sure you want to delete?')) {
            var id = $(this).attr("data-gallery_id");
            var path = $(this).attr("data-gallery_image");
            var token = $('input[name="csrf-token"]').attr('content');

            $.ajax({
                url: "{{url('/product-gallery/delete')}}/" + id,
                type: 'POST',
                data: {
                    "id": id,
                    "path": path,
                    "_token": token,
                },
                success: function(res) {
                    if (res == "success") {
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        alert("Failed to delete gallery image item.");
                    }
                }
            });
        }
    });

    jQuery.validator.addMethod("validUrl", function(value, element) {
        return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
    }, "Invalid URL.");

    $('#AddressForm').validate({
        errorPlacement: function(error, element) {
            error.insertAfter(element)
            error.addClass('text-danger');
            if (element.attr("name") == "image") {
                error.insertAfter(".banner_image_msg");
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
            locality: "required",
            pincode: "required",
            country: "required",
            state: "required",
            city: "required",
            address: "required",
            
            status: "required"
        }
    });

    $(document).ready(function() {
        $('.dropify').dropify();
        CKEDITOR.replace('product-content');
    });

     $('#state').change(function() {
            var cID = $(this).val();
          //  console.log('1');
            if (cID) {
                $.ajax({
                    type: "GET",
                    url: "{{url('get-city-list')}}?c_id=" + cID,
                    success: function(res) {
                        console.log(res);
                        if (res) {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose city</option>');
                            $.each(res, function(key, value) {
                                $("#city").append('<option value="' + key + '">' + value + '</option>');
                            });
                            
                        } else {
                            $("#city").empty();
                            $("#city").append('<option value="">Choose City</option>');
                            
                        }
                    }
                });
            } else {
                $("#city").empty();
                $("#city").append('<option value="">Choose City</option>');
                
            }
        });

     
</script>
@endsection