<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Social Auth
Route::get('auth/social', 'Auth\SocialAuthController@show')->name('social.login');
Route::get('oauth/{driver}', 'Auth\SocialAuthController@redirectToProvider')->name('social.oauth');
Route::get('oauth/{driver}/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('social.callback');

Route::get('/RetailerRegister', 'UserController@createRetailerRegisterPage')->name('RetailerRegister');
Route::post('retailer/request', 'UserController@saveRetailerRegister')->name('retailer.request.register');
Route::post('/existusername', 'UserController@existusername');




Route::get('get-state-list1','UserAddressController@getStateList');
Route::get('get-city-list1','UserAddressController@getCityList');

Auth::routes();
Route::post('captcha', 'RegisterController@captchaValidate');
Route::get('refreshcaptcha', 'RegisterController@refreshCaptcha');
Route::get('twoFactor', 'Auth\TwoFactorController@index');

Route::get('verify/resend', 'Auth\TwoFactorController@resend')->name('verify.resend');
Route::resource('verify', 'Auth\TwoFactorController')->only(['index', 'store']);


Route::get('change-password', 'ChangePasswordController@index')->name('changepassword');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth','admin']],function(){
  Route::get('admin/dashboard', function(){
  	return view('admin.dashboard');

  	/**
     * Banner Routes
     */
   

  });
  

  Route::group(['prefix' => 'admin/dashboard'], function()  
{ 
	Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');
 /**
 * Category Routes
 */
 Route::get('category','CategoryController@index')->name('category');
    Route::get('create-category','CategoryController@create')->name('category.create');
    Route::post('/store-category','CategoryController@store')->name('category.store');
    Route::get('/category/edit/{id}','CategoryController@edit')->name('category.edit');
    Route::post('/category/request/update/{id}', 'CategoryController@update')->name('category.update');

    /**
 * SubCategory Routes
 */
 Route::get('subcategory','SubCategoryController@index')->name('subcategory');
    Route::get('create-subcategory','SubCategoryController@create')->name('subcategory.create');
    Route::post('/store-subcategory','SubCategoryController@store')->name('subcategory.store');
    Route::get('/subcategory/edit/{id}','SubCategoryController@edit')->name('subcategory.edit');
    Route::post('/subcategory/request/update/{id}', 'SubCategoryController@update')->name('subcategory.update');

       /**
 * Product Routes
 */
 Route::get('product','ProductController@index')->name('product');
    Route::get('create-product','ProductController@create')->name('product.create');
    Route::post('/store-product','ProductController@store')->name('product.store');
    Route::get('/product/edit/{id}','ProductController@edit')->name('product.edit');
    Route::post('/product/request/update/{id}', 'ProductController@update')->name('product.update');

    Route::get('retailerproduct','RetailerProductController@index')->name('retailerproduct');
    Route::get('create-retailerproduct','RetailerProductController@create')->name('retailerproduct.create');
   Route::post('/store-retailerproduct','RetailerProductController@store')->name('retailerproduct.store');
   /*  Route::get('/retailerproduct/edit/{id}','RetailerProductController@edit')->name('retailerproduct.edit');
    Route::post('/retailerproduct/request/update/{id}', 'RetailerProductController@update')->name('retailerproduct.update');*/

   /* Route::get('retailerproduct', 'RetailerProductController@index')->name('retailerproduct');*/
//Route::get('/getUsers', 'RetailerProductController@getUsers');

   Route::get('/import_excel', 'ImportExcelController@index');
Route::post('/import_excel/import', 'ImportExcelController@import')->name('import');;

   Route::get('productrequest','ProductController@ProductRequest')->name('productrequest');
    Route::get('create-productrequest','ProductController@createProductRequest')->name('productrequest.create');
   Route::post('/store-productrequest','ProductController@storeProductRequest')->name('productrequest.store');
   Route::get('/productrequest/edit/{id}','ProductController@editProductRequest')->name('productrequest.edit');
    Route::post('/productrequest/request/update/{id}', 'ProductController@updateProductRequest')->name('productrequest.update');
     Route::get('manage_product_request','ProductController@manage_product_request')->name('manage_product_request');

Route::post('/retailerproduct-update', 'RetailerProductController@updateRetailerProduct1')->name('retailerproduct_update');


Route::get('/labels/show', 'LabelsController@show');
    Route::get('/labels/add-product-row', 'LabelsController@addProductRow');
    Route::post('/labels/preview', 'LabelsController@preview');



     /**
 * Brand Routes
 */
 Route::get('brand','BrandController@index')->name('brand');
    Route::get('create-brand','BrandController@create')->name('brand.create');
    Route::post('/store-brand','BrandController@store')->name('brand.store');
    Route::get('/brand/edit/{id}','BrandController@edit')->name('brand.edit');
    Route::post('/brand/request/update/{id}', 'BrandController@update')->name('brand.update');

     /**
 * Brand Routes
 */
 Route::get('unit','ProductUnitController@index')->name('product_unit');
    Route::get('create-unit','ProductUnitController@create')->name('product_unit.create');
    Route::post('/store-unit','ProductUnitController@store')->name('product_unit.store');
    Route::get('/unit/edit/{id}','ProductUnitController@edit')->name('product_unit.edit');
    Route::post('/unit/request/update/{id}', 'ProductUnitController@update')->name('product_unit.update');

    /**
     * Banner Routes
     */
    Route::get('banner','BannerController@index')->name('banner');
    Route::get('create-banner','BannerController@createBanner')->name('banner.create');
    Route::post('/store-banner','BannerController@storeBanner')->name('banner.storeBanner');
    Route::get('/banner/edit/{id}','BannerController@editBanner')->name('banner.edit');
    Route::post('/banner/request/update/{id}', 'BannerController@updateBanner')->name('banner.update');

     /**
 * menu Routes
 */
 Route::get('menu','MenuController@index')->name('menu');
    Route::get('create-menu','MenuController@create')->name('menu.create');
    Route::post('/store-menu','MenuController@store')->name('menu.store');
    Route::get('/menu/edit/{id}','MenuController@edit')->name('menu.edit');
    Route::post('/menu/request/update/{id}', 'MenuController@update')->name('menu.update');

     /**
 * coupon Routes
 */
 Route::get('coupon','CouponController@index')->name('coupon');
    Route::get('create-coupon','CouponController@create')->name('coupon.create');
    Route::post('/store-coupon','CouponController@store')->name('coupon.store');
    Route::get('/coupon/edit/{id}','CouponController@edit')->name('coupon.edit');
    Route::post('/coupon/request/update/{id}', 'CouponController@update')->name('coupon.update');

    /*role*/
    Route::get('roles', 'RoleController@showRoles')->name('roles');
Route::get('create-roles', 'RoleController@createRoles')->name('create.roles');
Route::get('edit-roles/{id}', 'RoleController@editRoles')->name('edit.roles');
Route::post('role/request', 'RoleController@saveRoles')->name('role.request');
Route::post('role/request/update/{id}', 'RoleController@updateRoles')->name('role.request.update');
Route::get('change-role/status/{id}', 'RoleController@changeRoleStatus')->name('change.roles.status');

// Permissions
Route::get('permissions', 'PermissionController@showPermissions')->name('permissions');

// Module
Route::get('modules', 'ModulesController@showModules')->name('modules');
Route::get('modules/create', 'ModulesController@createModules')->name('module.create');
Route::post('modules/request', 'ModulesController@saveModules')->name('module.request');
Route::get('edit-module/{id}', 'ModulesController@editModule')->name('edit.module');
Route::post('modules/request/update/{id}', 'ModulesController@updateModules')->name('module.request.update');
Route::get('change-module/status/{id}', 'ModulesController@changeModuleStatus')->name('change.modules.status');

//user
Route::get('users', 'UserController@showUsers')->name('users');
Route::get('user/create', 'UserController@createUsers')->name('users.create');
Route::post('user/request', 'UserController@saveUser')->name('user.request');
Route::get('edit-user/{id}', 'UserController@editUser')->name('edit.user');
Route::get('change/user-status/{id}', 'UserController@changeUserStatus')->name('change.user.status');
Route::get('change/product-status/{id}', 'ProductController@changeProductStatus_datatable')->name('change.product_datatable.status');
Route::post('user/request/update/{id}', 'UserController@updateUser')->name('user.request.update');
Route::get('/user/view/{id}', 'UserController@view')->name('view.user');
Route::get('view-user-upline/{id}', 'UserController@viewUserUpline')->name('view.user.upline');

Route::get('allCloseaccountrequest','UserController@AllCloseAccountRequest')->name('allCloseaccountrequest');

//user
Route::get('retailer', 'UserController@showRetailer')->name('retailer');
Route::get('retailer/create', 'UserController@createRetailer')->name('retailer.create');
Route::post('retailer/request', 'UserController@saveRetailer')->name('retailer.request');
Route::get('edit-retailer/{id}', 'UserController@editRetailer')->name('edit.retailer');
Route::get('change/retailer-status/{id}', 'UserController@changeUserStatus')->name('change.retailer.status');
Route::post('retailer/request/update/{id}', 'UserController@updateRetailer')->name('retailer.request.update');
Route::get('/retailer/view/{id}', 'UserController@view')->name('view.user');


Route::get('change-product-status1','ProductController@changeProductStatus');




Route::get('change/password', 'Auth\ChangePasswordController@changePasswordForm')->name('change.password');

Route::post('change/password/request', 'Auth\ChangePasswordController@changePasswordRequest')->name('change.password.request');


 Route::get('editprofile', 'UserController@aeditProfileForm')->name('aprofile.update');
Route::post('editprofile/request/{id}', 'UserController@aupdateProfile')->name('aprofile.update.request');

Route::get('retailer/editprofile', 'UserController@reditProfileForm')->name('rprofile.update');
Route::post('retailer/editprofile/request/{id}', 'UserController@rupdateProfile')->name('rprofile.update.request');

Route::get('myprofile/view', 'UserController@amyprofileView')->name('amyprofile.view');
Route::get('retailer/myprofile/view', 'UserController@rmyprofileView')->name('rmyprofile.view');

Route::get('retailer/profile/edit-bank-detail', 'BankVerificationController@editBankDetail')->name('profile.editbankdetail');
Route::post('retailer/bankdetail/request', 'BankVerificationController@updateBankdetail')->name('bankdetail.request');
Route::get('retailer/myprofile-show-kyc-info', 'UserController@show_kyc_info')->name('myprofile.view.kyc_info');
Route::get('retailer/myprofile-show-kyc-history', 'UserController@show_kyc_history')->name('myprofile.view.kyc_history'); 

Route::get('fetch-permission','PermissionController@fetchPermission')->name('permission.fetch');
Route::get('add-permission','PermissionController@addPermission')->name('permission.add');

Route::get('document/varification', 'BankVerificationController@kycDocVerification')->name('admin.document.varification');
Route::post('document/rejection/request', 'BankVerificationController@documentRejectionComment')->name('documentvarification.comment.request');
Route::get('document/confirmation/{id}/request', 'BankVerificationController@documentVerifiedRequest')->name('documentvarification.confirmation');


  });

  /**
 * Permission Ajax Routes
 */

Route::post('search-parent-user', 'UserController@searchParent')->name('search.parent.user');
Route::post('/user/existusername', 'UserController@existusername');




    /**
 * Ajax Routes
 */
Route::get('change-category-status','CategoryController@changeCategoryStatus');
Route::get('change-subcategory-status','SubCategoryController@changeSubCategoryStatus');
Route::get('change-product-status','ProductController@changeProductStatus');
Route::get('change-retailerproduct-status','RetailerProductController@changeRetailerProductStatus');
Route::get('change-address-status','UserAddressController@changeAddressStatus');
Route::get('change-address-d','UserAddressController@changeAddressD');
Route::get('change-menu-status','MenuController@changeMenuStatus');
Route::get('change-coupon-status','CouponController@changeCouponStatus');
Route::get('change-brand-status','BrandController@changeBrandStatus');
Route::get('change-unit-status','ProductUnitController@changeProductUnitStatus');
Route::get('get-subcategory-list','ProductController@getSubcategoryList');
Route::get('get-city-list','UserAddressController@getCityList');
Route::get('change-banner-status','BannerController@changeBannerStatus');
Route::get('product-request-status','ProductController@changeProductRequestStatus');
Route::get('close-account-request-status','UserController@CloseAccountRequestStatus');
Route::get('admin-close-request-status','UserController@AdminCloseAccountRequestStatus');
Route::post('/product-gallery/delete/{id}', 'ProductController@destroyGallery')->name('products.destroyGallery');
Route::get('/purchases/get_products', 'LabelsController@getProducts');
Route::get('/labels/add-product-row', 'LabelsController@addProductRow');

Route::get('allproduct', 'ProductController@allProducts' )->name('allproduct');
Route::get('allbanner', 'BannerController@allBanners' )->name('allbanner');
Route::get('allcategory', 'CategoryController@allCategory' )->name('allcategory');
Route::get('allsubcategory', 'SubCategoryController@allSubCategory' )->name('allsubcategory');
Route::get('allunit', 'ProductUnitController@allProductUnit' )->name('allunit');
Route::get('allbrand', 'BrandController@allBrand' )->name('allbrand');
Route::get('change/banner-status/{id}', 'BannerController@changeBannerStatus_datatable')->name('change.banner_datatable.status');
Route::get('change/category-status/{id}', 'CategoryController@changeCategoryStatus_datatable')->name('change.category_datatable.status');
Route::get('change/subcategory-status/{id}', 'SubCategoryController@changeSubCategoryStatus_datatable')->name('change.subcategory_datatable.status');
Route::get('change/productunit-status/{id}', 'ProductUnitController@changeProductUnitStatus_datatable')->name('change.productunit_datatable.status');
Route::get('change/brand-status/{id}', 'BrandController@changeBrandStatus_datatable')->name('change.brand_datatable.status');

Route::get('/', 'CartController@shop')->name('shop');
Route::get('/cart', 'CartController@cart')->name('cart.index');
Route::post('/add', 'CartController@add')->name('cart.store');
Route::post('/update', 'CartController@update')->name('cart.update');
Route::post('/remove', 'CartController@remove')->name('cart.remove');
Route::post('/clear', 'CartController@clear')->name('cart.clear');

Route::get('address','UserAddressController@index')->name('address');
    Route::get('create-address','UserAddressController@create')->name('address.create');
    Route::post('/store-address','UserAddressController@store')->name('address.store');
    Route::get('/address/edit/{id}','UserAddressController@edit')->name('address.edit');
    Route::post('/address/request/update/{id}', 'UserAddressController@update')->name('address.update');
    



});

Route::group(['middleware' => ['auth','admin','twofactor']],function(){
  

Route::get('/', 'CartController@shop')->name('shop');
Route::get('/cart', 'CartController@cart')->name('cart.index');
Route::post('/add', 'CartController@add')->name('cart.store');
Route::post('/update', 'CartController@update')->name('cart.update');
Route::post('/remove', 'CartController@remove')->name('cart.remove');
Route::post('/clear', 'CartController@clear')->name('cart.clear');

Route::get('address','UserAddressController@index')->name('address');
    Route::get('create-address','UserAddressController@create')->name('address.create');
    Route::post('/store-address','UserAddressController@store')->name('address.store');
    Route::get('/address/edit/{id}','UserAddressController@edit')->name('address.edit');
    Route::post('/address/request/update/{id}', 'UserAddressController@update')->name('address.update');

    Route::get('editprofile', 'UserController@editProfileForm')->name('profile.update');
Route::post('editprofile/request/{id}', 'UserController@updateProfile')->name('profile.update.request');

Route::get('myprofile/view', 'UserController@myprofileView')->name('myprofile.view');




});





 



