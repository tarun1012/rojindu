<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App/User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use DatabaseMigrations;
    public function it_has_cache_key_for_otp()
    {
        $user = factory(User::class)->create();
        $this->assertEquals($user->OTPkey(),'OTP_for_1');
    }
}
